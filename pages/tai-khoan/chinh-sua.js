import Profile from '@/components/Authorize/profile';
import Popup from '@/components/GlobalPopup/popup';
import eventEmitter from '@/lib/eventEmitter';
import { getCustomerData, redirectIfUnauthenticated } from '@/lib/helper';
import LocationService from '@/lib/location/location.service';
import ProductService from '@/lib/products/product.service';
import CustomerAddressService from '@/lib/users/address.service';
import { default as UserService } from '@/lib/users/user.service';
import { yupResolver } from '@hookform/resolvers/yup';
import axios from 'axios';
import { getSession } from "next-auth/client";
import Link from "next/link";
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as Yup from 'yup';

const baseURL = 'https://api.dev.318.suplo.vn/v1';

const UpdateUser = ({ cities, customer, address, session, districts }) => {
    const router = useRouter();
    const [savedata, setSavedata] = useState('');
    const [district, setDistrict] = useState(districts);
    const [base64, setBase64] = useState(null);
    const token = session.user.accessToken;
    const [isLoading, setLoading] = useState(false)

    const validationSchema = Yup.object().shape({
        name: Yup.string().required('Tên là bắt buộc.').min(1, 'Tên quá ngắn').max(64, 'Tên quá dài'),
        email: Yup.string().email('Email không hợp lệ.'),
        phoneNumber: Yup.string().required('Số điện thoại là bắt buộc.').matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/, 'Số điện thoại không hợp lệ'),
        city: Yup.string().when('address1', {
            is: (value) => value?.length,
            then: Yup.string().nullable().required("Vui lòng chọn Tỉnh/Thành")
        }),
        district: Yup.string().nullable().when('address1', {
            is: (value) => value?.length,
            then: Yup.string().required("Vui lòng chọn Quận/Huyện")
        }),
        address1: Yup.string().nullable().notRequired().when('address1', {
            is: (value) => value?.length,
            then: (rule) => rule.max(200, 'Địa chỉ 1 quá dài'),
        }).when('city', {
            is: (value) => value?.length,
            then: Yup.string().required("Vui lòng nhập địa chỉ cụ thể"),
        }),
        address2: Yup.string().nullable().notRequired().when('address2', {
            is: (value) => value?.length,
            then: (rule) => rule.max(200, 'Địa chỉ 2 quá dài'),
        })
    }, [
        ['address2', 'address2'],
        ['address1', 'address1'],
        ['address1', 'city'],
    ]);

    const formOptions = {
        resolver: yupResolver(validationSchema), defaultValues: {
            name: customer.name,
            email: customer.email,
            phoneNumber: customer.phoneNumber ? customer.phoneNumber.replace('+84', '0') : null,
            dob: customer.dob,
            gender: customer.gender,
            city: address?.city?.id,
            district: address?.district?.id,
            address1: address?.address1,
            address2: address?.address2,
        }
    };

    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;
    const getBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    const cityChanged = async (e) => {
        const cityId = e.target.value;
        const districtRes = await LocationService.litsDistrict(cityId)
        setDistrict(districtRes.districts);
    }


    const handleChange = async (e) => {
        const file = e.target.files[0];
        const base64 = await getBase64(file);
        setBase64(base64);
    }

    async function updateUser(data) {
        let hasAddress = false;
        let newAddress = {};
        setLoading(true)
        if (data.address1) {
            hasAddress = true;
            newAddress = {
                address1: data.address1,
                phoneNumber: data.phoneNumber ?? undefined,
                districtId: data.district,
                cityId: data.city,
                default: true,
                name: data.name,
            }
        }

        try {
            const dataCustomer = {

                phoneNumber: data.phoneNumber,
                name: data.name,
                dob: data.dob,
                gender: data.gender,

            };
            const flagData = base64?.split("base64,")[1];
            if (flagData) {
                const imageRes = await axios.post(`${baseURL}/images`, {
                    attachment: flagData,
                    width: 500,
                    height: 500
                }, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    }
                });
                dataCustomer.photoUrl = imageRes.data.image.publicUrl
            }

            if (hasAddress) {
                if (address) {
                    await CustomerAddressService.update(token, newAddress, address.id);
                } else {
                    await CustomerAddressService.create(token, newAddress);
                }
            }

            await UserService.update(token, dataCustomer);
            router.push('/tai-khoan/thong-tin')
        } catch (error) {
            setLoading(false)
            eventEmitter.emit('show_global_popup', {
                message: error?.response?.data?.message ?? error?.message ?? 'Có lỗi xảy ra'
            });
        }
    }

    return (
        <>
            <Profile>
                <div className="user-profile__info">
                    <h3 className="user-profile__info--title">
                        Sửa thông tin
                    </h3>
                    <div className="user-profile__info--console">
                        <div className="update-user-profile">
                            <form onSubmit={handleSubmit(updateUser)}>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <label>Thay đổi ảnh đại diện</label>
                                        <div className="change-user-img">
                                            <input
                                                type="file"
                                                className="form-control"
                                                name="photoUrl"
                                                onChange={handleChange}
                                            />
                                            <img src={base64} className={base64 ? "img-frames" : "d-none"} />
                                        </div>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="name"
                                            placeholder={savedata.name}
                                            {...register("name")}
                                        />
                                        <span className="error_message">{errors.name?.message}</span>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="phoneNumber"
                                            placeholder={savedata.phoneNumber}
                                            {...register("phoneNumber")}
                                        />
                                        <span className="error_message">{errors.phoneNumber?.message}</span>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <input
                                            type="date"
                                            name="dob"
                                            {...register("dob")}
                                            className="form-control"
                                        />
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <select name="gender"  {...register("gender")} className="form-control">
                                            <option value="u">Không xác định</option>
                                            <option value="m">Nam</option>
                                            <option value="f">Nữ</option>
                                        </select>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <select name="city"{...register("city")} onChange={(e) => cityChanged(e)} className="form-control">
                                            <option value="">Tỉnh/Thành phố</option>
                                            {cities.map(city => (
                                                <option key={city.id} value={city.id}>{city.name}</option>
                                            ))}
                                        </select>
                                        <span className="error_message">{errors.city?.message}</span>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <select name="districtId" {...register("district")} className="form-control">
                                            <option value="">Quận/Huyện</option>
                                            {district.map(item => (
                                                <option key={item.id} value={item.id}>{item.name}</option>
                                            ))}
                                        </select>
                                        <span className="error_message">{errors.district?.message}</span>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="address1"
                                            {...register("address1")}
                                            placeholder="Địa chỉ 1"
                                        />
                                        <span className="error_message">{errors.address1?.message}</span>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="address2"
                                            placeholder="Địa chỉ 2"
                                            {...register("address2")}
                                        />
                                        <span className="error_message">{errors.address2?.message}</span>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-12">
                                        <div className="submit-box">
                                            <Link href="/tai-khoan/thong-tin">
                                                <a type="button" className="btn btn-cancle-update">
                                                    Huỷ
                                                </a>
                                            </Link>
                                            <button type="submit" className="btn btn-update-user btn_submit">
                                                {isLoading ? (
                                                    <div className="spinner-border text-light" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                ) : 'Lưu thay đổi'}

                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </Profile>

        </>
    )
}

export async function getServerSideProps({ req, res }) {
    try {
        const customerData = await getCustomerData(res, req, true, true);
        const token = customerData?.session?.user?.accessToken;

        const cityRes = await LocationService.listCity();

        const addressRes = await CustomerAddressService.list(token);
        const addressDefault = addressRes?.addresses?.find(a => a.default) ?? addressRes?.addresses[0];

        let districts = [];
        if (addressDefault) {
            const districtRes = await LocationService.litsDistrict(addressDefault.city.id);
            districts = districtRes.districts;
        }

        return {
            props: {
                cities: cityRes.cities,
                address: addressDefault ?? null,
                districts,
                ...customerData
            }
        }
    } catch (error) {
        console.log(error);
        redirectIfUnauthenticated(undefined, res);
    }
}

UpdateUser.auth = true;
export default UpdateUser;

