import Link from "next/link";
import React, { useState } from 'react';
import Profile from '@/components/Authorize/profile';
import { formatDate, getCustomerData, redirectIfUnauthenticated, showGlobalPopup } from '@/lib/helper';
import CustomerAddressService from "@/lib/users/address.service";
import UserService from '@/lib/users/user.service';
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

const CHANGE_PASS = {
    oldPassword: "",
    newPassword: "",
    reType: ""
}

const Userinfo = ({ customer, address, wishlistCount, session }) => {
    const token = session?.user?.accessToken

    const [password, setPassword] = useState(CHANGE_PASS);
    const [flag, setFlag] = useState(false);

    const validationSchema = Yup.object().shape({
        oldPassword: Yup.string().required('Mật khẩu cũ là bắt buộc.').min(8, 'Mật khẩu phải từ 8-64 ký tự').max(64, 'Mật khẩu phải từ 8-64 ký tự'),
        newPassword: Yup.string().required('Mật khẩu mới là bắt buộc.').min(8, 'Mật khẩu phải từ 8-64 ký tự').max(64, 'Mật khẩu phải từ 8-64 ký tự'),
        passwordConfirmation: Yup.string()
            .oneOf([Yup.ref('newPassword'), null], 'Mật khẩu không khớp')
    });

    const formOptions = {
        resolver: yupResolver(validationSchema)
    };

    const { register, handleSubmit, reset, formState: { errors } } = useForm(formOptions);

    const changePass = (event) => {
        setPassword({
            [event.target.name]: event.target.value
        })
    }

    const onSubmit = async (data) => {
        try {
            const res = await UserService.changePassword(token, {
                oldPassword: data.oldPassword,
                newPassword: data.newPassword
            });

            showGlobalPopup("Đổi mật khẩu thành công");
            setFlag(false);
        } catch (error) {
            console.log('error:', error);
            setFlag(false);
            showGlobalPopup(error?.response?.data?.message ?? error.message ?? 'Có lỗi xảy ra khi đổi mật khẩu')
        }
    }

    const handlePopup = () => {
        setFlag(!flag);
    }

    return (
        <Profile>
            <div className="user-profile__info">
                <h3 className="user-profile__info--title">
                    Thông tin cá nhân
                </h3>

                <div className="user-profile__info--console">
                    <div className="mobile-console">
                        <div className="user-profile__sesstion">
                            <div className="user-profile__sesstion--image">
                                <img src={customer.photoUrl == null ? "/assets/images/user_profile_unauth.png" : customer.photoUrl} alt="customer" />
                            </div>
                            <div className="user-profile__sesstion--name">
                                <h4>
                                    {customer.name}
                                </h4>
                                <p>
                                    Ngày tạo tài khoản: {formatDate(customer.createdAt)}
                                </p>
                            </div>
                        </div>

                        <div className="user-profile__product-count">
                            <ul>
                                <li>
                                    Số nhà đã theo dõi: {wishlistCount ?? 0}
                                </li>

                                <li>
                                    Số nhà đã yêu thích: {wishlistCount ?? 0}
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                            <p>Họ tên: {customer.name}</p>
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                            <p>Số điện thoại: {customer.phoneNumber?.replace('+84', '0')}</p>
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                            <p>Email: {customer.email}</p>
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-3">
                            <p>Ngày sinh: {customer.dob}</p>
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-3">
                            <p>Giới tính: {customer.gender == "m" ? "Nam" : "Nữ"}</p>
                        </div>

                        {
                            address?.city && <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Tỉnh/Thành phố: {address?.city?.name}</p>
                            </div>
                        }
                        {
                            address?.district && <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p>Quận/Huyện: {address?.district?.name}</p>
                            </div>
                        }
                        {
                            address && <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                <p>Địa chỉ cụ thể: {address.address1}</p>
                            </div>
                        }

                        <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="d-flex flex-wrap align-items-center justify-content-end">
                                <button className="link-update-user change-pass mr-2" onClick={handlePopup}>
                                    Đổi mật khẩu
                                </button>
                                <Link href="/tai-khoan/chinh-sua">
                                    <a className="link-update-user">
                                        Chỉnh sửa
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className={flag == true ? "pop-up-change-pass" : "d-none"}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <h4 className="title">
                        Đổi mật khẩu
                    </h4>

                    <div className="form-box">
                        <input type="password"
                            className="form-control mg_bt0 mg_t15"
                            name="oldPassword"
                            placeholder="Nhập mật khẩu cũ"
                            {...register('oldPassword')}
                        />
                        <span className="error_message">{errors.oldPassword?.message}</span>


                        <input type="password"
                            className="form-control  mg_bt0 mg_t15"
                            name="newPassword"
                            placeholder="Mật khẩu mới"
                            {...register('newPassword')}
                        />
                        <span className="error_message">{errors.newPassword?.message}</span>
                        <input type="password"
                            className="form-control  mg_bt0 mg_t15"
                            name="reType"
                            placeholder="Nhập lại mật khẩu mới"
                            {...register('passwordConfirmation')}
                        />
                        <span className="error_message">{errors.passwordConfirmation?.message}</span>
                        <div className="actions-group  mg_bt0 mg_t15">
                            <button type="button" onClick={handlePopup}>
                                Huỷ
                            </button>

                            <button type="submit">
                                Thay đổi
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </Profile>
    )
}

export async function getServerSideProps({ req, res }) {
    try {
        const customerData = await getCustomerData(res, req, true, true);

        const token = customerData?.session?.user?.accessToken;

        const addressRes = await CustomerAddressService.list(token);

        const addressDefault = addressRes?.addresses?.find(a => a.default) ?? addressRes?.addresses[0];

        return {
            props: {
                address: addressDefault ?? null,
                ...customerData
            }
        }
    } catch (error) {
        console.log('error:', error);
        redirectIfUnauthenticated(undefined, res)
    }
}
Userinfo.auth = true
export default Userinfo;

