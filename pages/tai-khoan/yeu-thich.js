import { getSession } from "next-auth/client";
import Pagination from 'rc-pagination';
import React, { useContext, useEffect, useState } from 'react';
import Profile from '@/components/Authorize/profile';
import WishlistItem from '@/components/Product-item/wishlist-box';
import { getCustomerData, redirectIfUnauthenticated, showGlobalPopup, sleep } from '@/lib/helper';
import ProductService from '@/lib/products/product.service';
import eventEmitter from "@/lib/eventEmitter";
import { CustomerContext } from "@/components/customer_context";

const itemRender = (current, type, element) => {
    if (type === 'page') {
        return <a href={`#${current}`}>{current}</a>;
    }
    return element;
};

const Wishlist = ({ wishlistData, session }) => {
    const [currentPage, setCurrentPage] = useState(1);
    const productPerPages = 3;
    const [wishlist, setWishlist] = useState(wishlistData.product);
    const [totalItem, setTotalItem] = useState(wishlistData.total ?? 0);
    const token = session.user.accessToken;

    useEffect(() => {
        return () => {
            setWishlist([]);
        };
    }, []);

    const customerContext = useContext(CustomerContext);

    const totalDisable = customerContext.totalDisable;

    const handleDeleteWishlist = async (id) => {
        customerContext.setTotalDisable(true)
        try {
            await sleep(500);
            await ProductService.deleteWishList(token, id);

            let skip = 0

            if (totalItem >= productPerPages) {
                if (totalItem % productPerPages === 1) {
                    skip = productPerPages * (currentPage - 2);
                } else {
                    skip = productPerPages * (currentPage - 1);
                }
            } else {
                skip = 0
            }

            setCurrentPage(Math.ceil((totalItem - 1) / productPerPages))

            const getWishlist = await ProductService.getWishList(token, {
                take: productPerPages,
                skip
            })
            setWishlist(getWishlist.product)
            setTotalItem(getWishlist.total);
            localStorage.setItem('ting_tong_wishlist_count', JSON.stringify(getWishlist.total));
            customerContext.setWishlistCount(getWishlist.total);
            await sleep(500)
        } catch (error) {
            showGlobalPopup(error?.response?.data?.message ?? error.message ?? "Có lỗi xảy ra khi bỏ yêu thích tòa nhà");
        }
        customerContext.setTotalDisable(false);
    }

    const pageChange = async (current) => {
        setCurrentPage(current);
        const skip = productPerPages * (current - 1);

        const res = await ProductService.getWishList(token, { take: productPerPages, skip });
        setWishlist(res.product);
    }

    return (
        <Profile>
            <div className="user-profile__info">
                <h3 className="user-profile__info--title">
                    Danh sách yêu thích
                </h3>

                <div className="user-profile__info--console">
                    <div className="user-wishlist">
                        <div className="user-wishlist__list">
                            {
                                wishlist.length === 0 ? (
                                    <h3>Chưa có tòa nhà yêu thích.</h3>
                                ) : (
                                    wishlist.map(item => (
                                        <WishlistItem key={item.id}
                                            wishlist={item}
                                            onClick={handleDeleteWishlist}
                                            totalDisable={totalDisable}
                                        />
                                    ))
                                )
                            }
                            {
                                totalItem / productPerPages > 1 ? (<Pagination total={totalItem} current={currentPage} onChange={pageChange} pageSize={productPerPages} itemRender={itemRender} />) : ""
                            }
                        </div>
                    </div>
                </div>
            </div>
        </Profile>
    );
}

export async function getServerSideProps({ req, res }) {
    try {
        const customerData = await getCustomerData(res, req, undefined, true);
        const token = customerData?.session?.user?.accessToken;
        const wishlistRes = await ProductService.getWishList(token, { take: 3, skip: 0 })

        return {
            props: {
                ...customerData,
                wishlistData: wishlistRes,
                wishlistCount: wishlistRes.total,
            }
        }
    } catch (error) {
        redirectIfUnauthenticated(undefined, res);
    }
}
Wishlist.auth = true
export default Wishlist;