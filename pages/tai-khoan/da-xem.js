import Profile from '@/components/Authorize/profile';
import { getSession } from "next-auth/client";
import Pagination from 'rc-pagination';
import React, { useEffect, useState } from 'react';
import WatchedItem from '@/components/Product-item/watched';
import { getCustomerData, redirectIfUnauthenticated } from '@/lib/helper';
import ProductService from '@/lib/products/product.service';

const itemRender = (current, type, element) => {
    if (type === 'page') {
        return <a href={`#${current}`}>{current}</a>;
    }
    return element;
};

const CustomerWatchedBuidlings = ({ wishlistData, session, customer, cart }) => {
    const [buildings, setBuilding] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [postPerPage, setPostPerPage] = useState(3);
    const productPerPages = 3;
    const totalItem = buildings.length;

    const indexOfLastPost = currentPage * postPerPage;
    const indexOfFirstPost = indexOfLastPost - postPerPage;
    const currentPost = buildings.slice(indexOfFirstPost, indexOfLastPost)

    useEffect(() => {
        try {
            const getWatchedBuildingData = localStorage.getItem('ting_tong_watched_buildings');
            if (!getWatchedBuildingData) {
                localStorage.removeItem('ting_tong_watched_buildings');
                return;
            }

            const watchedBuildings = JSON.parse(getWatchedBuildingData);
            if (Array.isArray(watchedBuildings) && watchedBuildings.length > 0) {
                setBuilding(watchedBuildings)
            }
        } catch (error) {
            console.log('error:', error);
            localStorage.removeItem('ting_tong_watched_buildings');
        }
    }, [])

    const pageChange = async (current) => {
        setCurrentPage(current);
    }

    return (
        <Profile>
            <div className="user-profile__info">
                <h3 className="user-profile__info--title">
                    Danh sách nhà đã xem
                </h3>

                <div className="user-profile__info--console">
                    <div className="user-wishlist">
                        <div className="user-wishlist__list">
                            {
                                buildings.length === 0 ? (
                                    <h3>Chưa có tòa nhà đã xem.</h3>
                                ) : (
                                    currentPost.map(b => (
                                        <WatchedItem key={b.id} item={b}> </WatchedItem>
                                    ))
                                )
                            }
                            {
                                totalItem / productPerPages > 1 ? (<Pagination total={totalItem} current={currentPage} onChange={pageChange} pageSize={productPerPages} itemRender={itemRender} />) : ""
                            }
                        </div>
                    </div>
                </div>
            </div>
        </Profile>
    )
}

export async function getServerSideProps({ req, res }) {
    try {
        const customerData = await getCustomerData(res, req, true, true);
        return {
            props: customerData
        }
    } catch (error) {
        redirectIfUnauthenticated(undefined, res);
    }
}

export default CustomerWatchedBuidlings;
