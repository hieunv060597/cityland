import Authorize from '@/components/Authorize/Authorize';
import HeaderSearch from '@/components/Header/Search';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import { formatDate, formatMoney, getCookieOfReq, getCustomerData, redirectIfUnauthenticated, serverRemoveCookie } from '@/lib/helper';
import OrderService from '@/lib/order/order.service';
import ProductService from '@/lib/products/product.service';
import { getSession } from "next-auth/client";
import Link from "next/link";
import React from 'react';
import CartService from '@/lib/checkout/cart.service';


const OrderDetail = (params) => {
    const { order, session, room, cart, } = params
    const customer = order.customer;

    return (
        <Layout>
            <Navbar>
                <Authorize />
                <HeaderSearch></HeaderSearch>
            </Navbar>


            <main className="user-pages height-auto">
                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>

                                <li className="breadcrumb-item">
                                    <Link href="/tai-khoan/dat-lich">
                                        <a>Danh sách đặt cọc</a>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    {order.code}
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className="user-list-order">
                    <div className="container">
                        <div className="order-info">
                            <div className="order-info__title">
                                <h3>
                                    <p>Đơn đặt lịch </p>
                                    <p>#{order.code}</p>
                                </h3>

                                <ul>
                                    <li>
                                        <span>Ngày tạo đơn:</span>
                                        <span>{formatDate(order.createdAt)}</span>
                                    </li>

                                    <li>
                                        <span>Ngày cập nhật: </span>
                                        <span>{formatDate(order.updatedAt)}</span>
                                    </li>

                                    <li>
                                        <span>Trạng thái: </span>
                                        {
                                            order.cancelledAt ? (
                                                <span className="status status-box cancelled">Đã hủy</span>
                                            ) : (
                                                order.status === 'pending' ? (
                                                    <span className="status status-box pending">Chờ xác nhận</span>
                                                ) : (
                                                    <span className="status status-box success">Đã xác nhận</span>
                                                )
                                            )
                                        }
                                    </li>
                                </ul>
                            </div>
                            <div className="row">
                                <div className="col-12 col-sm-12 col-md-12 col-lg-6">
                                    <div className="order-info__customers">
                                        <h3 className="box-title">Thông tin đặt lịch</h3>

                                        <div className="order-info__customers--info">

                                            {
                                                order.endAt ? (
                                                    <div className="time-meeting">
                                                        <h4>Thời gian hẹn gặp</h4>
                                                        <p>{formatDate(order.endAt, true)}</p>
                                                    </div>
                                                ) : ''
                                            }


                                            <div className="row">
                                                <div className="col-12 col-md-12">
                                                    <p>
                                                        <span>Thông tin cá nhân</span>
                                                    </p>
                                                </div>
                                                {
                                                    customer?.name ? (
                                                        <div className="col-12 col-md-6">
                                                            <p>
                                                                <span>Tên: </span>
                                                                <span>{customer?.name}</span>
                                                            </p>
                                                        </div>
                                                    ) : ''
                                                }
                                                {
                                                    customer?.dob ? (
                                                        <div className="col-12 col-md-6">
                                                            <p>
                                                                <span>Ngày sinh: </span>
                                                                <span>{formatDate(customer.dob)}</span>
                                                            </p>
                                                        </div>

                                                    ) : ''
                                                }
                                                {
                                                    customer?.phoneNumber ? (
                                                        <div className="col-12 col-md-6">
                                                            <p>
                                                                <span>STĐ: </span>
                                                                <span>{customer?.phoneNumber?.replace('+84', '0')}</span>
                                                            </p>
                                                        </div>

                                                    ) : ''
                                                }
                                                {
                                                    customer?.gender ? (
                                                        <div className="col-12 col-md-6">
                                                            <p>
                                                                <span>Giới tính: </span>
                                                                <span>{customer.gender}</span>
                                                            </p>
                                                        </div>
                                                    ) : ''
                                                }
                                                {
                                                    customer?.email ? (
                                                        <div className="col-12 col-md-12">
                                                            <p>
                                                                <span>Email: </span>
                                                                <span>{customer?.email}</span>
                                                            </p>
                                                        </div>
                                                    ) : ''
                                                }
                                                {
                                                    customer?.address ? (
                                                        <div className="col-12 col-md-12">
                                                            <p>
                                                                <span>Địa chỉ: </span>
                                                                <span>{customer?.address}</span>
                                                            </p>
                                                        </div>
                                                    ) : ''
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-12 col-md-12 col-lg-6">
                                    <div className="order-info__rooms">
                                        <h3 className="box-title">Thông tin phòng đã chọn</h3>

                                        <div className="box-content">

                                            <div className="room-info">
                                                <h4 className="room-title">
                                                    <p>Loại phòng: {room?.type}</p>
                                                </h4>
                                                <div className="media">

                                                    {
                                                        Array.isArray(room.images) && room.images.length > 0 ? (
                                                            <img src={room.images[0].source} alt="aa" className="mr-3" />
                                                        ) : (
                                                            <img src="/assets/images/px1.jpg" alt="" className="mr-3" />
                                                        )
                                                    }

                                                    <div className="media-body">
                                                        <div className="media-body__info">
                                                            <h3 className="title">
                                                                <span>Phòng {room?.name}</span>
                                                                <p className="price">
                                                                    {formatMoney(room.minPrice)}/tháng
                                                                </p>
                                                            </h3>

                                                            <p className="area">
                                                                Diện tích: {room?.floorArea}m2
                                                            </p>
                                                        </div>

                                                        <div className="media-body__utilities">
                                                            <p>
                                                                Tiện ích ({room?.services?.filter(s => s.type !== 'Nội quy').length ?? 0})
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="list-utilities">
                                                    <div className="row">
                                                        {
                                                            room?.services?.filter(s => s.type !== 'Nội quy')?.map(s => (
                                                                <div key={s.id} className="col-6 col-sm-6 col-md-6 col-lg-4">
                                                                    <div className="utilities-item">
                                                                        {
                                                                            Array.isArray(s.images) && s.images.length > 0 ? (
                                                                                < img src={s.images[0].source} alt={s.name} />
                                                                            ) : (
                                                                                <img src="/assets/images/lo1.svg" alt="s.name" />
                                                                            )
                                                                        }
                                                                        <span>{s.name}</span>
                                                                    </div>
                                                                </div>
                                                            ))
                                                        }
                                                    </div>
                                                </div>

                                                <div className="total-money">
                                                    <h4>Số ngày giữ chỗ: 5 ngày</h4>
                                                    <ul>
                                                        <li>
                                                            Ngày hết hạn đặt cọc: 15/05/2021
                                                        </li>

                                                        <li>
                                                            Tổng tiền cọc: {formatMoney(order.totalAmount)}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="payment-methods">
                                            <div className="payment-methods__name">
                                                <p>Phương thức thanh toán</p>
                                                <p>{order.paymentMethod.name}</p>
                                            </div>
                                        </div>

                                        <div className="payment-methods">
                                            <h4 className="title">
                                                Thông tin chuyển khoản
                                            </h4>

                                            <ul>
                                                {
                                                    order.paymentMethod.description?.split('####')?.map((d, idx) => (
                                                        <li key={idx} className="mg_y10 dp_b">
                                                            {d}
                                                        </li>
                                                    ))
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
    )
}

export async function getServerSideProps({ params, req, res }) {
    const { id } = params;
    if (!id) {
        // redirectIfUnauthenticated(undefined, res);
    }
    try {
        const customerData = await getCustomerData(res, req, true, true);
        const token = customerData?.session?.user?.accessToken;
        const orderRes = await OrderService.get(id, token);
        const room = orderRes.order?.items[0]?.variant?.product;
        const data = {
            props: {
                order: orderRes.order,
                ...customerData,
            }
        };

        if (room) {
            const reloadRoom = await ProductService.getRoom(room.id);
            data.props.room = reloadRoom.room;
        }
        return data;
    } catch (error) {
        console.log('error:', error);
        // redirectIfUnauthenticated(undefined, res);
        return { props: {} }
    }
}

export default OrderDetail;