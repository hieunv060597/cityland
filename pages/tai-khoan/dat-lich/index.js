import Profile from '@/components/Authorize/profile';
import { formatDate, formatMoney, getCustomerData, redirectIfUnauthenticated } from '@/lib/helper';
import OrderService from '@/lib/order/order.service';
import ProductService from '@/lib/products/product.service';
import UserService from '@/lib/users/user.service';
import { getSession } from "next-auth/client";
import Link from "next/link";
import Pagination from 'rc-pagination';
import React, { useState } from 'react';


const CustomerListOrder = ({ orderData, session }) => {
    const [orders, setOrder] = useState(orderData.orders);
    const [total, setTotal] = useState(orderData.total);
    const [currentPage, setCurrentPage] = useState(1);
    const productPerPages = 3;
    const token = session?.user?.accessToken;


    const itemRender = (current, type, element) => {
        if (type === 'page') {
            return <a href={`#pages${current}`}>{current}</a>;
        }
        return element;
    };

    const listOrder = async (params) => {
        try {
            const res = await OrderService.list(params, token);
            setTotal(res.total);
            const newOrders = res.orders.map(o => {
                const room = o?.items[0]?.variant?.product;
                return {
                    ...o,
                    room
                }
            })
            setOrder(newOrders)
        } catch (error) {
            console.log('errorl', error);
        }
    }

    const pageChange = async (current) => {
        setCurrentPage(current);
        const take = productPerPages;
        const skip = productPerPages * (current - 1);
        await listOrder({ skip, take });
    }

    return (
        <Profile>
            <div className="user-profile__info">
                <h3 className="user-profile__info--title">
                    Danh sách đặt lịch
                </h3>

                <div className="user-profile__info--console">
                    <div className="user-list-order">
                        {
                            orders.length === 0 ? (
                                <h3>Chưa có lịch hẹn</h3>
                            ) : ''
                        }

                        {
                            orders.map(o => (

                                <div className="order-box mg_bt10" key={o.id}>
                                    <div className="row">
                                        <div className="col-12 col-md-9">
                                            <div className="order-box__left">
                                                <div className="order-box__left--images">
                                                    {
                                                        Array.isArray(o.room?.images) && o.room?.images.length > 0 ? (
                                                            <>
                                                                <img src={o.room?.images[0].source} alt="loading" />
                                                            </>
                                                        ) : (
                                                            <>
                                                                <img src="/assets/images/no-img.jpg" alt="loading" />
                                                            </>
                                                        )
                                                    }
                                                </div>

                                                <div className="order-box__left--text">
                                                    <span className="order-code">
                                                        {o.code}
                                                    </span>

                                                    <div className="order-status">
                                                        <span className="room-picked">{o.room?.name}</span>
                                                        {
                                                            o.cancelledAt ? (
                                                                <span className="status-box cancelled">Đã hủy</span>
                                                            ) : (
                                                                o.status === 'pending' ? (
                                                                    <span className="status-box pending">Chờ xác nhận</span>
                                                                ) : (
                                                                    <span className="status-box success">Đã xác nhận</span>
                                                                )
                                                            )
                                                        }
                                                    </div>

                                                    <ul className="room-info">
                                                        <li>Tiền cọc: {formatMoney(o.totalAmount)}</li>
                                                        <li>Diện tích: {o.room?.floorArea}m2</li>
                                                        <li>Tính năng: #</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12 col-md-3">
                                            <div className="order-box__right">
                                                <span className="time">
                                                    {formatDate(o.createdAt, true)}
                                                </span>

                                                <Link href={{
                                                    pathname: '/tai-khoan/dat-lich/[id]',
                                                    query: { id: o.id },
                                                }}>
                                                    <a className="see-detail">
                                                        Chi tiết
                                                    </a>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            ))
                        }

                        {
                            total / productPerPages > 1 ? (<Pagination total={total} current={currentPage} onChange={pageChange} pageSize={productPerPages} itemRender={itemRender} />) : ""
                        }


                    </div>
                </div>
            </div>
        </Profile>
    )
}

export async function getServerSideProps({ req, res }) {
    try {
        const customerData = await getCustomerData(res, req, true, true);
        const token = customerData?.session?.user?.accessToken;
        const orderRes = await OrderService.list({ take: 3, skip: 0 }, token);

        const newOrders = orderRes.orders.map(o => {
            const room = o?.items[0]?.variant?.product;
            return {
                ...o,
                room
            }
        });

        return {
            props: {
                orderData: {
                    orders: newOrders,
                    total: orderRes.total,
                },
                ...customerData
            }
        }
    } catch (error) {
        console.log('errorl', error);
        redirectIfUnauthenticated(undefined, res);
    }
}
CustomerListOrder.auth = true
export default CustomerListOrder;