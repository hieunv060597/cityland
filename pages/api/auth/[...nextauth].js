import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import axios from 'axios';

const baseUrl = 'https://api.dev.318.suplo.vn/v1';

const options = {
    providers: [
        Providers.Credentials({
            id: 'email',
            name: "login with email",
            credentials: {
                email: {
                    type: "email",
                },
                password: {
                    type: "text",
                }
            },
            async authorize(credentials) {
                try {
                    const url = '/auth/customer/signin?domain=tingtong';
                    const response = await axios.post(baseUrl + url, credentials);
                    return response.data;
                } catch (error) {
                    const message = error?.response?.data?.message ?? 'Có lỗi xảy ra'
                    return Promise.reject(new Error(message))
                }
            }
        }),

        Providers.Credentials({
            id: "login_with_exchange_token",
            name: "login with exchange token",
            credentials: {
                idToken: {
                    type: 'text',
                    require: true,
                }
            },
            async authorize(data) {
                try {
                    const url = '/auth/customer/exchange?domain=tingtong';
                    const response = await axios.post(baseUrl + url, {
                        idToken: data.idToken
                    });
                    return response.data;
                } catch (error) {
                    const message = error?.response?.data?.message ?? 'Có lỗi xảy ra'
                    return Promise.reject(new Error(message))
                }
            }
        }),
        Providers.Credentials({
            id: "signup",
            name: "signup with data input",
            credentials: {
                email: {
                    type: 'text',
                    require: true,
                },
                phoneNumber: {
                    type: 'text',
                },
                password: {
                    type: 'text',
                    require: true,
                },
                name: {
                    type: 'text',
                },
                dob: {
                    type: 'text',
                },
                gender: {
                    type: 'text',
                }
            },
            async authorize(data) {
                try {
                    const url = '/auth/customer/signup?domain=tingtong';
                    const response = await axios.post(baseUrl + url, data);
                    return response.data;
                } catch (error) {
                    const message = error?.response?.data?.message ?? 'Có lỗi xảy ra'
                    return Promise.reject(new Error(message))
                }
            }
        })
    ],

    session: {
        jwt: true
    },

    pages: {
        signIn: '/dang-nhap',
        signOut: '/auth/signout',
        error: false, // Error code passed in query string as ?error=
        verifyRequest: '/auth/verify-request', // (used for check email message)
        newUser: '/auth/new-user' // New users will be directed here on first sign in (leave the property out if not of interest)
    },

    callbacks: {
        async signIn(...props) {
            return true
        },
        async redirect(url, baseUrl) {
            return baseUrl
        },
        async session(session, user) {
            session.user = user.user
            return Promise.resolve(session)
        },
        async jwt(token, user) {
            user && (token.user = user)
            return Promise.resolve(token)
        }
    },
}

export default (req, res) => NextAuth(req, res, options);

