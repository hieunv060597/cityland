import Layout from "../components/Layout/Layout";
import Navbar from "../components/Navbar/Navbar";
import Authorize from "../components/Authorize/Authorize";
import Footer from "../components/footer";
import Link from "next/link";

export default function Custom404() {


    return (
        <Layout>
            <Navbar>
                <Authorize />
            </Navbar>
            <div className="error-pages">
                <h1>404</h1>
                <p>Không tìm thấy trang bạn yêu cầu</p>
                <Link href="/">
                    <a className="button">Quay về trang chủ</a>
                </Link>
            </div>
            <Footer />
        </Layout>
    )
}

