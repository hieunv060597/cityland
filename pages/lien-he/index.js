import React, { useState, useReducer, useContext } from "react";
import Authorize from '@/components/Authorize/Authorize';
import Footer from '@/components/footer';
import HeaderSearch from '@/components/Header/Search';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import { getSession } from "next-auth/client";
import Link from "next/link";
import { getCustomerData } from '@/lib/helper';

const Contact = ({ session, cart, customer }) => {

    return (
        <Layout>
            <Navbar>
                <Authorize session={session} cart={cart} customer={customer} />
                <HeaderSearch></HeaderSearch>
            </Navbar>

            <main>
                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Liên hệ
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className="main-contact">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-4 order-2 order-lg-1">
                                <div className="contact-social">
                                    <img src="/assets/images/contact.jpg" alt="" />
                                    <div className="contact-social__link">
                                        <ul>
                                            <li>
                                                <Link href="#">
                                                    <a>
                                                        <i className="fa fa-twitter"></i>
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link href="https://www.facebook.com/Tingtongvietnam">
                                                    <a>
                                                        <i className="fa fa-facebook"></i>
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link href="https://www.youtube.com/channel/UCqXBz2mLllZwBEBgSkFsDOQ">
                                                    <a>
                                                        <i className="fa fa-youtube-play"></i>
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link href="#">
                                                    <a>
                                                        <i className="fa fa-google"></i>
                                                    </a>
                                                </Link>
                                            </li>
                                        </ul>

                                        <p>Trên các nền tảng mạng xã hội</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-12 col-sm-12 col-md-12 col-lg-8 order-1 order-lg-2">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="contact-title">
                                            <h1>Liên hệ với chúng tôi</h1>
                                            <p>TingTong.vn là công ty có nhiều năm kinh nghiệm trong lĩnh vực thuê và cho thuê nhà giá tốt nhất thị trường.</p>
                                        </div>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <form className="contact-form">
                                            <div className="input-box">
                                                <input type="text" className="form-control" placeholder="Họ và tên" required />
                                            </div>

                                            <div className="input-box">
                                                <input type="text" className="form-control" placeholder="Số điện thoại" required />
                                            </div>

                                            <div className="input-box">
                                                <input type="email" className="form-control" placeholder="Email" required />
                                            </div>

                                            <div className="input-box">
                                                <textarea className="form-control" placeholder="Mong muốn của bạn là gì ?" rows="7"></textarea>
                                            </div>

                                            <div className="input-box">
                                                <button className="btn-submit">
                                                    Gửi tin nhắn
                                                </button>
                                            </div>
                                        </form>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <div className="list-contact">
                                            <div className="images">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.1228206993746!2d105.81464071530911!3d21.027771093187038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab6e066c6bf7%3A0xa4faabed97b52a1!2zNiBOZ3V54buFbiBDw7RuZyBIb2FuLCBHaeG6o25nIFbDtSwgQmEgxJDDrG5oLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1634874858148!5m2!1svi!2s" width="100%" height="400" allowFullScreen={true} loading="lazy"></iframe>
                                            </div>

                                            <ul>
                                                <li>
                                                    <Link href="mailto: tapdoantingtong@gmail.com">
                                                        tapdoantingtong@gmail.com
                                                    </Link>
                                                </li>

                                                <li>
                                                    <Link href="tel: 0246286222">
                                                        024 628 6222
                                                    </Link>
                                                </li>

                                                <li>
                                                    <Link href="#">
                                                        139 Cầu Giấy Quan Hoa, Cầu Giấy, Hà Nội
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
            <Footer />
        </Layout>
    )
}

export async function getServerSideProps({ req, res, }) {
    try {
        const customerData = await getCustomerData(res, req, true);

        return {
            props: customerData
        };
    } catch (error) {
        return {
            props: {}
        }
    }
}

export default Contact;