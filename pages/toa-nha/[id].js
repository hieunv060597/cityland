
import HeaderSearch from '@/components/Header/Search';
import ProductItem from '@/components/Product-item/product-box';
import ProductDetailPopup from '@/components/ProductDetail/popup';
import ProductDetailRenderImages from '@/components/ProductDetail/render-images';
import ProductDetailSampleRoom from '@/components/ProductDetail/sample-room';
import CartService from '@/lib/checkout/cart.service';
import "@fancyapps/ui/dist/fancybox.css";
import Link from "next/link";
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useRef, useState } from 'react';
import SwiperCore, {
    Navigation, Pagination, Thumbs
} from 'swiper';
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/thumbs";
import { Swiper, SwiperSlide } from "swiper/react";
import Authorize from '@/components/Authorize/Authorize';
import Footer from '@/components/footer';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import { convertCityName, convertDistrictName, formatMoney, getCustomerData, setClientCookie, showGlobalPopup } from '@/lib/helper';
import ProductService from '@/lib/products/product.service';
import { Fancybox, Carousel, Panzoom } from "@fancyapps/ui";
// cant not remove import { Fancybox, Carousel, Panzoom } from "@fancyapps/ui";

SwiperCore.use([Navigation, Thumbs, Pagination]);

const ProductDetail = (params) => {
    const {
        building,
        services,
        sameAreaBuildings,
        sampleRooms,
        rules,
        devices,
        nearbyAreas,
    } = params;

    const [thumbsSwiper, setThumbsSwiper] = useState(null);
    const [popup, setPopup] = useState(false);
    const [roomPicked, setRoomPicked] = useState(null);
    const [roomType, setType] = useState('');
    const [floors, setFloor] = useState([]);
    const router = useRouter();
    const [rooms, setRoom] = useState(building.rooms);
    const [sampleRoom, setSampleRoom] = useState(null);
    const [sameTypeOfRooms, setSameTypeOfRooms] = useState([]);
    const [wishlistIds, setWishlistId] = useState(params.wishlistIds ?? [])
    const [areaBuildings, setSameAreaBuildings] = useState(sameAreaBuildings ?? [])
    const iframeEle = useRef(null);

    useEffect(() => {
        accordion();
        smoothScroll();
        try {
            const getWatchedBuildingData = localStorage.getItem('ting_tong_watched_buildings');
            if (!getWatchedBuildingData) {
                localStorage.setItem('ting_tong_watched_buildings', JSON.stringify([building]));
                return;
            }
            const watchedBuildings = JSON.parse(getWatchedBuildingData);
            const newWatchedBuildings = watchedBuildings.filter(b => b.id !== building.id);
            localStorage.setItem('ting_tong_watched_buildings', JSON.stringify([building, ...newWatchedBuildings]))
        } catch (error) {
            localStorage.removeItem('ting_tong_watched_buildings');
        }
        return () => {
            document.body.style.overflowY = "auto";
        }
    }, []);

    const accordion = () => {
        var btn = document.querySelectorAll(".accordion");
        if (btn == null) {
            return 0;
        } else {
            for (var i = 0; i < btn.length; i++) {
                btn[i].addEventListener("click", function () {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (this.classList[1] == "active") {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    } else {
                        panel.style.maxHeight = null;
                    }
                })
                var panel = btn[i].nextElementSibling;
                if (btn[i].classList[1] == "active") {
                    panel.style.maxHeight = (panel.scrollHeight + 80) + "px";
                } else {
                    panel.style.maxHeight = null;
                }
            }
        }
    }

    const smoothScroll = () => {
        const easeInCubic = function (t) { return t * t * t }
        const scrollElem = document.querySelector('#map_picked');


        const scrollToElem = (start, stamp, duration, scrollEndElemTop, startScrollOffset) => {
            //debugger;
            const runtime = stamp - start;
            let progress = runtime / duration;
            const ease = easeInCubic(progress);

            progress = Math.min(progress, 1);
            console.log(startScrollOffset, startScrollOffset + (scrollEndElemTop * ease));

            const newScrollOffset = startScrollOffset + (scrollEndElemTop * ease);
            window.scroll(0, startScrollOffset + (scrollEndElemTop * ease));

            if (runtime < duration) {
                requestAnimationFrame((timestamp) => {
                    const stamp = new Date().getTime();
                    scrollToElem(start, stamp, duration, scrollEndElemTop, startScrollOffset);
                })
            }
        }

        scrollElem.addEventListener('click', function (e) {
            e.preventDefault();
            const scrollElemId = e.target.href.split('#')[1];
            const scrollEndElem = document.getElementById(scrollElemId);

            const anim = requestAnimationFrame(() => {
                const stamp = new Date().getTime();
                const duration = 750;
                const start = stamp;

                const startScrollOffset = window.pageYOffset;

                const scrollEndElemTop = scrollEndElem.getBoundingClientRect().top;

                scrollToElem(start, stamp, duration, scrollEndElemTop, startScrollOffset);
                // scrollToElem(scrollEndElemTop);
            })
        })

    }

    const openPopup = (id) => {
        const sampleRoom = sampleRooms.find(r => r.id === id);
        if (!sampleRoom) {
            alert('Không thể  tìm thấy phòng mẫu');
            return
        }
        setSampleRoom(sampleRoom);
        setType(sampleRoom.type);

        const sameTypeOfRooms = rooms.filter(r => r.type.toLowerCase() === sampleRoom.type.toLowerCase());
        setSameTypeOfRooms(sameTypeOfRooms);

        const newFloors = [...new Set(sameTypeOfRooms.map(r => r.floor))];
        setFloor(newFloors);

        const idx = sameTypeOfRooms.findIndex(r => r.id === sampleRoom.id);
        const newRooms = rooms.map(r => ({ ...r, picked: false }))
        newRooms[idx].picked = true;
        document.body.style.overflowY = 'hidden'

        setRoomPicked(sampleRoom);
        setRoom(newRooms);
        setPopup(true);
    }

    const closePopup = () => {
        document.body.style.overflowY = 'auto'
        setPopup(false)
    }

    const roomClicked = (roomId, variantId, picked) => {
        setThumbsSwiper(null);
        const idx = rooms.findIndex(r => r.id === roomId);
        if (idx === -1) {
            alert('Không tìm thấy phòng đã chọn');
            return null;
        }
        const newRooms = rooms.map(r => ({
            ...r,
            picked: false
        }))
        newRooms[idx].picked = picked;
        setRoomPicked(newRooms[idx]);
        setRoom(newRooms);
    }

    const checkOut = async () => {
        const roomPicked = rooms.find(r => r.picked);
        if (!roomPicked) {
            alert("vui lòng chọn 1 phòng");
            return;
        }

        const variant = roomPicked.variants[0];
        if (!variant) {
            alert("không tìm thấy phòng")
            return;
        }

        try {
            const res = await CartService.addItem(variant.id, 0);

            setClientCookie('ting_tong_cart_token', res.token, 30)
            setClientCookie('ting_tong_building_id', building.id, 30)
        } catch (error) {
            return showGlobalPopup(error?.response?.data?.message ?? error.message ?? "Có lỗi xảy ra.");
        }

        router.push("/dat-phong");
    }

    return (
        <Layout>
            <Navbar>
                <Authorize />
                <HeaderSearch></HeaderSearch>
            </Navbar>

            <main className="main-detail">
                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>
                                {
                                    building?.address?.city ?
                                        <li className="breadcrumb-item">
                                            <Link href={`/tim-kiem?city=${building?.address?.city?.codename}`}>
                                                <a target="_blank">Khu vực {convertCityName(building?.address?.city?.name)}</a>
                                            </Link>
                                        </li>
                                        : ''
                                }
                                {
                                    building?.address?.district ?
                                        <li className="breadcrumb-item">
                                            <Link href={`/tim-kiem?city=${building?.address?.city?.codename}&district=${building?.address?.district?.codename}`}>
                                                <a target="_blank">{convertDistrictName(building?.address?.district?.name)}</a>
                                            </Link>
                                        </li>
                                        : ''
                                }

                                <li className="breadcrumb-item active" aria-current="page">
                                    {building?.name}
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className="product-detail">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-8 order-2 order-md-2 order-lg-1">
                                <div className="product-detail__basic d-none d-sm-none d-md-none d-lg-block">
                                    <div className="product-detail__basic--title">
                                        <ul className="title">
                                            <li key="1">
                                                {building?.name}
                                            </li>
                                            <li key="2">
                                                <Link href="#map">
                                                    <a id="map_picked">
                                                        {building?.address?.address1}, {convertDistrictName(building?.address?.district?.name)}, {convertCityName(building?.address?.city?.name)}
                                                    </a>
                                                </Link>

                                            </li>
                                        </ul>

                                        <div className="review">
                                            <div className="review-box">
                                                <i className="fa fa-picture-o"></i>
                                                <span>
                                                    {building.images?.length ?? "0"}
                                                </span>
                                            </div>

                                            <div className="review-box">
                                                <i className="fa fa-eye"></i>
                                                <span>{building?.viewCount}</span>
                                            </div>

                                            <div className="review-box">
                                                <i className="fa fa-star"></i>
                                                <span>{building?.rating ?? '0'}</span>
                                            </div>
                                        </div>
                                    </div >
                                </div >
                                <div className="product-detail__galerry mt-2 d-none d-sm-none d-md-none d-lg-block">
                                    <ProductDetailRenderImages images={building?.images}></ProductDetailRenderImages>
                                </div>
                                <div className="product-detail__descript">
                                    <h4 className="product-detail__descript--title">
                                        Mô tả
                                    </h4>

                                    <div className="product-detail__descript--content" dangerouslySetInnerHTML={{ __html: building?.description }}>
                                    </div>

                                </div>
                                <div className="product-detail__list-type">
                                    <div className="title">
                                        <h4>Loại phòng ({sampleRooms?.length})</h4>
                                    </div>
                                    <div className="list">
                                        {
                                            sampleRooms?.map(r => (
                                                <ProductDetailSampleRoom key={r.id} sampleRoom={r} openPopup={openPopup} />
                                            ))
                                        }
                                    </div>
                                </div>
                                {
                                    building?.mapUrl && (
                                        <div className="product-detail__map" id="map">
                                            <h4 className="product-detail__map--title">
                                                Bản đồ vị trí
                                            </h4>
                                            <div className="product-detail__map--content">
                                                <iframe target="_parent" id="google_map" ref={iframeEle} src={building?.mapUrl} width="100%" height="450" loading="lazy"></iframe>
                                            </div>
                                        </div>
                                    )
                                }

                                {
                                    areaBuildings?.length > 0 ?
                                        (
                                            <>
                                                <div className="product-related">
                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div className="title">
                                                                <p>Bất động sản cùng khu vực</p>
                                                            </div>

                                                            {areaBuildings.length >= 3 ? (
                                                                <>
                                                                    <Swiper slidesPerView={3} spaceBetween={30} loop={false} pagination={{
                                                                        "dynamicBullets": true
                                                                    }} navigation={false} autoplay={{
                                                                        "delay": 5000
                                                                    }}
                                                                        breakpoints={{
                                                                            "300": {
                                                                                "slidesPerView": 1,
                                                                            },
                                                                            "768": {
                                                                                "slidesPerView": 2,
                                                                            },
                                                                            "886": {
                                                                                "slidesPerView": 2,
                                                                            },
                                                                            "1024": {
                                                                                "slidesPerView": 2,
                                                                            },
                                                                            "1368": {
                                                                                "slidesPerView": 2,
                                                                            }
                                                                        }}
                                                                        className="mySwiper">
                                                                        {
                                                                            areaBuildings.map(s => (
                                                                                <SwiperSlide key={s.id}>
                                                                                    <ProductItem key={s.id} product={s}></ProductItem>
                                                                                </SwiperSlide>

                                                                            ))
                                                                        }

                                                                    </Swiper>
                                                                </>
                                                            ) : (
                                                                <div className="row">
                                                                    {
                                                                        areaBuildings.map(s => (
                                                                            <div className="col-12 col-sm-12 col-md-6 col-lg-6" key={s.id}>
                                                                                <ProductItem key={s.id} product={s}></ProductItem>
                                                                            </div>
                                                                        ))
                                                                    }
                                                                </div>
                                                            )}
                                                        </div>
                                                    </div>
                                                </div>
                                            </>
                                        ) : ''
                                }
                            </div >

                            <div className="col-12 col-sm-12 col-md-12 col-lg-4 order-1 order-md-1 order-lg-2">
                                <div className="product-detail__basic d-block d-sm-block d-md-block d-lg-none">
                                    <div className="product-detail__basic--title">
                                        <ul className="title">
                                            <li>
                                                {building?.name}
                                            </li>

                                            {/* <li>
                                                {data.address.address1}
                                            </li> */}
                                        </ul>

                                        <div className="review">
                                            <div className="review-box">
                                                <i className="fa fa-picture-o"></i>
                                                <span>
                                                    {building.images?.length ?? "0"}
                                                </span>
                                            </div>

                                            <div className="review-box">
                                                <i className="fa fa-eye"></i>
                                                <span>{building?.viewCount}</span>
                                            </div>

                                            <div className="review-box">
                                                <i className="fa fa-star"></i>
                                                <span>{building?.rating ?? 0}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="product-detail__galerry d-block d-sm-block d-md-block d-lg-none">
                                    <ProductDetailRenderImages images={building?.images}></ProductDetailRenderImages>
                                </div>

                                <div className="product-detail__sidebar">

                                    <div className="sidebar-box">
                                        <button className="accordion active">
                                            Thông tin căn nhà
                                        </button>

                                        <div className="panel">
                                            <div className="info">
                                                <div className="title">
                                                    Loại nhà: <span>{building?.type}</span>
                                                </div>

                                                <div className="info-box">
                                                    <ul className="row">
                                                        <li key="1" className="col-4">
                                                            <div className="info-detail">
                                                                <p>Diện tích</p>
                                                                <p>
                                                                    <img src="/assets/images/dtic1.svg" alt="" />
                                                                    <span>{building?.minRoomArea}-{building?.maxRoomArea}m2</span>
                                                                </p>
                                                            </div>
                                                        </li>

                                                        <li key="2" className="col-4">
                                                            <div className="info-detail">
                                                                <p>Số phòng</p>
                                                                <p>
                                                                    <img src="/assets/images/dtic2.svg" alt="" />
                                                                    <span>{rooms.length}</span>
                                                                </p>
                                                            </div>
                                                        </li>

                                                        <li key="3" className="col-4">
                                                            <div className="info-detail">
                                                                <p>Trạng thái</p>

                                                                {
                                                                    rooms.filter(r => !r.availableOn).length > 0 ? (
                                                                        <p>
                                                                            <b className="avaiable">Có thể thuê</b>
                                                                        </p>
                                                                    ) : (
                                                                        <p>
                                                                            <b className="avaiable">Không thể thuê</b>
                                                                        </p>
                                                                    )
                                                                }

                                                            </div>
                                                        </li>

                                                        <li key="4" className="col-4">
                                                            <div className="info-detail">
                                                                <p>Phòng trống</p>
                                                                <p>
                                                                    <img src="/assets/images/dtic3.svg" alt="" />
                                                                    <span>{rooms.filter(r => !r.availableOn).length}</span>
                                                                </p>
                                                            </div>
                                                        </li>

                                                        <li key="5" className="col-4">
                                                            <div className="info-detail">
                                                                <p>Số tầng</p>
                                                                <p>
                                                                    <img src="/assets/images/dtic4.svg" alt="" />
                                                                    <span>{building?.floorCount}</span>
                                                                </p>
                                                            </div>
                                                        </li>

                                                        <li key="6" className="col-4">
                                                            <div className="info-detail">
                                                                <p>Chỗ để xe</p>
                                                                <p>
                                                                    <img src="/assets/images/dtic5.svg" alt="" />
                                                                    <span>2</span>
                                                                </p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div className="utilities">
                                                <div className="wrap-title">
                                                    <div className="title">
                                                        <span>Tiện ích: </span>
                                                        <span>{devices.length}</span>
                                                    </div>
                                                </div>
                                                <ul className="row">
                                                    {
                                                        Array.isArray(devices) && devices.length > 0 ? (
                                                            devices.map(s => (
                                                                < li key={s.id} className="col-6">
                                                                    {
                                                                        Array.isArray(s.images) && s.images.length > 0 ? (
                                                                            <img src={s.images[0].source} alt={s.name} />
                                                                        ) : (<img src="/assets/images/no-img.jpg" alt={s.name} />)
                                                                    }
                                                                    <span>{s.name}</span>
                                                                </li>
                                                            ))
                                                        ) : ''
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    {
                                        Array.isArray(rules) || rules?.length >= 0 ? (
                                            <div className="sidebar-box">
                                                <button className="accordion active">
                                                    Nội quy
                                                </button>

                                                <div className="panel">
                                                    <div className="rules">
                                                        <ul className="row">
                                                            {
                                                                rules.filter(s => s.type == "Nội quy").map(s => (
                                                                    < li key={s.id} className="col-12">
                                                                        {
                                                                            Array.isArray(s.images) && s.images.length > 0 ? (
                                                                                <img src={s.images[0].source} alt={s.name} />
                                                                            ) : (<img src="/assets/images/no-img.jpg" alt={s.name} />)
                                                                        }
                                                                        <span>{s.name}</span>
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }

                                    {
                                        Array.isArray(services) && services.length > 0 ? (
                                            <div className="sidebar-box">
                                                <button className="accordion active">
                                                    Thông tin dịch vụ
                                                </button>

                                                <div className="panel">
                                                    <div className="service">
                                                        <ul>
                                                            {
                                                                services.map(s => (
                                                                    <li key={s.id}>
                                                                        <span>{s.name} ({s.unit})</span>
                                                                        <span>{s.minPrice > 0 ? formatMoney(s.minPrice) : 'Theo hợp đồng'}</span>
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }

                                    {
                                        Array.isArray(nearbyAreas) && nearbyAreas.length > 0 ? (
                                            <div className="sidebar-box">
                                                <button className="accordion active">
                                                    Khu vực gần đây
                                                </button>
                                                <div className="panel">
                                                    <div className="service">
                                                        <ul>
                                                            {
                                                                nearbyAreas.map(s => (
                                                                    <li key={s.id} className="dp_fl jc_fs ai_fs mg_y10">
                                                                        <div className="dp_fl jc_fs ai_c ">
                                                                            <img src="/assets/images/icon/location.svg" />
                                                                        </div>
                                                                        <div className="dp_fl jc_fs ai_c fw_w mg_l5">
                                                                            <span className="dp_fl width100">{s.location}</span>
                                                                            <span className="dp_fl width100">({s.km})</span>
                                                                        </div>
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                            </div>
                        </div >
                    </div >
                </section >



                {
                    roomPicked ? <ProductDetailPopup
                        floors={floors}
                        rooms={rooms}
                        popup={popup}
                        roomPicked={roomPicked}
                        closePopup={closePopup}
                        roomType={roomType}
                        checkOut={checkOut}
                        roomClicked={roomClicked}
                        thumbsSwiper={thumbsSwiper}
                        setThumbsSwiper={setThumbsSwiper}
                        sameTypeOfRooms={sameTypeOfRooms}
                        sampleRoom={sampleRoom}
                    ></ProductDetailPopup> : ''
                }

            </main >

            <Footer />
        </Layout >

    )
}


export async function getServerSideProps({ req, res, params }) {
    const { id } = params;

    if (!id) {
        return {
            props: {}
        }
    }

    try {
        const customerData = await getCustomerData(res, req, true);

        const buildingRes = await ProductService.getBuilding(id);

        const description = buildingRes?.building?.description;

        let nearbyAreas = description?.split('****');

        buildingRes.building.description = nearbyAreas.shift();
        nearbyAreas = nearbyAreas.reduce((p, c) => {
            const data = c.split('(');
            const location = data[0];
            const km = data[1]?.replace(')', '');
            p.push({
                location: location ?? null,
                km: km ?? null,
            })
            return p
        }, []);

        const listBuildingSameArea = await ProductService.listBuilding({
            district: buildingRes.building.address.district.codename
        });
        const sameAreaBuildings = listBuildingSameArea.buildings.filter(b => b.id !== buildingRes.building.id).map(p => ({
            ...p,
            liked: customerData.wishlistIds.includes(p.id)
        }));

        const rooms = buildingRes.building.rooms;
        const sampleRooms = [];
        const nameTypes = [...new Set(rooms.map(r => r.type))];
        if (!nameTypes.length === 0) {
            alert('error cant not get room type');
            return {
                props: {}
            }
        }

        for (const nameType of nameTypes) {
            let sampleRoomOfType = rooms.find(r => nameType === r.type && r.tags?.includes('Phòng mẫu'));
            if (sampleRoomOfType) {
                sampleRooms.push(sampleRoomOfType);
                continue;
            }
            sampleRoomOfType = rooms.find(r => nameType === r.type);
            if (sampleRoomOfType) {
                sampleRooms.push(sampleRoomOfType);
            }
        }

        var uniq = {};

        const services = rooms.reduce((p, c) => p.concat(c.services), []).concat(buildingRes.building.services).filter(r => !uniq[r.id] && (uniq[r.id] = true)).filter((r) => r.type !== 'Tài sản');

        const newServices = services.filter(r => r.type === 'Dịch vụ');
        const rules = services.filter(r => r.type === 'Nội quy');
        const devices = services.filter(r => r.type === 'Thiết bị');

        return {
            props: {
                building: buildingRes.building,
                sampleRooms,
                rules: rules ?? [],
                devices,
                sameAreaBuildings,
                services: newServices,
                nearbyAreas: nearbyAreas ?? [],
                ...customerData,
            }
        }
    } catch (error) {
        console.log('error:', error);

        return {
            props: {}
        }
    }
}

export default ProductDetail;
