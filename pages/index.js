
import Authorize from "@/components/Authorize/Authorize";
import Footer from "@/components/footer";
import SearchHome from '@/components/Home/section_0--search/search-home';
import MainSlider from "@/components/Home/section_1--slider/slider";
import HomeContact from '@/components/Home/section_10--contact-form/home-contact';
import Utilities from "@/components/Home/section_2--utilities/Utilities";
import ListProduct from '@/components/Home/section_4--product1/Listproducts';
import Achievement from '@/components/Home/section_5--achievement/Achievement';
import Caring from '@/components/Home/section_6--caringarea/Caring';
import Discover from '@/components/Home/section_7--discover/Discover';
import HomeBlogs from '@/components/Home/section_8--blogs/blogs';
import LogoBrand from '@/components/Home/section_9--logo-brand/logobrand';
import Layout from "@/components/Layout/Layout";
import Navbar from "@/components/Navbar/Navbar";
import BlogService from "@/lib/blog/blog.service";
import { getCustomerData } from '@/lib/helper';
import LocationService from "@/lib/location/location.service";
import ProductService from '@/lib/products/product.service';
import Link from "next/link";
import React from 'react';

export default function Home({ buildings, articles, services, cities, houseTypes }) {
    return (
        <Layout>
            <Navbar>
                <Authorize />
                <div className="logo-responsive">
                    <Link href="/">
                        <a>
                            <img src="/assets/images/logo.svg" alt="logo" />
                        </a>
                    </Link>
                </div>
            </Navbar>

            <main className="home-content">
                <MainSlider />
                <Utilities />
                <SearchHome services={services} cities={cities} houseTypes={houseTypes} />
                <ListProduct products={buildings} />
                <Achievement />
                <Caring />
                <Discover products={buildings} />
                <HomeBlogs articles={articles ?? []} />
                <LogoBrand />
                <HomeContact />
            </main>
            <Footer />
        </Layout>
    )
}

export async function getServerSideProps({ req, res }) {
    try {
        const customerData = await getCustomerData(res, req, true);

        const buildingRes = await ProductService.listBuilding({ page: 1 });
        const buildings = buildingRes.buildings.map(p => ({
            ...p,
            liked: customerData.wishlistIds.includes(p.id)
        }));
        const dataActicle = await BlogService.listArticle();
        const serviceRes = await ProductService.listService();

        const services = serviceRes.services.filter(s => s.type === "Thiết bị").map(i => ({
            ...i,
            isPicked: false
        }));
        const cityRes = await LocationService.listCity();

        const typeRes = await ProductService.listType();
        const houseTypes = typeRes.types.map(t => ({
            value: t,
            name: t,
            checked: false
        }));

        return {
            props: {
                buildings,
                articles: dataActicle.articles,
                services,
                cities: cityRes.cities,
                houseTypes,
                ...customerData
            }
        }
    } catch (error) {
        return {
            props: {}
        }
    }
}


