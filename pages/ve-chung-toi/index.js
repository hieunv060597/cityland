import React, { useState, useReducer, useContext } from "react";
import Authorize from '@/components/Authorize/Authorize';
import Footer from '@/components/footer';
import HeaderSearch from '@/components/Header/Search';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import { getSession } from "next-auth/client";
import Link from "next/link";
import { getCustomerData } from '@/lib/helper';
import HomeContact from "../../components/Home/section_10--contact-form/home-contact";
import { default as classes } from '@/styles/about-us/about-us.module.scss';
import { AboutUsSettings } from "../../lib/settings/about";

const AboutUs = ({ session, cart, customer }) => {
    return (
        <Layout>
            <Navbar>
                <Authorize session={session} cart={cart} customer={customer} />
                <HeaderSearch></HeaderSearch>
            </Navbar>

            <main>
                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Hợp tác
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className={classes.topBanner}>
                    <div className="container">
                        <h1>Về chúng tôi</h1>
                        <img src={AboutUsSettings.Content.section1.images} alt="img" className="w-100" />
                    </div>
                </section>


                <section className={classes.vision}>
                    <div className="container">
                        <div className={classes.pagesTitle}>
                            <h2 className={classes.title}>{AboutUsSettings.Title.title1}</h2>
                        </div>

                        <div className="row mt-4">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <p style={{
                                    color: "#00948C"
                                }}>
                                    {AboutUsSettings.Content.section2.text1}
                                </p>

                                <p style={{
                                    color: "#2B296D"
                                }}>
                                    {AboutUsSettings.Content.section2.text2}
                                </p>
                            </div>

                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <img src={AboutUsSettings.Content.section2.images} alt="" className="w-100" />
                            </div>
                        </div>
                    </div>
                </section>

                <section className={classes.utilities}>

                    <div className="container">
                        <div className={classes.pagesTitle}>
                            <h2 className={classes.title}>{AboutUsSettings.Title.title2}</h2>
                        </div>
                        <div className={`${classes.listSupport} row`}>
                            {AboutUsSettings.Content.section3.listContent.map((item, index) => (
                                <div className={`col-12 col-sm-12 col-md-4 ${classes.ctFlex}`} key={index}>
                                    <div className={classes.supportBox}>
                                        <div className="row no-gutters align-items-center">
                                            <div className="col-2 col-sm-2 col-md-12 col-lg-12">
                                                <div className={classes.icon}>
                                                    <i style={{
                                                        backgroundImage: `url(${item.images})`
                                                    }}></i>
                                                </div>

                                            </div>

                                            <div className="col-10 col-sm-10 col-md-12 col-lg-12">
                                                <div className={classes.text}>
                                                    <p>{item.text}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}


                        </div>
                    </div>
                </section>

                <section className={classes.ourTeam}>
                    <div className="container">
                        <div className={classes.pagesTitle}>
                            <h2 className={classes.title}>{AboutUsSettings.Title.title3}</h2>
                        </div>
                        <div className="row mt-4">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <img src={AboutUsSettings.Content.section4.image} alt="img" className="w-100" />
                            </div>

                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div className={classes.ourTeam_content}>
                                    <p className={classes.title}>
                                        {AboutUsSettings.Content.section4.text1}
                                    </p>

                                    <label>Giá trị cốt lõi</label>

                                    <ul>
                                        {AboutUsSettings.Content.section4.list.map((item, index) => (
                                            <li className="mb-3" key={index}>
                                                <div className={classes.listValue}>
                                                    <i style={{
                                                        backgroundImage: `url(${item.images})`
                                                    }}></i>
                                                    <div className={classes.content}>
                                                        <h4>{item.title}</h4>
                                                        <p>{item.subtitle}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        ))}

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <HomeContact />
            </main>

            <Footer />
        </Layout>
    )
}

export async function getServerSideProps({ req, res, }) {
    try {
        const customerData = await getCustomerData(res, req, true);

        return {
            props: customerData
        };
    } catch (error) {
        return {
            props: {}
        }
    }
}


export default AboutUs;