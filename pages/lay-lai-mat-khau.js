import Unauthorize from '@/components/Authorize/Unauthorize';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import AuthService from '@/lib/auth/auth.service';
import { yupResolver } from '@hookform/resolvers/yup';
import Link from "next/link";
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import * as Yup from 'yup';
import { showGlobalPopup } from '../lib/helper';

const Recover = ({ token }) => {
    const router = useRouter();
    useEffect(() => {
        if (!token) {
            router.push('/dang-nhap')
        }
    });

    const validationSchema = Yup.object().shape({
        password: Yup.string()
            .required('Mật khẩu là bắt buộc.').min(8, 'Mật khẩu quá ngắn').max(64, 'Mật khẩu quá dài'),
        passwordConfirm: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Mật khẩu không khớp'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };

    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const onSubmit = async (data) => {
        try {
            const payload = {
                password: data.password,
                token
            }
            await AuthService.post(payload);
            showGlobalPopup(
                <>
                    <span className="dp_b mg_bt15">Thay đổi mật khẩu thành công</span>
                    <Link href="/dang-nhap">
                        <button type="button" className="btn btn-primary">
                            Đi đến đăng nhập
                        </button>
                    </Link>
                </>
            );
        } catch (error) {
            showGlobalPopup(error?.response?.data?.message);
        }
    }

    return (
        <Layout>
            <Navbar>
                <Unauthorize />
                <div className="d-none"></div>
            </Navbar>

            <div className="login-content">
                <div className="container">
                    <div className="row no-gutters">
                        <div className="col-12 col-sm-12 col-md-5 col-lg-5">
                            <div className="authen-images">
                                <img src="/assets/images/img1.jpg" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, Egestas scelerisque molestie varius pretium ullamcorper vulputate vitae nisi,
                                </p>
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 col-md-7 col-lg-7">
                            <div className="login-box">
                                <div className="provider-login">
                                    <h3 className="title">
                                        Thiết lập lại mật khẩu
                                    </h3>
                                </div>

                                <div className="login-cretinal">
                                    <form onSubmit={handleSubmit(onSubmit)}>
                                        <div className="row">
                                            <div className="col-12 col-md-12">
                                                <input
                                                    type="password"
                                                    className="form-control"
                                                    placeholder="Mật khẩu mới"
                                                    {...register("password")}
                                                />
                                                <span className="error_message">{errors.password?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <input
                                                    type="password"
                                                    className="form-control"
                                                    placeholder="Nhập lại mật khẩu"
                                                    {...register("passwordConfirm")}
                                                />
                                                <span className="error_message">{errors.passwordConfirm?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <button className="btn-submit-cre" type="submit">
                                                    Thiết lập
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export async function getServerSideProps({ query }) {
    const { token } = query
    return {
        props: {
            token: token ?? null
        }
    };
}
export default Recover;