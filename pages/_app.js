import Popup from '@/components/GlobalPopup/popup';
import { Provider } from "next-auth/client";
import React from "react";
import { CustomerProvider } from '../components/customer_context';

export default function App({ Component, pageProps }) {
    return (
        <Provider session={pageProps.session}>
            <CustomerProvider props={pageProps}>
                <Component {...pageProps} />
            </CustomerProvider>
            <Popup></Popup>
        </Provider>
    )
}
