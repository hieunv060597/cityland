import Authorize from '@/components/Authorize/Authorize';
import Footer from '@/components/footer';
import HeaderSearch from '@/components/Header/Search';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import { getCustomerData } from '@/lib/helper';
import { default as classes } from '@/styles/cooperate/cooperate.module.scss';
import Link from "next/link";
import React from "react";
import HomeContact from "@/components/Home/section_10--contact-form/home-contact";
import { CooperateSettings } from "@/lib/settings/cooperate";


const Cooperate = ({ session, cart, customer }) => {
    return (
        <Layout>
            <Navbar>
                <Authorize session={session} cart={cart} customer={customer} />
                <HeaderSearch></HeaderSearch>
            </Navbar>

            <main>
                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Hợp tác
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className={classes.topBanner}>
                    <div className="container">
                        <h1>Hợp tác</h1>

                        <div className={classes.box}>
                            <div className={classes.topBanner_images}>
                                <img src={CooperateSettings.Content.imageBanner} layout="fill" alt="Picture of the author" />
                            </div>

                            <div className={classes.topBanner_boxText}>
                                <div className={classes.title}>
                                    <h1>{CooperateSettings.Title.titleBanner.mainTitle}</h1>
                                    <p>{CooperateSettings.Title.titleBanner.subTitle}</p>
                                </div>

                                <div className={classes.content}>
                                    <ul className="row">
                                        {CooperateSettings.Content.listContentBanner.map((item, index) => (
                                            <li className="col-12 col-sm-12 col-md-6 col-lg-6" key={index}>
                                                {item}
                                            </li>
                                        ))}

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className={classes.listServices}>
                    <div className="container">
                        <div className={classes.pagesTitle}>
                            <h2 className={classes.title}>{CooperateSettings.Title.normalTitle.title1}</h2>
                        </div>

                        <div className={`${classes.listSupport} row`}>
                            {CooperateSettings.Content.section_2_Content.map((item, index) => (
                                <div className={`col-12 col-sm-12 col-md-4 ${classes.ctFlex}`} key={index}>
                                    <div className={classes.supportBox}>
                                        <div className="row no-gutters align-items-center">
                                            <div className="col-2 col-sm-2 col-md-12 col-lg-12">
                                                <div className={classes.icon}>
                                                    <i style={{
                                                        backgroundImage: `url(${item.images})`
                                                    }}></i>
                                                </div>

                                            </div>

                                            <div className="col-10 col-sm-10 col-md-12 col-lg-12">
                                                <div className={classes.text}>
                                                    <p>{item.text}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}


                        </div>
                    </div>
                </section>

                <section className={classes.customerGive}>
                    <div className="container">
                        <div className={classes.pagesTitle}>
                            <h2 className={classes.title}>{CooperateSettings.Title.normalTitle.title2}</h2>
                        </div>

                        <div className="row mt-4">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <img src={CooperateSettings.Content.section_3_Content.images} alt="" className="w-100" />
                            </div>

                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <ul className={classes.listGive}>
                                    {CooperateSettings.Content.section_3_Content.listGive.map((item, index) => (
                                        <li key={index}>
                                            <label>
                                                {item.id}
                                            </label>

                                            <h4>
                                                {item.content}
                                            </h4>
                                        </li>
                                    ))}

                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <HomeContact />
            </main>

            <Footer />
        </Layout>
    )
}

export async function getServerSideProps({ req, res, resolvedUrl }) {
    try {
        const customerData = await getCustomerData(res, req, true);

        return {
            props: {
                ...customerData,
                pathName: resolvedUrl
            }
        };
    } catch (error) {
        return {
            props: {}
        }
    }
}

export default Cooperate;