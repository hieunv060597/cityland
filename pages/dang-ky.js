import Authorize from '@/components/Authorize/Authorize';
import Unauthorize from '@/components/Authorize/Unauthorize';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import eventEmitter from '@/lib/eventEmitter';
import { yupResolver } from '@hookform/resolvers/yup';
import { getSession, signIn, useSession } from "next-auth/client";
import Link from "next/link";
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as Yup from 'yup';
import { showGlobalPopup } from '@/lib/helper';

export default function Signup() {
    const [flagCheck1, setFlagCheck1] = useState(false);
    const [flagCheck2, setFlagCheck2] = useState(false);

    const router = useRouter();
    const [isLoading, setLoading] = useState(false);

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Tên là bắt buộc.').min(1, 'Tên quá ngắn').max(64, 'Tên quá dài'),
        email: Yup.string()
            .required('Email là bắt buộc.').email('Email không hợp lệ.'),
        phoneNumber: Yup.string()
            .required('Số điện thoại là bắt buộc.').matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/, 'Số điện thoại không hợp lệ'),
        password: Yup.string().required('Mật khẩu là bắt buộc').min(8, 'Mật khẩu quá ngắn, mật khẩu phải dài tối đa 8 ký tự'),
        passwordConfirmation: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Mật khẩu không khớp'),
        dob: Yup.string()
            .required('Ngày sinh là bắt buộc'),
    });

    const formOptions = {
        resolver: yupResolver(validationSchema)
    };

    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    const showPass1 = () => {
        if (flagCheck1 == true) {
            setFlagCheck1(false)
        }
        else {
            setFlagCheck1(true)
        }
    }

    const showPass2 = () => {
        if (flagCheck2 == true) {
            setFlagCheck2(false)
        }
        else {
            setFlagCheck2(true)
        }
    }

    const registerUser = async (data) => {
        const dataSignin = {
            email: data.email,
            phoneNumber: data.phoneNumber,
            password: data.password,
            name: data.name,
            dob: data.dob,
        }
        setLoading(true)
        try {
            const res = await signIn('signup', { redirect: false, ...dataSignin });
            if (res.error) {
                setLoading(false)
                return showGlobalPopup(res.error)
            }
            router.push(res.url)
        } catch (error) {
            setLoading(false)
            return showGlobalPopup(error?.response?.data?.message ?? 'Có lỗi xảy ra')
        }
    }

    return (
        <Layout>
            <Navbar>
                <Unauthorize />
                <div className="d-none"></div>
            </Navbar>
            <div className="login-content">
                <div className="container">
                    <div className="row no-gutters">
                        <div className="col-12 col-sm-12 col-md-5 col-lg-5">
                            <div className="authen-images">
                                <img src="/assets/images/img1.jpg" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, Egestas scelerisque molestie varius pretium ullamcorper vulputate vitae nisi,
                                </p>
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 col-md-7 col-lg-7">
                            <div className="login-box">
                                <div className="provider-login">
                                    <h3 className="title">
                                        Tạo tài khoản
                                    </h3>
                                </div>

                                <div className="login-cretinal">
                                    <form onSubmit={handleSubmit(registerUser)}>
                                        <div className="row">
                                            <div className="col-12 col-md-12">
                                                <input
                                                    type="text"
                                                    name="name"
                                                    className="form-control"
                                                    {...register("name")}
                                                    placeholder="Họ và tên"
                                                />
                                                <span className="error_message">{errors.name?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <input type="text"
                                                    name="email"
                                                    className="form-control"
                                                    {...register("email")}
                                                    placeholder="E-mail" />
                                                <span className="error_message">{errors.email?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-6">
                                                <div className="input-password">
                                                    <input type={flagCheck1 == false ? "password" : "text"}
                                                        name="password"
                                                        className="form-control"
                                                        {...register("password")}
                                                        placeholder="Mật khẩu" />
                                                    <button type="button" className="show-password" onClick={showPass1}>
                                                        <i className={flagCheck1 == false ? "fa fa-eye-slash" : "fa fa-eye"}></i>
                                                    </button>
                                                </div>
                                                <span className="error_message">{errors.password?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-6">
                                                <div className="input-password">
                                                    <input type={flagCheck2 == false ? "password" : "text"}
                                                        name="password"
                                                        className="form-control"
                                                        {...register("passwordConfirmation")}
                                                        placeholder="Nhập lại mật khẩu" />
                                                    <button type="button" className="show-password" onClick={showPass2}>
                                                        <i className={flagCheck2 == false ? "fa fa-eye-slash" : "fa fa-eye"}></i>
                                                    </button>
                                                </div>
                                                <span className="error_message">{errors.passwordConfirmation?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <input
                                                    type="text"
                                                    name="phoneNumber"
                                                    className="form-control"
                                                    {...register("phoneNumber")}
                                                    placeholder="Số điện thoại"
                                                />
                                                <span className="error_message">{errors.phoneNumber?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <input
                                                    type="radio"
                                                    name="gender"
                                                    value="m"
                                                />
                                                <span className="gen-s ml-1">
                                                    Nam
                                                </span>

                                                <input
                                                    type="radio"
                                                    name="gender"
                                                    value="f"
                                                    className="ml-3"
                                                />
                                                <span className="gen-s ml-1">
                                                    Nữ
                                                </span>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <input
                                                    type="date"
                                                    name="dob"
                                                    className="form-control mt-2"
                                                    {...register("dob")}

                                                />
                                                <span className="error_message">{errors.dob?.message}</span>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <button className="btn-submit-cre" type="submit">
                                                    {
                                                        isLoading ? (
                                                            <div className="spinner-border text-light" role="status">
                                                                <span className="sr-only">Loading...</span>
                                                            </div>
                                                        ) : 'Tạo Tài khoản'
                                                    }
                                                </button>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <div className="suggest-sign-up">
                                                    <p>Bạn đã có tài khoản?
                                                        <Link href="/dang-nhap">
                                                            <a>
                                                                Đăng nhập
                                                            </a>
                                                        </Link>

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export async function getServerSideProps({ req, res }) {
    const session = await getSession({ req });
    if (session) {
        res.writeHead(301, { Location: '/dang-nhap' })
        res.end()
        return true
    }

    return {
        props: {}
    }
}
