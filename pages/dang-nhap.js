import Authorize from '@/components/Authorize/Authorize';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import eventEmitter from '@/lib/eventEmitter';
import { initializeApp } from 'firebase/app';
import { FacebookAuthProvider, getAuth, GoogleAuthProvider, RecaptchaVerifier, signInWithPhoneNumber, signInWithPopup } from "firebase/auth";
import { getSession, signIn } from "next-auth/client";
import Link from "next/link";
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

const firebaseConfig = {
    apiKey: process.env.NEXT_PUBLIC_apiKey,
    authDomain: process.env.NEXT_PUBLIC_authDomain,
    projectId: process.env.NEXT_PUBLIC_projectId,
    storageBucket: process.env.NEXT_PUBLIC_storageBucket,
    messagingSenderId: process.env.NEXT_PUBLIC_messagingSenderId,
    appId: process.env.NEXT_PUBLIC_appId,
    measurementId: process.env.NEXT_PUBLIC_measurementId
};

export default function SignIn() {
    const router = useRouter();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordValid, setPasswordValid] = useState(true);
    const [checkLoginPhone, setCheckLoginPhone] = useState(false);
    const [checkOTP, setCheckOTP] = useState(false);
    const [phoneSignIn, setPhoneSignIn] = useState("");
    const [inputOTP, setInputOTP] = useState("");
    const [errorPhone, setErrorPhone] = useState("");
    const [time, setTime] = useState(null);
    const [isLoading, setLoading] = useState(false);

    const emailChanged = (e) => setEmail(e.target.value);
    const passwordChanged = (e) => {
        setPasswordValid(true);
        setPassword(e.target.value);
    };

    useEffect(() => {
        eventEmitter.emit('clone_global_popup');
    }, [])

    initializeApp(firebaseConfig);
    const googleProvider = new GoogleAuthProvider();
    const facebookProvider = new FacebookAuthProvider();

    const auth = getAuth();

    const signin_gg = () => {
        signInWithPopup(auth, googleProvider)
            .then(async (result) => {
                const idToken = result.user.toJSON().stsTokenManager.accessToken;
                const res = await signIn('login_with_exchange_token', { redirect: false, idToken });
                if (res.error) {
                    eventEmitter.emit('show_global_popup', {
                        message: res.error
                    })
                    return;
                }
                router.push(res.url)
                return;
            }).catch((error) => {
                eventEmitter.emit('show_global_popup', {
                    message: error.message
                })
                return;
            })
    }

    const signin_fb = () => {
        signInWithPopup(auth, facebookProvider)
            .then(async (result) => {
                const idToken = result.user.toJSON().stsTokenManager.accessToken;
                const res = await signIn('login_with_exchange_token', { redirect: false, idToken });
                if (res.error) {
                    eventEmitter.emit('show_global_popup', {
                        message: res.error
                    })
                    return;
                }
                router.push(res.url)
                return;
            }).catch((error) => {
                eventEmitter.emit('show_global_popup', {
                    message: error.message
                })
                return;
            })
    }

    const handleSubmitEmail = async (e) => {
        e.preventDefault();
        setLoading(true)
        if (password.length < 8 || password.length > 60) {
            setPasswordValid(false);
            setLoading(false)
            return;
        }
        const res = await signIn('email', { redirect: false, email, password });
        if (res.error) {
            eventEmitter.emit('show_global_popup', {
                message: res.error
            })
            setLoading(false)
            return;
        }
        router.push(res.url)
        return;
    }

    const handleInputPhone = (event) => {
        const value = event.target.value;
        const flag = value.match(/((09|03|07|08|05)+([0-9]{8})\b)/g);

        if (flag == null) {
            setErrorPhone("Số điện thoại không hợp lệ!");
            return
        }
        setPhoneSignIn(value);
        setErrorPhone(null)
    }

    const checkSignInPhone = async () => {

        try {
            window.reCaptchaVerifier = new RecaptchaVerifier('recaptcha-container', {}, auth);
            const confirmationResult = await signInWithPhoneNumber(auth, phoneSignIn?.replace('0', '+84'), window.reCaptchaVerifier);
            window.confirmationResult = confirmationResult;
            window.reCaptchaVerifier = null;
            setCheckOTP(!checkOTP);
            countTimeOTP(81);
        } catch (dataError) {
            eventEmitter.emit('show_global_popup', {
                message: dataError.message
            })
        }
    }

    const handleSubmitPhone = async (event) => {
        event.preventDefault();
        try {
            const result = await confirmationResult.confirm(inputOTP);
            const idToken = result.user.toJSON().stsTokenManager.accessToken;
            const res = await signIn('login_with_exchange_token', { redirect: false, idToken });
            if (res.error) {
                return showGlobalPopup(res.error)
            }
            router.push(res.url)
            return;
        } catch (error) {
            return showGlobalPopup(error?.response?.data?.message ?? error.message ?? 'Có lỗi xảy ra')
        }
    }

    const countTimeOTP = (time) => {
        setInterval(function () {
            if (time > 0) {
                time--;
                setTime(time);

            }
            else if (time == 0) {
                clearInterval(countTimeOTP);
                setTime(0)
                return 0;
            }
        }, 1000);

    }

    const switchToLoginPhone = () => {
        setCheckLoginPhone(!checkLoginPhone);
    }

    const otpChange = (event) => {
        const value = event.target.value;
        setInputOTP(value)
    }


    const backToSwitch = () => {
        setCheckOTP(!checkOTP);
    }

    return (
        <Layout>
            <Navbar>
                <Authorize />
                <div className="d-none"></div>
            </Navbar>

            <div className="login-content">
                <div className={checkOTP == false ? "container" : "d-none"}>
                    <div className="row no-gutters">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-5">
                            <div className="authen-images">
                                <img src="/assets/images/img1.jpg" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, Egestas scelerisque molestie varius pretium ullamcorper vulputate vitae nisi,
                                </p>
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 col-md-8 col-lg-7">
                            <div className="login-box">
                                <div className="provider-login">
                                    <h3 className="title">
                                        Đăng nhập
                                    </h3>
                                    <div className="option-login">
                                        <div className="option-login__providers">
                                            <button className="btn-provider" type="button" onClick={() => signin_gg()}>
                                                <img src="/assets/images/login_gg.svg" />
                                                <span>Đăng nhập với Google</span>
                                            </button>
                                        </div>
                                        <div className="option-login__providers">
                                            <button className="btn-provider" type="button" onClick={() => signin_fb()}>
                                                <img src="/assets/images/login_fb.svg" />
                                                <span>Đăng nhập với Facebook</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div className={checkLoginPhone == false ? "login-cretinal" : "d-none"}>
                                    <h4 className="login-cretinal__title">- Hoặc -</h4>
                                    <form className="login-form" onSubmit={handleSubmitEmail}>
                                        <div className="input-box">
                                            <input
                                                key="signin_email"
                                                type="email"
                                                className="form-control"
                                                name="email"
                                                placeholder="Email"
                                                tabIndex="1"
                                                value={email}
                                                onChange={emailChanged}
                                                required
                                            />
                                        </div>

                                        <div className="input-box">
                                            <input
                                                key="signin_password"
                                                type="password"
                                                className="form-control"
                                                name="password"
                                                value={password}
                                                onChange={passwordChanged}
                                                placeholder="Password"
                                                tabIndex="2"
                                                required
                                            />
                                            {
                                                !passwordValid ? <span className="error_message">Mật khẩu phải từ 8-60 ký tự</span> : ''
                                            }
                                            <style jsx>{`
                                                .error_message{
                                                    color: #f00;
                                                    font-size: 12px;
                                                }
                                            `}</style>
                                        </div>

                                        <div className="forgot-password d-flex align-items-center justify-content-between">
                                            <button className="btn-login-number" onClick={switchToLoginPhone}>
                                                Đăng nhập bằng số điện thoại
                                            </button>

                                            <Link href="/quen-mat-khau">
                                                <a>Quên mật khẩu?</a>
                                            </Link>
                                        </div>

                                        <div className="input-box">
                                            <button className="btn-submit-cre" type="submit">
                                                {
                                                    isLoading ? (
                                                        <div className="spinner-border text-light" role="status">
                                                            <span className="sr-only">Loading...</span>
                                                        </div>
                                                    ) : ' Đăng nhập'
                                                }
                                            </button>
                                        </div>

                                        <div className="suggest-sign-up">
                                            <p>Bạn chưa có tài khoản?
                                                <Link href="/dang-ky">
                                                    <a>
                                                        Đăng ký
                                                    </a>
                                                </Link>

                                            </p>
                                        </div>
                                    </form>
                                </div>

                                <div className={checkLoginPhone == true ? "login-cretinal" : "d-none"}>
                                    <h4 className="login-cretinal__title">- Hoặc -</h4>
                                    <div className="login-form login-number">
                                        <div className="input-box">
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="Nhập số điện thoại"
                                                onChange={handleInputPhone}
                                            />
                                            <span className="error_message">{errorPhone}</span>
                                        </div>
                                        <div id="recaptcha-container"></div>

                                        <div className="forgot-password d-flex align-items-center justify-content-start">
                                            <button className="btn-login-number" onClick={switchToLoginPhone}>
                                                Đăng nhập bằng mật khẩu
                                            </button>
                                        </div>

                                        <div className="input-box">
                                            <button className="btn-submit-cre" type="button" onClick={checkSignInPhone}>
                                                Đăng nhập
                                            </button>
                                        </div>

                                        <div className="suggest-sign-up">
                                            <p>Bạn chưa có tài khoản?
                                                <Link href="/dang-ky">
                                                    <a>
                                                        Đăng ký
                                                    </a>
                                                </Link>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className={checkOTP == true ? "container validate-otp" : "d-none"}>
                    <div className="check-otp">
                        <button className="switch-account" onClick={backToSwitch}>
                            Sử dụng tài khoản khác
                        </button>

                        <form className="verification-phone" onSubmit={handleSubmitPhone}>
                            <div className="verification-phone__title">
                                <h4>Vui lòng nhập mã xác minh</h4>
                                <div className="sub-descript">
                                    <p>Mã xác minh của bạn sẽ được gửi bằng tin nhắn đến</p>
                                    <p>{phoneSignIn}</p>
                                </div>
                            </div>

                            <div className="verification-phone__validate">
                                <div className="row no-gutters">
                                    <div className="col-10">
                                        <div className="type-otp">
                                            <input type="text"
                                                maxLength="6"
                                                onChange={otpChange} />
                                            <div className="line">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="time-count">
                                    <p>Thời gian hiệu lực của mã xác minh: </p>
                                    <p>{time}</p>
                                </div>

                                <div className="input-box">
                                    <button className="btn-submit-cre" type="submit">
                                        Đăng nhập
                                    </button>
                                    <div className={time == 0 ? "disable-btn" : "d-none"}></div>
                                </div>

                                <div className="input-box d-flex align-items-center justify-content-center">
                                    <p>Bạn không nhận được mã ?</p>
                                    <button type="button" className="re-send">
                                        Gửi lại OTP
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        </Layout>
    )
}


export async function getServerSideProps({ req, res }) {
    const session = await getSession({ req });
    if (session) {
        res.writeHead(301, { Location: '/' })
        res.end()
        return true
    }

    return {
        props: {}
    }
}
