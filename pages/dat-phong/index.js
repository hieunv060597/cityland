import Authorize from '@/components/Authorize/Authorize';
import HeaderSearch from '@/components/Header/Search';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import CartService from '@/lib/checkout/cart.service';
import CheckoutService from '@/lib/checkout/checkout.service';
import eventEmitter from '@/lib/eventEmitter';
import { formatDate, formatMoney, getCookieOfReq, setClientCookie } from '@/lib/helper';
import ProductService from '@/lib/products/product.service';
import UserService from '@/lib/users/user.service';
import { yupResolver } from '@hookform/resolvers/yup';
import { getSession } from "next-auth/client";
import Link from "next/link";
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as Yup from 'yup';

const Checkout = (params) => {
    const { cartToken, room, payments, session, variantDeposit, building, customer, services, paymentDefault, items } = params;

    const [payment, setPayment] = useState(false);
    const [dayCount, setDayCount] = useState(5);
    const [checkoutSuccess, setCheckoutSuccess] = useState(false);
    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Tên là bắt buộc.').min(1, 'Tên quá ngắn').max(64, 'Tên quá dài'),
        dateTime: Yup.string()
            .required('Thời gian hẹn là bắt buộc'),
        email: Yup.string()
            .email('Email không hợp lệ.').required('Email là bắt buộc'),
        phoneNumber: Yup.string()
            .required('Số điện thoại là bắt buộc.').matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/, 'Số điện thoại không hợp lệ'),
        note: Yup.string().max(2000, 'Ghi chú quá dài.')
    });

    const [paymentPicked, setPaymentPicked] = useState(null);
    const [paymentMethod, setPaymentMethod] = useState(null);
    const [paymentMethodDescript, setPaymentMethodDescript] = useState([]);

    const [expirationDate, setExpirationDate] = useState(new Date());

    const formOptions = {
        resolver: yupResolver(validationSchema), defaultValues: {
            name: customer?.name,
            email: customer?.email,
            phoneNumber: customer?.phoneNumber ? customer.phoneNumber.replace('+84', '0') : null,
        }
    };

    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    const onSummit = async (data) => {
        if (!cartToken) {
            eventEmitter.emit('show_global_popup', {
                message: 'Có lỗi xảy ra.'
            });
            return;
        }

        let paymentPick = paymentDefault;

        if (payment) {
            if (!paymentPicked) {
                eventEmitter.emit('show_global_popup', {
                    message: 'Vui lòng chọn phương thức thanh toán'
                });
                return;
            }
            if (!items[0]) {
                eventEmitter.emit('show_global_popup', {
                    message: 'Có lỗi xảy ra. thiếu item'
                });
                return;
            }
            if (dayCount < 5) {
                eventEmitter.emit('show_global_popup', {
                    message: 'Ngày đặt cọc tối thiểu là 5 ngày'
                });
                return;
            }

            paymentPick = paymentPicked;
            const newDateTime = new Date(data.dateTime)
            setExpirationDate(new Date(newDateTime.setDate(newDateTime.getDate() + dayCount)))
            await CartService.updateItem(cartToken, items[0].variant.id, dayCount);
        }

        if (!paymentPick) {
            eventEmitter.emit('show_global_popup', {
                message: 'Có lỗi xảy ra. thiếu payment'
            });
            return
        }

        const checkoutDataReq = {
            customer: {
                phoneNumber: data.phoneNumber,
                name: data.name,
                email: data.email
            },
            note: data.note,
            paymentMethodId: paymentPick.id,
            endAt: new Date(data.dateTime)?.toISOString() ?? new Date().toISOString()
        }

        try {
            const checkoutRes = await CheckoutService.post(cartToken, checkoutDataReq, session?.user?.accessToken);
            setCheckoutSuccess(true);
            localStorage.removeItem('ting_tong_cart_token');
            setPaymentMethod(checkoutRes.order.paymentMethod);
            setPaymentMethodDescript(checkoutRes?.order?.paymentMethod?.description?.split('####'));
            setClientCookie('ting_tong_cart_token', null, new Date('1990-10-10'))
            if (checkoutRes.paymentUrl) {
                window.open(checkoutRes.paymentUrl, undefined, '_blank');
            }

        } catch (error) {
            console.log('checkoutRes_error:', error);
        }
    };

    const switchPayment = () => {
        setPaymentPicked(payments[0])
        setPayment(!payment)
    }

    const dayCountChange = (e) => {
        if (e.target.value < 0) {
            return
        }
        setDayCount(e.target.value)
    }

    const handleInput = (e) => {
        const payment = payments.find(p => p.id === e.target.value);
        if (!payment) {
            return eventEmitter.emit('show_global_popup', {
                message: 'Không thể tìm thấy phương thức thanh toán'
            });
        }
        setPaymentPicked(payment);
    }

    return (
        <>
            <Layout>
                <Navbar>
                    <Authorize />
                    <HeaderSearch></HeaderSearch>
                </Navbar>

                <main className="pd_bt50">
                    <section className="main-breadcumb">
                        <div className="container">
                            <nav>
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item">
                                        <Link href="/">
                                            <a>Trang chủ</a>
                                        </Link>
                                    </li>
                                    <li className="breadcrumb-item active" aria-current="page">
                                        Đặt lịch hẹn
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </section>

                    <section className="checkout-detail">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">

                                    {
                                        !checkoutSuccess ? (
                                            <>
                                                <div className="checkout-detail__title">
                                                    <h4>Thông tin đặt lịch</h4>
                                                </div>

                                                <div className="checkout-detail__box">
                                                    <form className="checkout-detail__box--form" onSubmit={handleSubmit(onSummit)}>
                                                        <div className="form-info">
                                                            <div className="row">
                                                                <div className="col-12 col-md-12">
                                                                    <label>Thiết lập thời gian hẹn gặp</label>
                                                                    <input type="datetime-local"
                                                                        className="form-control pick-date"
                                                                        {...register("dateTime")}
                                                                    />
                                                                    <span className="error_message">{errors.dateTime?.message}</span>
                                                                </div>

                                                                <div className="col-12 col-md-12">
                                                                    <label>Cung cấp thông tin cá nhân</label>
                                                                </div>

                                                                <div className="col-12 col-md-6">
                                                                    <input type="text"
                                                                        className="form-control"
                                                                        placeholder="Nhập tên*"
                                                                        {...register("name", { required: true })}
                                                                    />
                                                                    <span className="error_message">{errors.name?.message}</span>
                                                                </div>

                                                                <div className="col-12 col-md-6">
                                                                    <input
                                                                        type="tel"
                                                                        className="form-control"
                                                                        placeholder="Số điện thoại*"
                                                                        {...register("phoneNumber", { required: true })}
                                                                    />
                                                                    <span className="error_message">{errors.phoneNumber?.message}</span>
                                                                </div>

                                                                <div className="col-12 col-md-12">
                                                                    <input
                                                                        type="email"
                                                                        className="form-control"
                                                                        placeholder="Email"
                                                                        {...register("email")}
                                                                    />
                                                                    <span className="error_message">{errors.email?.message}</span>
                                                                </div>

                                                                <div className="col-12 col-md-12">
                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        placeholder="Nhu cầu chi tiết/đặc biệt (nếu có)"
                                                                        {...register("note")}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="submit-box">
                                                            <button className="btn-submit-check">
                                                                Tiếp theo
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </>
                                        ) : (
                                            <>
                                                <div className="success-box">
                                                    <div className="success-box__noti">
                                                        <h3 className="success-box__noti--title">
                                                            Lịch hẹn đã gửi thành công
                                                        </h3>

                                                        <div className="success-box__noti--icon">
                                                            <img src="/assets/images/icon_success.svg" alt="" />
                                                        </div>

                                                        <div className="success-box__noti--alert">
                                                            <p>
                                                                Cảm ơn bạn đã đặt lịch xem phòng, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất để xác nhận thời gian xem phòng.
                                                            </p>
                                                        </div>
                                                    </div>


                                                    {
                                                        paymentMethod && paymentMethod.method === 'bank_transfer' ? (
                                                            <div className="success-box__noti">
                                                                <h3 className="success-box__noti--title title-2">
                                                                    Thông tin chuyển khoản
                                                                </h3>
                                                                <p>{
                                                                    paymentMethodDescript.map(d => (
                                                                        <>
                                                                            <span className="dp_b mg_y10">{d}</span>
                                                                        </>
                                                                    ))
                                                                }</p>
                                                            </div>
                                                        ) : ''
                                                    }

                                                    <div className="success-box__submit">
                                                        <Link href="/">
                                                            <a className="btn-submit-check">
                                                                Quay về trang chủ
                                                            </a>
                                                        </Link>
                                                    </div>
                                                </div>


                                            </>
                                        )
                                    }
                                </div>

                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div className="checkout-detail__title">
                                        <h4>Thông tin phòng đã chọn</h4>
                                    </div>

                                    <div className="checkout-detail__product-info">
                                        <div className="info-box">
                                            <div className="info-box__header">
                                                <h4 className="info-box__header--title">
                                                    {building?.name}
                                                </h4>

                                                <ul className="info-box__header--type">
                                                    <li>
                                                        Loại phòng: {room?.type}
                                                    </li>
                                                    <li>
                                                        Tầng {room?.floor}
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="info-box__body">
                                                <div className="media">
                                                    <div className="media-images mr-3">
                                                        <Link href={`/toa-nha/${building.slug}`} >
                                                            <a>
                                                                {
                                                                    Array.isArray(room.images) && room.images.length > 0 ? (
                                                                        <img src={room.images[0].source} alt={room.name} />
                                                                    ) : (
                                                                        <img src="/assets/images/no-img.jpg" alt="" />
                                                                    )
                                                                }
                                                            </a>
                                                        </Link>
                                                    </div>

                                                    <div className="media-body">
                                                        <div className="media-body__title">
                                                            <ul>
                                                                <li>
                                                                    Phòng  {room?.name}
                                                                </li>
                                                                <li>
                                                                    {formatMoney(room?.variants[1]?.price)}/tháng
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div className="media-body__info">
                                                            <ul>
                                                                <li>
                                                                    Diện tích: {room?.floorArea}m2
                                                                </li>
                                                                <li>
                                                                    Tiện ích {services?.length}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="info-box__body--features">
                                                    <ul className="row">
                                                        {
                                                            Array.isArray(services) && services.length > 0 ? (
                                                                services.filter(s => s.type != "Nội quy").map(s => (
                                                                    < li key={s.id} className="col-4 col-md-4">
                                                                        {
                                                                            Array.isArray(s.images) && s.images.length > 0 ? (
                                                                                <img src={s.images[0].source} alt={s.name} />
                                                                            ) : (<img src="/assets/images/no-img.jpg" alt={s.name} />)
                                                                        }
                                                                        <span>{s.name}</span>
                                                                    </li>
                                                                ))
                                                            ) : ''
                                                        }
                                                    </ul>
                                                </div>
                                                {
                                                    checkoutSuccess && payment ? (
                                                        <>
                                                            <div className={`room-payment-count ${!checkoutSuccess && !payment ? 'dp_n' : ''}`}>
                                                                <p className="title">Số ngày giữ chỗ: {dayCount} ngày</p>
                                                                <ul>
                                                                    <li>
                                                                        Ngày hết hạn đặt cọc: {formatDate(expirationDate)}
                                                                    </li>

                                                                    <li>
                                                                        Tổng tiền cọc: {formatMoney(variantDeposit * dayCount)}
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </>
                                                    ) : ''
                                                }
                                            </div>
                                        </div>
                                    </div>

                                    <div className={`checkout-detail__payment-methods ${checkoutSuccess ? 'dp_n' : ''}`}>
                                        <div className="checkout-detail__payment-methods--title">
                                            <button className={payment == false ? "methods-user" : "methods-user active"} onClick={switchPayment}>
                                                <span className="check-box"></span>
                                                <span className="text">Tôi muốn thanh toán đặt cọc giữ chỗ</span>
                                            </button>

                                            <Link href="/">
                                                <a className="link">
                                                    <i className="fa fa-info-circle"></i>
                                                    <span>Chính sách đặt cọc giữ chỗ</span>
                                                </a>
                                            </Link>
                                        </div>

                                        <div className={payment == false ? "d-none" : "checkout-detail__payment-methods--info"}>
                                            <ul className="date">
                                                <li>
                                                    <span>Số ngày giữ chỗ:</span>
                                                    <input type="number" onChange={dayCountChange} value={dayCount} />
                                                </li>

                                                <li>
                                                    (Tối thiểu 5 ngày)
                                                </li>
                                            </ul>

                                            <ul className="price">
                                                <li>
                                                    Tổng tiền: {formatMoney(variantDeposit * dayCount)}
                                                </li>

                                                <li>
                                                    {formatMoney(variantDeposit)} / ngày
                                                </li>
                                            </ul>
                                        </div>

                                        <div className={payment == false ? "d-none" : "checkout-detail__payment-methods--switch"}>
                                            <h4 className="title">
                                                Phương thức thanh toán
                                            </h4>
                                            {
                                                payments.map(p => (
                                                    <label key={p.id}>
                                                        <input type="radio" value={p.id} name="pay" onChange={handleInput} checked={p.id === paymentPicked?.id} />
                                                        <p>
                                                            <span className="check"></span>
                                                            <span>{p.name}</span>
                                                        </p>
                                                    </label>
                                                ))
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>

            </Layout>
        </>
    )
}

export const redirectIfCartTokenNotFound = (res) => {
    res.writeHead(301, { Location: '/' })
    res.end()
    return true
}


export async function getServerSideProps({ req, res, params }) {
    const session = await getSession({ req });
    const cartToken = getCookieOfReq(req, 'ting_tong_cart_token');

    if (!cartToken) {
        redirectIfCartTokenNotFound(res)
    }

    try {
        let customer = null;
        if (session?.user?.accessToken) {
            const customerRes = await UserService.getUser(session?.user?.accessToken);
            customer = customerRes.customer;
        }

        const cartRes = await CheckoutService.get(cartToken);
        const payments = cartRes.paymentMethods.filter(p => p.method !== 'cod');
        let paymentDefault = cartRes.paymentMethods.find(p => p.method === 'cod');
        if (!paymentDefault) {
            paymentDefault = cartRes.paymentMethods[0];
        }

        const items = cartRes.cart.items;
        const room = items[0]?.variant?.product;

        const buildingId = getCookieOfReq(req, 'ting_tong_building_id');

        const buildingRes = await ProductService.getBuilding(buildingId);

        const newRoom = buildingRes.building.rooms.find(r => r.id === room.id);

        console.log('newRoom:', newRoom);

        const variantDeposit = newRoom.variants[0]?.price ?? 0;
        return {
            props: {
                cart: cartRes.cart,
                cartToken: cartToken ?? null,
                paymentDefault,
                payments,
                items,
                room: newRoom,
                services: newRoom.services.filter(s => s.type === 'Thiết bị'),
                variantDeposit,
                building: buildingRes.building,
                session,
                customer
            }
        }
    } catch (error) {
        console.log('error:', error)
        redirectIfCartTokenNotFound(res)
    }
}

export default Checkout;