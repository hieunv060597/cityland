import React from 'react';
import Layout from '@/components/Layout/Layout';
const SuccessTwo = () => {
    return (
        <>
            <Layout>

                <main>
                    <section className="checkout-detail dp_fl jc_c ai_c" style={{
                        height: '100vh'
                    }}>
                        <div className="container">
                            <div className="row dp_fl jc_c ai_c">
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div className="success-box" style={{
                                        marginBottom: '50%',
                                        backgroundColor: '#369ef5'
                                    }}>
                                        <div className="success-box__noti">
                                            <h3 className="success-box__noti--title">
                                                Thanh toán hoàn tất
                                            </h3>

                                            <div className="success-box__noti--icon">
                                                <img src="/assets/images/icon_success.svg" alt="" />
                                            </div>

                                            <div className="success-box__noti--method">
                                                <p>
                                                    Phương thức thanh toán: <span>Vnpay</span>
                                                </p>
                                            </div>

                                            <div className="success-box__noti--alert">
                                                <p>
                                                    Cảm ơn bạn đã đặt lịch xem phòng, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất để xác nhận thời gian xem phòng.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>

            </Layout>
        </>
    )
}

export default SuccessTwo;