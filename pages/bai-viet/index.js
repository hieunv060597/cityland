import Authorize from '@/components/Authorize/Authorize';
import BlogItem from '@/components/Blog-item/blog-item';
import Footer from '@/components/footer';
import HeaderSearch from '@/components/Header/Search';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import BlogService from '@/lib/blog/blog.service';
import { getCustomerData } from '@/lib/helper';
import { getSession } from "next-auth/client";
import Link from "next/link";
import Pagination from 'rc-pagination';
import React, { useState } from 'react';
import "swiper/css";

const Blogs = ({ productPerPages, blogs, total, cart, session, customer }) => {
    const [list, setList] = useState(blogs);
    const [totals, setTotal] = useState(total);
    const [currentPage, setCurrentPage] = useState(1);

    const pageChange = async (current) => {
        setCurrentPage(current);
        const take = productPerPages;
        const skip = productPerPages * (current - 1);
        await listBlogs({ skip, take });
    }

    const listBlogs = async (params) => {
        try {

            const listRes = await BlogService.listArticle(params);
            setTotal(listRes.total);
            const newblogsList = listRes.orders.map(item => {
                const list = item?.articles;
                return {
                    ...item,
                    list
                }

            });
            setList(newblogsList)
        } catch (error) {
            console.log('errorl', error);
        }
    }

    const itemRender = (current, type, element) => {
        if (type === 'page') {
            return <a href={`#pages${current}`}>{current}</a>;
        }
        return element;
    };

    return (
        <Layout>
            <Navbar>
                <Authorize session={session} cart={cart} customer={customer} />
                <HeaderSearch></HeaderSearch>
            </Navbar>

            <main className="blogs-pages">
                <section className="banner-pages">
                    <div className="container">
                        <img src="/assets/images/banner.png" alt="" />
                    </div>
                </section>

                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Blogs
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className="list-blogs">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-8">
                                <div className="list-blogs-box">
                                    <div className="list-blogs-box__title">
                                        <h2>Tất cả bài viết</h2>
                                    </div>
                                    {list.map(item => (
                                        <BlogItem key={item.id} blog={item} />
                                    ))}
                                </div>

                                {
                                    totals / productPerPages > 1 ? (<Pagination total={totals} current={currentPage} onChange={pageChange} pageSize={productPerPages} itemRender={itemRender} />) : ""
                                }
                            </div>

                            <div className="col-12 col-sm-12 col-md-12 col-lg-4">
                                <div className="blog-sidebar">
                                    <div className="blog-sidebar__title">
                                        <h4>Các bài viết gần đây</h4>
                                    </div>

                                    <div className="blog-sidebar__list">
                                        {list.map(item => (
                                            <div className="row blogs-recent" key={item.id}>
                                                <div className="col-4">
                                                    <Link href={{
                                                        pathname: '/bai-viet/[id]',
                                                        query: { id: item.id },
                                                    }} >
                                                        <a className="blogs-recent__images">
                                                            <img src={item.image} alt="" />
                                                        </a>
                                                    </Link>
                                                </div>

                                                <div className="col-8">
                                                    <Link href={{
                                                        pathname: '/bai-viet/[id]',
                                                        query: { id: item.id },
                                                    }} >
                                                        <a className="blogs-recent__title">
                                                            {item.title}
                                                        </a>
                                                    </Link>
                                                </div>
                                            </div>
                                        ))}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <Footer />
        </Layout>
    )
}

export async function getServerSideProps({ req, res }) {
    try {
        const customerData = await getCustomerData(res, req, true);

        const listRes = await BlogService.listArticle({ take: 10, skip: 0 });
        const blogsList = listRes.articles.map(item => {
            const list = item;
            return {
                ...item,
                list
            }

        });
        const total = listRes.total

        return {
            props: {
                listRes,
                blogs: blogsList,
                total,
                ...customerData
            }
        }
    } catch (error) {
        console.log('error:', error);
        return {
            props: {},
        }
    }
}

export default Blogs;