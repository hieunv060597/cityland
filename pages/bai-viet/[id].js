import HeaderSearch from '@/components/Header/Search';
import { getSession } from "next-auth/client";
import Link from "next/link";
import React from 'react';
import SwiperCore, {
    Pagination
} from 'swiper';
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from 'swiper/react';
import Authorize from '@/components/Authorize/Authorize';
import Footer from '@/components/footer';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import BlogService from '@/lib/blog/blog.service';
import { formatDate, getCustomerData } from '@/lib/helper';
import VerticalBlogItem from '../../components/Blog-item/vertical-blog-item';

SwiperCore.use([Pagination]);

const BlogDetail = ({ article, blogsList, cart, session, customer }) => {
    return (
        <Layout>
            <Navbar>
                <Authorize cart={cart} session={session} customer={customer} />
                <HeaderSearch></HeaderSearch>
            </Navbar>

            <main>
                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>

                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Bài viết</a>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Thuê nhà ở đâu thì đẹp
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className="blogs-detail">
                    <div className="container">
                        <div className="blogs-detail__content">
                            <div className="the_content">
                                <h1 className="mb-2">{article.title}</h1>
                                <img src={article.image} />
                                <div dangerouslySetInnerHTML={{ __html: article.content }}></div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="home-blogs">
                    <div className="container">
                        <div className="homepages-title text-left">
                            <h2 className="main-title">
                                Các bài viết liên quan
                            </h2>
                        </div>
                        <Swiper slidesPerView={3} spaceBetween={30} loop={true} pagination={{
                            "dynamicBullets": true
                        }} navigation={false} autoplay={{
                            "delay": 5000
                        }}
                            breakpoints={{
                                "300": {
                                    "slidesPerView": 1,
                                },
                                "768": {
                                    "slidesPerView": 2,
                                },
                                "886": {
                                    "slidesPerView": 3,
                                },
                                "1024": {
                                    "slidesPerView": 3,
                                },
                                "1368": {
                                    "slidesPerView": 3,
                                }
                            }}
                            className="mySwiper">

                            {blogsList.map(item => (
                                <SwiperSlide key={item.id}>
                                    <VerticalBlogItem item={item}></VerticalBlogItem>
                                </SwiperSlide>
                            ))}

                        </Swiper>
                    </div>
                </section>
            </main>

            <Footer />
        </Layout>
    )
}

export async function getServerSideProps({ req, params, res }) {
    const { id } = params;
    try {
        const customerData = await getCustomerData(res, req, true);


        const article = await BlogService.getArticle(id);
        const listRes = await BlogService.listArticle();
        const blogsList = listRes.articles.filter(a => a.id !== id).map(item => {
            const list = item;
            return {
                ...item,
                list
            }
        });

        return {
            props: {
                article: article.article,
                blogsList,
                ...customerData
            }
        }
    } catch (error) {
        console.log(error);
        return {
            props: {}
        }
    }
}

export default BlogDetail;