import Popup from '@/components/GlobalPopup/popup';
import axios from 'axios';
import Link from "next/link";
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import Unauthorize from '@/components/Authorize/Unauthorize';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';

const ForgotPassword = () => {
    const [email, setEmail] = useState("");
    const baseURL = "https://api.dev.318.suplo.vn/v1";
    const router = useRouter();
    const handleChange = (event) => {
        var value = event.target.value
        setEmail(
            value.replace("@gmail.com", "%40gmail")
        );
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const url = `${baseURL}/auth/customer/reset_password?email=${email}.com&domain=tingtong`;
        try {
            axios.get(url)
                .then(res => {
                    // alert("a messege was sent to your email, open it to change password");
                    alert("Một tin nhắn đã được gửi đến email của bạn, vui lòng mở email để biết thêm thông tin");
                    router.push("/dang-nhap");
                })
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <Layout>
            <Navbar>
                <Unauthorize />
                <div className="d-none"></div>
            </Navbar>

            <div className="login-content">
                <div className="container">
                    <div className="row no-gutters">
                        <div className="col-12 col-sm-12 col-md-5 col-lg-5">
                            <div className="authen-images">
                                <img src="/assets/images/img1.jpg" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, Egestas scelerisque molestie varius pretium ullamcorper vulputate vitae nisi,
                                </p>
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 col-md-7 col-lg-7">
                            <div className="login-box">
                                <div className="provider-login">
                                    <h3 className="title">
                                        Quên mật khẩu
                                    </h3>

                                    <p className="sub-title">
                                        Vui lòng nhập email của bạn để chúng tôi có thể hỗ trợ việc tìm lại tài khoản.
                                    </p>
                                </div>

                                <div className="login-cretinal">
                                    <form onSubmit={(event) => handleSubmit(event)}>
                                        <div className="row">
                                            <div className="col-12 col-md-12">
                                                <input
                                                    type="email"
                                                    className="form-control"
                                                    placeholder="Email"
                                                    onChange={handleChange}
                                                />
                                                <button className="btn-submit-cre" type="submit">
                                                    Đăng nhập
                                                </button>
                                            </div>

                                            <div className="col-12 col-md-12">
                                                <div className="suggest-sign-up">
                                                    <p>Bạn chưa có tài khoản?
                                                        <Link href="/dang-ky">
                                                            <a>
                                                                Đăng ký
                                                            </a>
                                                        </Link>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>

    )
}

export default ForgotPassword;