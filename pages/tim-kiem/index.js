import Authorize from '@/components/Authorize/Authorize';
import Footer from '@/components/footer';
import { listArea, listRangePrices, maxPeopleOfRoom } from '@/components/Header/search.model';
import Layout from '@/components/Layout/Layout';
import Navbar from '@/components/Navbar/Navbar';
import ProductSearchItem from "@/components/Product-item/product-search-item";
import { getCustomerData, sleep } from '@/lib/helper';
import LocationService from '@/lib/location/location.service';
import ProductService from '@/lib/products/product.service';
import { default as classes, default as searchHeader } from '@/styles/header/search.module.scss';
import search from '@/styles/product/search.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Pagination from 'rc-pagination';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import Select from 'react-select';
import SelectSearch from 'react-select-search/dist/cjs';
import 'react-select-search/style.css';
import * as Yup from 'yup';

const sortOptions = [
    { name: 'Giá từ thấp đến cao', value: 'price-asc' },
    { name: 'Giá từ cao đến thấp', value: 'price-desc' },
    { name: 'Theo bảng chữ cái a-z', value: 'name-asc' },
    { name: 'Theo bảng chữ cái z-a', value: 'name-desc' },
    { name: 'Đăng mới nhất', value: 'latest' },
    { name: 'Đăng cũ nhất', value: 'oldest' },
];

const itemRender = (current, type, element) => {
    if (type === 'page') {
        return <a href={`#${current}`}>{current}</a>;
    }
    return element;
};
const take = 10;
function ProductSearchPage(params = {}) {
    const { price, session, wishlistIds, buildings, buildingTotal, cart, customerData, houseTypes } = params;

    const { city, roomPeopleCount, district, sort, q, type } = params.queries;
    const router = useRouter();
    const [pricePicked, setPricePicked] = useState(price);
    const [products, setProducts] = useState(buildings ?? []);
    const [totalItem, setTotalItem] = useState(buildingTotal);
    const [sortValue, setSortValue] = useState(sort ?? null);

    const [titleSearch, setTitleSearch] = useState(null);
    const [valueSearch, setValueSearch] = useState(null);
    const [searchPageListRangePrices, setSearchPageListRangePrices] = useState(listRangePrices.map(p => ({
        ...p,
        checked: p.value == price
    })));
    const [searchArea, setSearchArea] = useState(listArea);
    const [maxPeopleCounts, setMaxPeopleCount] = useState(maxPeopleOfRoom);

    const cities = params.cities;
    const [districts, setDistrict] = useState(params.districts);

    const [cityName, setCityName] = useState(cities.find(c => c.codename === city)?.name);
    const [districtName, setDistrictName] = useState(districts.find(d => d.codename === district)?.name);

    const [isLoading, setIsLoading] = useState(false);
    const [cityPicked, setCityPicked] = useState(null);
    const [districtPicked, setDistrictPicked] = useState(null);
    const [houseTypePicked, setHouseTypePicked] = useState(null);
    const [peopleCountPicked, setPeopleCountPicked] = useState(roomPeopleCount ?? null);
    const [peopleCountPicked2, setPeopleCountPicked2] = useState(null);
    const [flag, setFlag] = useState(false);

    const [isDistrictLoading, setIsDistrictLoading] = useState(false);

    const [services, setServices] = useState(params.services ?? []);
    const [checkboxValueCount, setCheckboxValueCount] = useState(services.filter(s => s.isPicked)?.length ?? 0);

    const [pageCurrent, setPageCurrent] = useState(1);
    const [wishListIds, setWishListId] = useState(wishlistIds ?? []);
    const [flagSearchMobile, setFlagSearchMobile] = useState(false);
    const validationSchema = Yup.object().shape({
        q: Yup.string().nullable().notRequired().when('q', {
            is: (value) => value?.length,
            then: (rule) => rule.min(3, 'Từ khoá phải từ 3-200 ký tự'),
        }),
    }, [
        ['q', 'q']
    ]);

    const formOptions = {
        resolver: yupResolver(validationSchema), defaultValues: {
            q: q ?? null
        }
    };

    const { register, handleSubmit, reset, formState: { errors }, setValue } = useForm(formOptions);

    const { register: mobileRegister, handleSubmit: mobileHandleSubmit, reset: mobileReset, formState: {
        errors: mobileErrors
    }, setValue: mobileSetValue } = useForm(formOptions);

    useEffect(() => {
        if (!router.query || Object.keys(router.query).length === 0) {
            router.push('/tim-kiem?page=1');
        }

        const renderLocation = async () => {
            betweenPriceOnChange(price);
            houseTypeOnChange(type);
            peopleCountOnChange(roomPeopleCount);

            let titleSearch = 'Nhà đất cho thuê:';
            let valueSearch = 'Toàn quốc';

            if (city) {
                titleSearch = 'Nhà đất cho thuê khu vực:';
                valueSearch = cityName;
                if (district) {
                    valueSearch = districtName;
                }
            }
            if (q) {
                titleSearch = 'Nhà đất cho thuê khớp với từ khoá:';
                valueSearch = q;
            }
            setTitleSearch(titleSearch);
            setValueSearch(valueSearch);
        }

        renderLocation();
        toggleFilterMobile();
    }, []);

    const toggleFilterMobile = () => {
        var btn = document.querySelector("#toggle-filter-mobile");
        if (btn == null) {
            return 0;
        } else {
            var filter = document.querySelector("#filter-mobile");
            var btn_close = document.querySelector("#close-filter-mobile");
            var over_close = document.querySelector("#filter-mobile-overlay");
            btn.addEventListener("click", function () {
                filter.classList.toggle("active");
                over_close.classList.toggle("active")
            });

            btn_close.addEventListener("click", function () {
                filter.classList.remove("active");
                over_close.classList.remove("active")
            });
            over_close.addEventListener("click", function () {
                filter.classList.remove("active");
                over_close.classList.remove("active")
            });
        }
    }

    async function resetFilter() {
        setCityPicked(null);
        setDistrictPicked(null);
        setSortValue(null);
        setTitleSearch(null);
        setPeopleCountPicked(null);
        setPeopleCountPicked2(null);
        setMaxPeopleCount(maxPeopleCounts.map((s) => ({
            ...s,
            checked: false,
        })));
        setSearchPageListRangePrices(
            searchPageListRangePrices.map((p) => ({
                ...p,
                checked: false,
            }))
        );
        setSearchArea(
            listArea.map(item => ({
                ...item,
                checked: false,
            }))
        )
        setTitleSearch('Nhà đất cho thuê: ');
        setValueSearch('Toàn quốc');
        reset({ q: null });
        mobileReset({ q: null });
        await searchProduct();
    }

    const resetSearchForm = () => {
        setFlagSearchMobile(false)
        toggleFilterMobile()
        setPricePicked(null);
        setHouseTypePicked(null)
        setServices(services.map(c => ({
            ...c,
            isPicked: false,
        })));
        setCheckboxValueCount(0);
        setCityName('Toàn quốc');
        setDistrictName('Tất cả');
        resetFilter();
    }

    async function onSubmit(dataForm) {
        setFlagSearchMobile(false)
        const values = searchArea.map((item) => ({
            ...item,
            checked: false,
        }))
        setSearchArea(values)

        const serviceIds = services.filter(s => s.isPicked).map(s => s.id).join(',');

        dataForm.serviceIds = serviceIds;

        Object.keys(dataForm).forEach((k) => {
            if (!dataForm[k] || dataForm[k] == '' || dataForm[k] == 'undefined') {
                delete dataForm[k];
            }
        });

        let keyword = '';

        if (dataForm.city && !dataForm.district) {
            setTitleSearch('Nhà đất cho thuê khu vực:');
            const city = cities.find(c => c.codename === dataForm.city);
            keyword = city.name;
            setCityName(city.name);
        }

        if (dataForm.district) {
            const district = districts.find(d => d.codename === dataForm.district);
            setTitleSearch('Nhà đất cho thuê khu vực:');
            keyword = district.name;
            setDistrictName(district.name);
        }

        if (dataForm.q) {
            setTitleSearch('Nhà đất cho thuê khớp với từ khoá:');
            keyword = dataForm.q
        }
        if (!keyword) {
            setTitleSearch('Nhà đất cho thuê:');
            keyword = 'Toàn quốc'
        }
        setValueSearch(keyword);
        await searchProduct(dataForm);
    }

    const handleChange = async (name, value, idx) => {
        if (value === undefined) {
            return;
        }

        let filters = router.query;

        if (name === 'between_price') {
            const prices = value.split('-');
            filters.minPrice = `${parseInt(prices[0] + '000000', 10)}`.trim();
            filters.maxPrice = `${prices[1]}000000`.trim();
            setValue('minPrice', filters.minPrice);
            setValue('maxPrice', filters.maxPrice);
            mobileSetValue('minPrice', filters.minPrice);
            mobileSetValue('maxPrice', filters.maxPrice);

            const values = searchPageListRangePrices.map((s) => ({
                ...s,
                checked: false,
            }));

            values[idx].checked = true;
            setPricePicked(value)
            setSearchPageListRangePrices(values);
        }

        if (name == 'between_m2') {
            const area = value.split('-');
            filters = {
                ...filters,
                minArea: `${parseInt(`${area[0]}`, 10)}`,
                maxArea: `${parseInt(`${area[1]}`, 10)}`,
            };
            const values = searchArea.map((item) => ({
                ...item,
                checked: false,
            }))
            values[idx].checked = true;
            setSearchArea(values)
        }
        filters.page = 1;

        await searchProduct(filters);
    };

    const peopleCountChange = async (option) => {
        let filters = router.query;

        setValue('roomPeopleCount', option.value);
        mobileSetValue('roomPeopleCount', option.value);
        setPeopleCountPicked(option.value);
        setPeopleCountPicked2(option);
        filters.roomPeopleCount = option.value;
        await searchProduct(filters);
    };

    const searchProduct = async (filters) => {
        try {
            buildUrl(filters);
            setProducts([]);
            setIsLoading(true);
            await sleep(200);
            const res = await ProductService.listBuilding({ ...filters, take });
            await sleep(200);
            setIsLoading(false);

            const buildings = res.buildings.map(p => ({
                ...p,
                liked: wishListIds.includes(p.id)
            }));
            setProducts(buildings);
            setTotalItem(res.total);
        } catch (error) {
            alert(error.message ?? 'Có lỗi xảy ra');
            setIsLoading(false);
        }
    };

    const betweenPriceOnChange = (value) => {
        if (!value) {
            return
        }
        const idx = searchPageListRangePrices.findIndex(s => s.value === value);
        if (idx < 0) {
            return
        }

        const prices = value.split('-');
        setValue('minPrice', `${parseInt(`${prices[0]}000000`, 10)} `.trim());
        setValue('maxPrice', `${prices[1]}000000`.trim());
        mobileSetValue('minPrice', `${parseInt(`${prices[0]}000000`, 10)} `.trim());
        mobileSetValue('maxPrice', `${prices[1]}000000`.trim());
        const values = searchPageListRangePrices.map((s) => ({
            ...s,
            checked: false,
        }));
        values[idx].checked = true;
        setPricePicked(value)
        setSearchPageListRangePrices(values);
    }

    const houseTypeOnChange = (value) => {
        if (!value) {
            return
        }
        const houseType = houseTypes.find(t => t.value === value);
        setValue('type', houseType?.name);
        mobileSetValue('type', houseType?.name);
        setHouseTypePicked(houseType?.value);
    }

    const peopleCountOnChange = (value) => {
        if (!value) {
            return
        }
        const peopleCount = maxPeopleCounts.find(t => t.value === value);
        setValue('roomPeopleCount', peopleCount.name);
        mobileSetValue('roomPeopleCount', peopleCount.name);
        setPeopleCountPicked(peopleCount.value);
        setPeopleCountPicked2({
            label: peopleCount.name,
            value: peopleCount.name,
            id: peopleCount.name
        });
        setMaxPeopleCount(maxPeopleCounts.map(c => ({
            ...c,
            checked: c.value == value
        })));
    }

    const sortChanged = async (e) => {
        const filters = router.query;
        filters.sort = e;
        setSortValue(e);
        await searchProduct(filters);
    };

    const buildUrl = (newFilters) => {
        const queries = new URLSearchParams(newFilters);
        router.push(`/tim-kiem?${queries}`, undefined, {
            shallow: true
        });
    };

    const listDistricts = async (cityId) => {
        try {
            setIsDistrictLoading(true);
            const res = await LocationService.litsDistrict(cityId);
            setDistrict(res.districts);
            setIsDistrictLoading(false);
        } catch (error) {
            setIsDistrictLoading(false)
            alert(error.message ?? 'Có lỗi xảy ra')
        }
    }

    const cityChanged = async (option) => {
        setCityPicked(null);
        setDistrictPicked(null);
        setDistrictName('Tất cả');
        setCityName(option.label);
        setValue('city', option.value);
        mobileSetValue('city', option.value);
        await listDistricts(option.id);
    }

    const districtChanged = async (option) => {
        setDistrictPicked(null);
        setDistrictName(option.label);
        setValue('district', option.value);
        mobileSetValue('district', option.value);
    }

    const pageChange = async (current) => {
        const filters = router.query;
        filters.page = current;
        setPageCurrent(current)
        await searchProduct(filters);
    }

    const inputChange = (idx, newChecked) => {
        const values = services;
        values[idx].isPicked = newChecked;
        setServices(values);
        const checkedCheckboxes = values.filter(v => v.isPicked);
        setCheckboxValueCount(checkedCheckboxes.length);
    }

    const openPanel = () => {
        setFlag(!flag)
    }

    const toggleSearchMobile = () => {
        setFlagSearchMobile(!flagSearchMobile);
    }

    return (
        <Layout>
            <Navbar>
                <Authorize />
                <>
                    <section className="header-search-pages">
                        <div id={searchHeader.header_search}>
                            <div className="container">
                                <div className="header_search_main">
                                    <form onSubmit={handleSubmit(onSubmit)} className={searchHeader.header_search_form}>
                                        <div className="form-row fw_nw mg_t0 pd_y15">
                                            <div className={`${searchHeader.header_search_col} header-search-pages__select-box`}>
                                                <span className={searchHeader.search_icon}>
                                                    <i className="search-feature-icon ml-1"></i>
                                                </span>
                                                <input
                                                    name="q"
                                                    type="text"
                                                    {...register('q')}
                                                    placeholder="Tìm kiếm theo tên toà nhà, địa điểm"
                                                    className={`form - control ${errors.q ? 'is-invalid' : ''} ${searchHeader.form_input} `}
                                                />
                                            </div>
                                            <div className="header-search-pages__select-box">
                                                <Select
                                                    value={cityPicked}
                                                    options={cities.map(c => ({
                                                        label: c.name.includes('Tỉnh') ? c.name.replace('Tỉnh', '') : c.name.replace('Thành phố', ''),
                                                        value: c.codename,
                                                        id: c.id,
                                                    }))}
                                                    onChange={cityChanged}
                                                    placeholder="Tỉnh/Thành phố"
                                                    noOptionsMessage={(data) => `Không tìm thấy Tỉnh / Thành với từ khoá "${data.inputValue}"`}
                                                />
                                                <span className={searchHeader.city_name}>
                                                    {
                                                        cityName ? cityName.includes('Tỉnh') ? cityName?.replace('Tỉnh', '') : cityName?.replace('Thành phố', '') : 'Toàn quốc'
                                                    }
                                                </span>
                                            </div>
                                            <div className="header-search-pages__select-box">
                                                <Select
                                                    value={districtPicked}
                                                    options={
                                                        districts.map(d => ({
                                                            id: d.id,
                                                            label: d.name,
                                                            value: d.codename
                                                        }))
                                                    }
                                                    onChange={districtChanged}
                                                    isLoading={isDistrictLoading}
                                                    placeholder="Quận/Huyện"

                                                    noOptionsMessage={(data) => `Không tìm thấy Quận / Huyện với từ khoá "${data.inputValue}"`}
                                                />
                                                <span className={searchHeader.city_name}>{districtName ?? 'Tất cả'}</span>
                                            </div>
                                            <div className="header-search-pages__select-box">
                                                <button type="button" onClick={resetSearchForm} className={`${searchHeader.form_button_reset} dp_fl jc_c ai_c`}>
                                                    <span>Đặt lại</span>
                                                    <i className={`fa fa-repeat mg_l5 ${searchHeader.form_reset_icon} `}></i>
                                                </button>
                                            </div>
                                            <div className="header-search-pages__select-box d-flex align-items-center">
                                                <span className={searchHeader.form_filter_count}>
                                                    {checkboxValueCount}
                                                </span>
                                                <button type="button" onClick={openPanel}
                                                    id="show_search_advanced_button"
                                                    className={`${searchHeader.form_advanced_search_button} accordion`}>
                                                    Tìm kiếm nâng cao
                                                </button>
                                                <i className={flag == false ? `fa fa-angle-down mg_l5` : `fa fa-angle-up mg_l5`}></i>
                                            </div>
                                            <div className="header-search-pages__select-box">
                                                <button type="submit" className={`btn btn-primary ${searchHeader.form_submit} `}>
                                                    Tìm kiếm
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id={searchHeader.header_search_advanced} >
                            <div className={flag == false ? "slide-up" : "slide-down"}>
                                <div className="container">
                                    <div className="row mg_bt20">
                                        <div className="col-12">
                                            <h5>
                                                Thông tin bổ sung
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="row mg_bt20" >
                                        <div className={`col col - lg - 4 col - md - 4 col - sm - 6 ${searchHeader.header_search_advanced_col} `}>
                                            <SelectSearch
                                                options={listRangePrices}
                                                onChange={betweenPriceOnChange}
                                                placeholder="Khoảng giá"
                                                value={pricePicked}
                                            />
                                        </div>
                                        <div className={`col col - lg - 4 col - md - 4 col - sm - 6 ${searchHeader.header_search_advanced_col} `}>
                                            <SelectSearch
                                                value={houseTypePicked}
                                                options={houseTypes}
                                                placeholder="Chọn loại nhà"
                                                onChange={houseTypeOnChange}
                                            />
                                        </div>
                                        <div className={`col col - lg - 4 col - md - 4 col - sm - 6 ${searchHeader.header_search_advanced_col} `}>
                                            <SelectSearch
                                                value={peopleCountPicked}
                                                options={maxPeopleOfRoom}
                                                onChange={peopleCountOnChange}
                                                placeholder="Chọn số người ở"
                                            />
                                        </div>
                                    </div>
                                    <div className="row mg_bt20">
                                        <div className="col-12">
                                            <h5>
                                                Tiện tích
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12 dp_fl jc_fs ai_fs fw_w">
                                            {
                                                services.map((item, index) => (
                                                    <div className="form_check_box mg_bt15" key={item.id}>
                                                        <label className="form_check_label pointer" htmlFor={`checkBoxUtility${item.id} `}>
                                                            <input
                                                                className="form_check_input"
                                                                type="checkbox"
                                                                value={item.id}
                                                                id={`checkBoxUtility${item.id} `}
                                                                onChange={() => inputChange(index, !item.isPicked)}
                                                                checked={item.isPicked}
                                                                key={item.id}
                                                            />
                                                            <span>{item.name}</span>
                                                        </label>
                                                    </div>
                                                ))
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>
                    <section className="header-search-mobile">
                        <div className="container">
                            <button className="btn-open-search-mb" onClick={toggleSearchMobile}>
                                <i></i>
                                <span>Tìm kiếm theo tên toà nhà, địa điểm</span>
                            </button>
                        </div>

                        <div className={flagSearchMobile == true ? "search-mb-pop-up" : "d-none"}>
                            <div className="container">
                                <div className="title">
                                    <span>
                                        Tìm kiếm
                                    </span>

                                    <button onClick={toggleSearchMobile}>
                                        &times;
                                    </button>
                                </div>

                                <form className="search-mb-form" onSubmit={mobileHandleSubmit(onSubmit)}>
                                    <div className="input-box">
                                        <input
                                            name="q"
                                            type="text" {...mobileRegister('q')}
                                            placeholder="Tìm kiếm địa điểm, khu vực"
                                            className={`form-control ${mobileErrors.q ? 'is-invalid' : ''} ${classes.form_input}`}
                                        />
                                    </div>

                                    <div className="input-box">
                                        <Select
                                            value={cityPicked}
                                            options={cities.map(c => ({
                                                label: c.name.includes('Tỉnh') ? c.name.replace('Tỉnh', '') : c.name.replace('Thành phố', ''),
                                                value: c.codename,
                                                id: c.id,
                                            }))}
                                            onChange={cityChanged}
                                            placeholder=""
                                            noOptionsMessage={(data) => `Không tìm thấy Tỉnh/Thành với từ khoá "${data.inputValue}"`}
                                        />
                                        <span className="select-field">
                                            {
                                                cityName ? cityName.includes('Tỉnh') ? cityName?.replace('Tỉnh', '') : cityName?.replace('Thành phố', '') : 'Toàn quốc'
                                            }
                                        </span>
                                    </div>

                                    <div className="input-box">
                                        <Select
                                            value={districtPicked}
                                            options={
                                                districts.map(d => ({
                                                    id: d.id,
                                                    label: d.name,
                                                    value: d.codename
                                                }))
                                            }
                                            onChange={districtChanged}
                                            placeholder=""
                                            noOptionsMessage={(data) => `Không tìm thấy Quận/Huyện với từ khoá "${data.inputValue}"`}
                                            isLoading={isDistrictLoading}
                                        />
                                        <span className="select-field">{districtName ?? 'Tất cả'}</span>
                                    </div>

                                    <div className="input-box">
                                        <div className="advance-search d-flex align-items-center">
                                            <span className={classes.form_filter_count}>
                                                {checkboxValueCount}
                                            </span>
                                            <button type="button" onClick={openPanel}
                                                id="show_search_advanced_button"
                                                className={`${classes.form_advanced_search_button} accordion`}>
                                                Tìm kiếm nâng cao
                                            </button>
                                            <i className={flag == false ? `fa fa-angle-down mg_l5 ${classes.form_search_icon}` : `fa fa-angle-up mg_l5 ${classes.form_search_icon}`}></i>
                                        </div>
                                    </div>

                                    <div className={flag == false ? "input-box slide-up" : "input-box slide-down"}>
                                        <div className="advance-select">
                                            <SelectSearch
                                                value={peopleCountPicked}
                                                options={maxPeopleOfRoom}
                                                onChange={setPeopleCountPicked}
                                                placeholder="Chọn số người ở"
                                            />
                                        </div>

                                        <div className="advance-select">
                                            <SelectSearch
                                                value={pricePicked}
                                                options={listRangePrices}
                                                onChange={setPricePicked}
                                                placeholder="Khoảng giá"
                                            />
                                        </div>

                                        <div className="advance-select">
                                            <SelectSearch
                                                value={houseTypePicked}
                                                options={houseTypes}
                                                onChange={setHouseTypePicked}
                                                placeholder="Chọn loại nhà"
                                            />
                                        </div>

                                        <div className="advance-select has-bg">
                                            <p className="filter-title">
                                                Tiện ích
                                            </p>
                                            <div className="filter-utilities">
                                                {
                                                    services.map((item, index) => (
                                                        <div className="form_check_box mg_bt15" key={item.id}>
                                                            <label className="form_check_label pointer" htmlFor={`checkBoxUtility${item.id}`}>
                                                                <input
                                                                    className="form_check_input"
                                                                    type="checkbox"
                                                                    value={item.id}
                                                                    id={`checkBoxUtility${item.id}`}
                                                                    onChange={() => inputChange(index, !item.checked)}
                                                                    checked={item.isPicked}
                                                                    key={item.id}
                                                                />
                                                                <span>{item.name}</span>
                                                            </label>
                                                        </div>
                                                    ))
                                                }
                                            </div>
                                        </div>
                                    </div>

                                    <div className="input-box has-stick">
                                        <div className="actions">
                                            <button type="button" onClick={resetSearchForm} className="btn-reset-form">
                                                <span>Đặt lại</span>
                                                <i className={`fa fa-repeat mg_l5 ${classes.form_reset_icon}`}></i>
                                            </button>

                                            <button type="submit" className="btn-submit">
                                                Tìm kiếm
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </>
            </Navbar>

            <main>
                <section className="search-product-banner mt-3" >
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <img src="/assets/images/banner2.jpg" alt="img" />
                            </div>
                        </div>
                    </div>
                </section>

                <section className="main-breadcumb pd_t20">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb ">
                                <li className="breadcrumb-item">
                                    <Link href="/">Trang chủ</Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Tìm kiếm
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className={`${search.product_search_main} mg_bt30`}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12 col-md-8 col-lg-9">
                                <div className="search-page-title">
                                    <div className="title">
                                        <h1 className="mg_r10">
                                            {titleSearch}
                                            {valueSearch}
                                        </h1>
                                        <div className="dp_fl jc_fs ai_c">
                                            <div className={search.product_search_main_item_count}>
                                                <span>Hiện có {totalItem} bất động sản</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="sort">
                                        <SelectSearch
                                            value={sortValue}
                                            options={sortOptions}
                                            onChange={sortChanged}
                                            name="product_search_sort"
                                            placeholder="Sắp xếp"
                                        />

                                        <button className="btn-filter-mb" id="toggle-filter-mobile">
                                            <i></i>
                                        </button>
                                    </div>
                                </div>

                                <span className={`${search.product_search_filter_space} dp_fl mg_y25`}></span>

                                <div className="row">
                                    <div className={`col-12 col-md-12 col-lg-12 jc_c dp_fl ${isLoading ? '' : 'dp_n'} `}>
                                        <div className="spinner-border text-primary" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                    </div>

                                    {
                                        products.length === 0 && !isLoading ? (
                                            <>
                                                <div className="col-12 col-md-12 col-lg-12 jc_c dp_fl">
                                                    Không tìm thấy bất động sản nào
                                                </div>
                                            </>
                                        ) : (
                                            products.map(product => (
                                                <ProductSearchItem key={product.id} product={product}></ProductSearchItem>
                                            ))
                                        )
                                    }
                                </div>
                                {
                                    totalItem / take > 1 ? (<Pagination current={pageCurrent} total={totalItem} pageSize={take} itemRender={itemRender} onChange={pageChange} />) : ''
                                }
                            </div>
                            <div className="col-sm-12 col-md-4 col-lg-3">
                                <div className="filter-search-side-bar" id="filter-mobile">

                                    <div className={`${search.product_search_filter} pd_y10`}  >
                                        <Select
                                            value={peopleCountPicked2}
                                            options={maxPeopleOfRoom.map(c => ({
                                                label: c.name,
                                                value: c.value,
                                                id: c.id,
                                            }))}
                                            onChange={peopleCountChange}
                                            placeholder="Số người ở"
                                            className={'select_people_count'}
                                        />
                                    </div>

                                    <div className={`${search.product_search_filter}`}>
                                        <h3>Lọc theo khoảng giá</h3>
                                        <span className={search.product_search_filter_space}></span>

                                        <div className={search.product_search_filter_content}>
                                            {searchPageListRangePrices.map((p, idx) => (
                                                <div key={p.value}>
                                                    <label htmlFor={`let_than_${idx} tr`} className={search.product_search_filter_radio}>
                                                        <input
                                                            key={p.value}
                                                            id={`let_than_${idx} tr`}
                                                            value={p.value}
                                                            name="between_price"
                                                            type="radio"
                                                            checked={p.checked}
                                                            onChange={() => handleChange('between_price', p.value, idx)}
                                                        />
                                                        <span> {p.name}</span>
                                                    </label>
                                                </div>
                                            ))}
                                        </div>
                                    </div>

                                    <div
                                        className={`${search.product_search_filter}`}
                                    >
                                        <h3>Lọc theo diện tích</h3>
                                        <span className={search.product_search_filter_space}></span>
                                        <div className={search.product_search_filter_content}>
                                            {
                                                searchArea.map((item, idx) => (
                                                    <div key={item.value}>
                                                        <label htmlFor={`let_than_${idx}_m2`} className={search.product_search_filter_radio}>
                                                            <input
                                                                key={item.value}
                                                                id={`let_than_${idx}_m2`}
                                                                value={item.value}
                                                                name="between_m2"
                                                                type="radio"
                                                                checked={item.checked}
                                                                onChange={() => handleChange('between_m2', item.value, idx)}
                                                            />
                                                            <span> {item.name}</span>
                                                        </label>
                                                    </div>
                                                ))
                                            }

                                        </div>
                                    </div>

                                    <button id="close-filter-mobile">
                                        &times;
                                    </button>
                                </div>

                                <div id="filter-mobile-overlay"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <Footer />
        </Layout>
    );
};

export async function getServerSideProps({ req, query, res }) {
    try {
        const customerData = await getCustomerData(res, req, true);

        const params = {
            queries: {
                ...query,
            },
            ...customerData
        };

        let page = params.queries.page;
        params.queries.take = take;
        if (!page || isNaN(parseInt(page, 10))) {
            params.queries.page = 1;
        }

        const buildingRes = await ProductService.listBuilding(params.queries);

        const buildings = buildingRes.buildings.map(b => ({
            ...b,
            liked: params.wishlistIds.includes(b.id)
        }));

        params.buildings = buildings;

        params.buildingTotal = buildingRes.total;

        const buildingTypeRes = await ProductService.listType();

        params.buildingTypes = buildingTypeRes.types;

        let getMinPrice = query.minPrice;
        let getMaxPrice = query.maxPrice;

        if (getMinPrice && getMaxPrice) {
            const newMinPrice = isNaN(parseInt(getMinPrice, 10)) ? 0 : parseInt(getMinPrice, 10) / 1000000;
            const newMaxPrice = isNaN(parseInt(getMaxPrice, 10)) ? 0 : parseInt(getMaxPrice, 10) / 1000000;
            const rangePrice = `${newMinPrice}-${newMaxPrice}`;
            params.price = rangePrice;
        }

        const buildRes = await ProductService.listService();
        const servicePickIds = query.serviceIds ?? [];
        params.services = buildRes.services.filter(s => s.type === "Thiết bị").map(i => ({
            ...i,
            isPicked: servicePickIds.includes(i.id)
        }));

        let newCities = [];
        let newDistricts = [];

        const listCityRes = await LocationService.listCity();
        newCities = listCityRes.cities;

        if (params.queries.city) {
            const city = newCities.find(c => c.codename === params.queries.city)
            const districtRes = await LocationService.litsDistrict(city.id);
            newDistricts = districtRes.districts;
        }

        const typeRes = await ProductService.listType();
        params.houseTypes = typeRes.types.map(t => ({
            value: t,
            name: t,
            checked: false
        }));

        params.cities = newCities;
        params.districts = newDistricts;

        if (Object.keys(query).length === 0) {
            return {
                props: {
                    ...params,
                    queries: {
                        page: 1
                    },
                }
            };
        }
        return {
            props: params
        };
    } catch (error) {
        console.log('error:', error)

        return {
            props: {}
        }
    }
};

export default ProductSearchPage;
