module.exports = {
    webpack5: true,
    images: {
        loader: "imgix",
        path: "",
    },
    env: {
        PUBLIC_URL: '/',
    }
}
