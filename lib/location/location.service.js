import axios from 'axios';

const baseURL = 'https://api.dev.318.suplo.vn/v1';

export default class LocationService {
    static async listCity() {
        const response = await axios.get(`/cities`, {
            baseURL
        })
        return response.data;
    }

    static async litsDistrict(cityId) {
        const response = await axios.get(`/districts`, {
            baseURL,
            params: {
                cityId
            }
        })
        return response.data;
    }
}