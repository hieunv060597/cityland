import axios from 'axios';

const baseURL = 'https://api.dev.318.suplo.vn/v1';

export default class OrderService {
    static async list(params, accessToken) {
        const response = await axios.get(`/customers/self/orders`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            },
            params,
            baseURL
        })
        return response.data;
    }

    static async get(id, accessToken) {
        const response = await axios.get(`/customers/self/orders/${id}`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            },
            baseURL
        })
        return response.data;
    }
}