
export const AboutUsSettings = {
    Title: {
        title1: "Tầm nhìn, sứ mệnh",
        title2: "tiện ích",
        title3: "đội ngũ của chúng tôi"
    },

    Content: {
        section1: {
            images: "/assets/images/aimg1.jpg"
        },
        section2: {
            text1: "Trở thành hệ sinh thái căn hộ tiện ích cho thuê hàng đầu Việt Nam.Mang không gian sống hạnh phúc tới mọi người bằng việc tiên phong chuyển đổi số.",
            text2: "Tại TingTong chúng tôi khát khao trong việc tiên phong đi đầu và tạo ra các tiêu chuẩn dành riêng cho cộng đồng thuê trọ.Tiêu chuẩn tại TingTong:",
            images: "/assets/images/aimg2.jpg"
        },
        section3: {
            listContent: [
                {
                    images: "/assets/images/aicon1.svg",
                    text: "Với kinh nghiệm quản trị hệ thống lớn hơn 1000 căn hộ trải dài khắp Hà Nội."
                },

                {
                    images: "/assets/images/aicon2.svg",
                    text: "Đội ngũ nhân sự chuyên nghiệp, nhiệt huyết"
                },

                {
                    images: "/assets/images/aicon3.svg",
                    text: "Áp dụng hệ thống quy trình và hệ thống công nghệ vào quản lý"
                },

                {
                    images: "/assets/images/aicon4.svg",
                    text: "Đội ngũ vệ sinh, bảo trì – sửa chữa riêng biệt đáp ứng nhu cầu của khách hàng và toà nhà"
                },

                {
                    images: "/assets/images/aicon5.svg",
                    text: "Hợp đồng rõ ràng, công khai – minh bạch trong suốt quá trình hợp tác"
                },
            ]
        },
        section4: {
            image: "/assets/images/aimg3.jpg",
            text1: "Thành công của chúng tôi được tạo nên từ sự nhiệt huyết và chuyên môn của đội ngũ nhân sự. Chúng tôi luôn giữ vững năng lượng trong công việc, chuẩn chỉ trong quy trình và tận tâm với từng khách hàng, đối tác.",
            list: [
                {
                    images: "/assets/images/aic1.svg",
                    title: "CHU TOÀN BỔN PHẬN",
                    subtitle: "(Trung thực - Quyết liệt - Chủ động)"
                },

                {
                    images: "/assets/images/aic2.svg",
                    title: "PHÁT TRIỂN KHÔNG NGỪNG",
                    subtitle: "(Sáng tạo - Cải tiến - Học tập trọn đời)"
                },

                {
                    images: "/assets/images/aic3.svg",
                    title: "HƯỚNG TỚI KHÁCH HÀNG",
                    subtitle: "(Yêu thương - Chia sẻ - Tận tâm phục vụ)"
                },

                {
                    images: "/assets/images/aic4.svg",
                    title: "TRÁCH NHIỆM XÃ HỘI",
                    subtitle: "(Trung thực - Quyết liệt - Chủ động)"
                }
            ]
        }
    }
}