export const CooperateSettings = {
    Title: {
        titleBanner: {
            mainTitle: "Anh Chị là Chủ nhà - Chủ đầu tư - Các đơn vị vận hành nhỏ lẻ",
            subTitle: "Đang đau đầu vì ?"
        },

        normalTitle: {
            title1: "Hãy để chúng tôi giúp Anh Chị",
            title2: "Anh chị nhận được gì ?"
        }

    },

    Content: {
        imageBanner: `/assets/images/bnabout.jpg`,

        listContentBanner: [
            "Quản lý vận hành đau đầu, tốn nhiều thời gian, không có thời gian cho bản thân và gia đình…",
            "An ninh – An toàn phức tạp: Tụ tập, mất trộm mất cắp, xảy ra nhiều loại hình vi phạm pháp luật…",
            "Không biết Maketing dẫn tới trống phòng, khiến doanh thu thấp hoặc thua lỗ",
            "Mệt mỏi khi thu tiền: thu tiền nhỏ lẻ, chây ì, không nộp tiền…",
            "Gặp đủ các đối tượng thành phần và rất phức tạp",
            "Sửa chữa, hỏng hóc nhiều, khách gọi ngay cả nửa đêm…",
            "Vận hành mệt mỏi mà lãi ít, không thấy tiền đâu do làm nhỏ lẻ, quản trị kém"
        ],

        section_2_Content: [
            {
                images: "/assets/images/about1.svg",
                text: "Với kinh nghiệm quản trị hệ thống lớn hơn 1000 căn hộ trải dài khắp Hà Nội."
            },

            {
                images: "/assets/images/about2.svg",
                text: "Đội ngũ nhân sự chuyên nghiệp, nhiệt huyết"
            },

            {
                images: "/assets/images/about3.svg",
                text: "Áp dụng hệ thống quy trình và hệ thống công nghệ vào quản lý"
            },

            {
                images: "/assets/images/about4.svg",
                text: "Đội ngũ vệ sinh, bảo trì – sửa chữa riêng biệt đáp ứng nhu cầu của khách hàng và toà nhà"
            },

            {
                images: "/assets/images/about5.svg",
                text: "Hợp đồng rõ ràng, công khai – minh bạch trong suốt quá trình hợp tác"
            },
        ],

        section_3_Content: {
            images: "/assets/images/about_img_2.jpg",
            listGive: [
                {
                    id: "1",
                    content: "Giải phóng thời gian: Thoải mái dành thời gian cho sở thích, công việc bản thân và gia đình"
                },
                {
                    id: "2",
                    content: "Thu nhập ổn định: Nhận khoản thu nhập định kỳ, đúng hạn mà không cần đi thu tiền trực tiếp"
                },

                {
                    id: "3",
                    content: "Không còn đau đầu lo lắng về các vấn đề vệ sinh, an ninh – an toàn, sửa chữa sự cố,…"
                },

                {
                    id: "4",
                    content: "Gia tăng thu nhập: Yên tâm mở rộng quy mô mà không còn lo tình trạng trống phòng vì đã có chúng tôi vận hành giúp anh/chị."
                },
            ]
        }
    }
}