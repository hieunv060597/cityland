export const menuSettings = [
    {
        name: 'Trang chủ',
        value: '/'
    },
    {
        name: 'Hợp tác',
        value: '/hop-tac'
    },
    {
        name: 'Về chúng tôi',
        value: '/ve-chung-toi'
    },
    {
        name: 'Tin tức',
        value: '/bai-viet',
        subMenu: [
            {
                name: 'Tin tức 1',
                value: '/bai-viet',
            },
            {
                name: 'Tin tức 2',
                value: '/bai-viet',
            }
        ]
    },
    {
        name: 'Liên hệ',
        value: '/lien-he'
    },
]