import axios from 'axios';

const baseURL = 'https://api.dev.318.suplo.vn/v1';

export default class AuthService {
    static async post(data) {
        const response = await axios.post(`${baseURL}/auth/customer/reset_password`, data,);
        return response.data;
    }

    static async getUser(token) {
        const response = await axios.get(`${baseURL}/customers/self`, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })
        return response.data;
    }
}
