import axios from 'axios';

const baseURL = 'https://api.dev.318.suplo.vn/v1';
const shopId = '017c691c-b991-f95a-07f2-62da5b63ab07';

export default class BlogService {
    static async list() {
        const res = await axios.post('/cart?domain=tingtong', {
            variantId,
            quantity
        }, {
            headers: {
                'X-Suplo-Shop-Id': shopId,
            },
            baseURL
        });
        return res.data;
    }

    static async listArticle(params) {
        const res = await axios.get('/articles', {
            headers: {
                'X-Suplo-Shop-Id': shopId,
            },
            params,
            baseURL
        });
        return res.data;
    }

    static async getArticle(id) {
        const res = await axios.get('/articles/' + id, {
            headers: {
                'X-Suplo-Shop-Id': shopId,
            },
            baseURL
        });
        return res.data;
    }
}