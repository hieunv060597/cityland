import axios from 'axios';

const baseURL = process.env.NEXT_PUBLIC_BASE_URL;

export default class CustomerAddressService {
    static async list(token) {
        const response = await axios.get(`${baseURL}/customers/self/addresses`, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        });
        return response.data;
    }

    static async create(token, data) {
        const response = await axios.post(`${baseURL}/customers/self/addresses`, {
            address: data
        }, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        });
        return response.data;
    }

    static async update(token, data, id) {
        const response = await axios.put(`${baseURL}/customers/self/addresses/${id}`, {
            address: data
        }, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        });
        return response.data;
    }
}
