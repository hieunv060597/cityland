import axios from 'axios';


const baseURL = process.env.NEXT_PUBLIC_BASE_URL;

export default class UserService {
    static async getUser(token) {
        const response = await axios.get(`${baseURL}/customers/self?withWallet=true&withReferrer=true`, {
            headers: {
                'Authorization': `Bearer ${token}`
            },

        })
        return response.data;
    }

    static async update(token, data) {
        const response = await axios.patch(`${baseURL}/customers/self/`, data, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        });
        return response.data;
    }

    static async changePassword(token, data) {
        const response = await axios.put(`${baseURL}/customers/self/password`, data, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        });
        return response.data;
    }
}