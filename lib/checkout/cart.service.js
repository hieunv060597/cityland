import axios from 'axios';

const baseURL = 'https://api.dev.318.suplo.vn/v1';
const shopId = '017c691c-b991-f95a-07f2-62da5b63ab07';

export default class CartService {
    static async addItem(variantId, quantity) {
        const res = await axios.post('/cart?domain=tingtong', {
            variantId,
            quantity
        }, {
            headers: {
                'X-Suplo-Shop-Id': shopId,
            },
            baseURL
        });
        return res.data;
    }

    static async get(token) {
        const res = await axios.get('/cart', {
            headers: {
                'X-Suplo-Shop-Id': shopId,
            },
            params: {
                token
            },
            baseURL
        });
        return res.data;
    }

    static async updateItem(token, variantId, quantity) {
        const res = await axios.put(`/cart/items/${variantId}`, {
            quantity: parseInt(quantity, 10)
        }, {
            params: {
                token
            },
            baseURL
        });
        return res.data;
    }
}