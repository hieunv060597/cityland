import axios from 'axios';

const baseURL = 'https://api.dev.318.suplo.vn/v1';

export default class CheckoutService {
    static async post(token, checkoutDataReq, accessToken) {
        const res = await axios.post('/checkout/orders', checkoutDataReq, {
            baseURL,
            params: {
                token
            },
            headers: {
                Authorization: accessToken ? `Bearer ${accessToken}` : undefined
            }
        });
        return res.data;
    }

    static async get(token) {
        const res = await axios.get('/checkout/orders', {
            params: {
                token
            },
            baseURL
        });
        return res.data;
    }
}
