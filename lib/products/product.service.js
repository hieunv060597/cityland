import axios from 'axios';

const baseURL = process.env.NEXT_PUBLIC_BASE_URL;
const shopId = process.env.NEXT_PUBLIC_SHOP_ID;

export default class ProductService {
    static async list(params) {
        let skip = 5 * (parseInt(params?.page, 10) - 1);
        if (isNaN(skip)) {
            skip = 0
        }
        params = {
            ...params,
            skip
        }
        const response = await axios.get(`/products`, {
            headers: {
                'X-Suplo-Shop-Id': shopId,
            },
            params,
            baseURL
        })
        return response.data;
    }


    static async listType() {
        const response = await axios.get(`/building/types`, {
            baseURL
        })
        return response.data;
    }

    static async listBuilding(params) {
        let skip = params.take * (parseInt(params?.page, 10) - 1);
        if (isNaN(skip)) {
            skip = 0
        }
        params = {
            ...params,
            skip
        }
        const response = await axios.get(`/buildings`, {
            params,
            baseURL: baseURL
        })
        return response.data;
    }

    static async getBuilding(id) {
        const response = await axios.get(`/buildings/${id}`, {
            baseURL: baseURL
        })
        return response.data;
    }

    static async get(id) {
        const response = await axios.get(`/products/${id}`, {
            headers: {
                'X-Suplo-Shop-Id': shopId,
            },
            baseURL
        })
        return response.data;
    }

    static async getWishList(token, params) {
        const response = await axios.get(`${baseURL}/customers/self/wishlist`, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
            params,
        })
        return response.data;
    }

    static async deleteWishList(token, id) {
        const response = await axios.delete(`${baseURL}/customers/self/wishlist/${id}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })
        return response.data;
    }

    static async addWishList(token, id) {
        const response = await axios.post(`${baseURL}/customers/self/wishlist`, { "productId": id }, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })
        return response.data;
    }

    static async listService() {
        const response = await axios.get(`${baseURL}/services`)
        return response.data;
    }

    static async getRoom(id) {
        const response = await axios.get(`${baseURL}/rooms/${id}`)
        return response.data;
    }


    static listBuilding2(params) {
        let skip = params.take * (parseInt(params?.page, 10) - 1);
        if (isNaN(skip)) {
            skip = 0
        }
        params = {
            ...params,
            skip
        }
        const response = axios.get(`/buildings`, {
            params,
            baseURL: baseURL
        })
        return response.data;
    }
}