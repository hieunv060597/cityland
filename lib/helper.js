
import { serialize, parse } from 'cookie';
import CartService from './checkout/cart.service';
import UserService from './users/user.service';
import Link from "next/link";
import ProductService from './products/product.service';
import eventEmitter from './eventEmitter';
import { alertService } from './alert';
import { getSession } from 'next-auth/client';

export const removeAccent = (str) => {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
    str = str.replace(/Đ/g, 'D');
    str = str.replace(/\s+/g, '-');
    return str;
};

export const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
};

export const formatMoney = (price) => {
    return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(price);
}

export const fuzzySearch = (options) => {
    return (value) => {
        if (!value.length) {
            return options;
        }
        const newValue = removeAccent(value).toLowerCase();
        return options.filter(s => {
            const newKey = removeAccent(s.name).toLowerCase();
            return newKey.includes(newValue);
        })
    };
};

export const formatDate = (date, withTime) => {
    if (!date) {
        return '';
    }
    const newDate = new Date(date);
    let day = newDate.getDate();
    let month = newDate.getMonth();
    const year = newDate.getFullYear();
    month = month + 1 < 10 ? `0${month + 1}` : month + 1;
    let time = '';
    if (withTime) {
        const minutes = newDate.getMinutes();
        const hours = newDate.getHours();
        time = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
    }
    return `${time} ${day.toString().padStart(2, '0')}/${month}/${year}`;
};

export const convertDistrictName = (districtName) => {
    if (!districtName || districtName.length === 0) {
        return ''
    }

    const newDistrictName = districtName.split(' ');
    newDistrictName.shift();

    return newDistrictName.join(' ')
}

export const convertCityName = (name) => {
    if (!name || name.length === 0) {
        return ''
    }

    return name.includes('Tỉnh') ? name.replace('Tỉnh', '') : name.replace('Thành phố', '')
}

export const setClientCookie = (cname, cvalue, exdays) => {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export const getClientCookie = (cname) => {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export const getCookieOfReq = (req, name) => {
    var nameEQ = name + "=";
    var ca = req.headers.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

export const redirectIfUnauthenticated = async (session, res) => {
    if (!session?.user?.accessToken) {
        res.writeHead(301, { Location: '/dang-nhap' })
        res.end()
        return true
    }
}

export const MAX_AGE = 60 * 60 * 24 * 30 // 8 hours

export function serverSetCookie(res, cookieName, value) {
    const cookie = serialize(cookieName, value, {
        maxAge: MAX_AGE,
        expires: new Date(Date.now() + MAX_AGE * 1000),
        httpOnly: true,
        secure: false,
        path: '/',
        sameSite: 'lax',
    })

    res.setHeader('Set-Cookie', cookie)
}

export function serverRemoveCookie(res, cookieName) {
    const cookie = serialize(cookieName, '', {
        maxAge: -1,
        path: '/',
    })

    res.setHeader('Set-Cookie', cookie)
}

export function parseCookies(req) {
    if (req.cookies) {
        return req.cookies
    }
    const cookie = req.headers?.cookie
    return parse(cookie || '')
}

export function serverGetCookie(req, cookieName) {
    const cookies = parseCookies(req)
    return cookies[cookieName]
}


export const getCustomerData = async (res, req, isGetWishlist, isCheckAuth) => {
    const session = await getSession({ req });

    if (isCheckAuth) {
        redirectIfUnauthenticated(session, res);
    }

    const cartToken = getCookieOfReq(req, 'ting_tong_cart_token');
    const data = {
        cart: null,
        customer: null,
        wishlistCount: 0,
        wishlistIds: [],
        session: session ?? null
    }

    if (cartToken) {
        try {
            const cartRes = await CartService.get(cartToken);
            if (cartRes) {
                data.cart = cartRes.cart;
            }
        } catch (error) {
            serverRemoveCookie(res, 'ting_tong_cart_token')
        }
    }

    if (session?.user?.accessToken) {
        const userRes = await UserService.getUser(session?.user.accessToken);
        data.customer = userRes.customer;
        if (isGetWishlist) {
            const reloadWishlist = await ProductService.getWishList(session?.user?.accessToken);
            data.wishlistIds = reloadWishlist.product.map(p => p.id);
            data.wishlistCount = reloadWishlist.total;
        }
    }
    return data
}

export const alertUnauthorize = async () => {
    eventEmitter.emit('show_global_popup', {
        message: (
            <>
                <span className="dp_b mg_bt15">Bạn cần đăng nhập để thực hiện hành động này</span>
                <Link href="/dang-nhap">
                    <button type="button" className="btn btn-primary">
                        Đi đến đăng nhập
                    </button>
                </Link>
            </>
        )
    })
    return
}


export const showGlobalPopup = (message) => {
    eventEmitter.emit('show_global_popup', {
        message
    })
}



export const handleWishlist = async (id, action, token, wishlistCount, wishlistIds) => {
    if (action) {
        const response = await ProductService.addWishList(token, id);
        wishlistIds = [...wishlistIds, response.product.id];
        wishlistCount++
    } else {
        await ProductService.deleteWishList(token, id);
        wishlistIds = wishlistIds.filter(wId => wId !== id);
        wishlistCount--
    }

    return {
        wishlistCount,
        wishlistIds
    }
}