import { createContext, useEffect, useState } from "react";

const CustomerContext = createContext();

function CustomerProvider(params) {
    const { children, props } = params;
    const [wishlistCount, setWishlistCount] = useState(props?.wishlistCount ?? 0);
    const [wishlistIds, setWishlistId] = useState(props?.wishlistIds ?? []);
    const [totalDisable, setTotalDisable] = useState(false)

    const customerData = {
        cart: children?.props?.cart ?? null,
        customer: children?.props?.customer ?? null,
        wishlistCount: wishlistCount,
        setWishlistCount,
        wishlistIds,
        setWishlistId,
        session: children?.props?.session ?? null,
        totalDisable,
        setTotalDisable,
        pathName: props.pathName
    }

    useEffect(() => {
        setWishlistCount(props?.wishlistCount);
    }, [props?.wishlistCount])
    useEffect(() => {
        setWishlistId(props?.wishlistIds);
    }, [props?.wishlistIds]);

    return (
        <CustomerContext.Provider value={customerData}>
            {children}
        </CustomerContext.Provider>
    )
}

export { CustomerContext, CustomerProvider }

