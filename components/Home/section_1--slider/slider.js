import React from 'react';
import SwiperCore, { Pagination } from 'swiper';
import "swiper/css";
import { Swiper, SwiperSlide } from 'swiper/react';


SwiperCore.use([Pagination]);

const Mainslider = () => {
    return (
        <>
            <section className="home-slider">
                <Swiper slidesPerView={1} loop={true} pagination={{ "clickable": true }} navigation={false} autoplay={{
                    "delay": 5000, "disableOnInteraction": false
                }} className="mySwiper">
                    <SwiperSlide>
                        <div className="home-banner">
                            <img src="/assets/images/slider1.jpg" alt="ting tong banner" />
                        </div>
                    </SwiperSlide>

                    <SwiperSlide>
                        <div className="home-banner">
                            <img src="/assets/images/slider1.jpg" alt="ting tong banner" />
                        </div>
                    </SwiperSlide>

                    <SwiperSlide>
                        <div className="home-banner">
                            <img src="/assets/images/slider1.jpg" alt="ting tong banner" />
                        </div>
                    </SwiperSlide>

                </Swiper>
            </section>
        </>
    )
}

export default Mainslider;