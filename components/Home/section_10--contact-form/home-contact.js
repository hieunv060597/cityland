import React from "react";

const HomeContact = () => {
    return (
        <section className="home-contact">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-6">
                        <div className="home-contact__text">
                            <div className="media">
                                <img src="/assets/images/icon_home_contact.svg" alt="" className="mr-3" />
                                <div className="media-body">
                                    <h4>
                                        Liên hệ nhanh
                                    </h4>

                                    <p className="d-none d-sm-none d-md-none d-lg-block">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquet nunc scelerisque lobortis vulputate pretium, vitae, neque orci. Ultrices vitae et lacus, aliquam ac tempor.
                                    </p>
                                </div>
                            </div>

                            <p className="d-block d-sm-none d-md-none d-lg-none">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquet nunc scelerisque lobortis vulputate pretium, vitae, neque orci. Ultrices vitae et lacus, aliquam ac tempor.
                            </p>
                        </div>
                    </div>

                    <div className="col-12 col-sm-12 col-md-12 col-lg-6">
                        <form className="home-contact__form">
                            <div className="row no-gutters">
                                <div className="col-6 col-sm-6 col-md-4 col-lg-4">
                                    <input type="text" placeholder="Điền tên của bạn" className="form-control" />
                                </div>

                                <div className="col-6 col-sm-6 col-md-4 col-lg-4">
                                    <input type="text" placeholder="Số điện thoại" className="form-control" />
                                </div>

                                <div className="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <input type="text" placeholder="Email liên hệ" className="form-control" />
                                </div>

                                <div className="col-9 col-lg-10">
                                    <input type="text" placeholder="Mong muốn của bạn là" className="form-control" />
                                </div>

                                <div className="col-3 col-lg-2">
                                    <button className="btn btn-home-contact">
                                        <span>Gửi đi</span>
                                        <img src="/assets/images/ic_send.svg" alt="" />
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}


export default HomeContact;
