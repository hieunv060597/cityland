
import { homeSettings } from '@/lib/settings/home';
import React from "react";
import SwiperCore, {
    Pagination
} from 'swiper';
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from 'swiper/react';
import ProductItem from "@/components/Product-item/product-box";


SwiperCore.use([Pagination]);

const ListProduct = ({ products }) => {
    return (
        <section className="home-product-slider__1">
            <div className="container">
                <div className="homepages-title">
                    <h2 className="main-title">
                        {homeSettings.moduleMostInterested.titleOne}
                    </h2>

                    <p className="sub-title">
                        {homeSettings.moduleMostInterested.titleTwo}
                    </p>
                </div>

                <Swiper slidesPerView={'auto'} spaceBetween={30} loop={true} pagination={{
                    "dynamicBullets": true
                }} navigation={false} autoplay={{
                    "delay": 5000
                }}
                    breakpoints={{
                        "300": {
                            "slidesPerView": 'auto',
                            "spaceBetween": 16
                        },
                        "768": {
                            "slidesPerView": 2,
                        },
                        "886": {
                            "slidesPerView": 3,
                        },
                        "1024": {
                            "slidesPerView": 3,
                        },
                        "1368": {
                            "slidesPerView": 3,
                        }
                    }}
                    className="mySwiper">
                    {products.map(product => (
                        <SwiperSlide key={product.id}>
                            <ProductItem product={product} name={"ListProduct"}  ></ProductItem>
                        </SwiperSlide>
                    ))}

                </Swiper>

            </div>
        </section>
    )
}

export default ListProduct;
