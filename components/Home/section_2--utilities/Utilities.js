import React, { useRef, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import "swiper/css";
import "swiper/css/pagination"

import SwiperCore, {
    Pagination
} from 'swiper';

SwiperCore.use([Pagination]);



const Utilities = () => {

    return (
        <section className="home-utilities">
            <div className="container">
                <div className="row">
                    <div className="col-md-4 col-lg-2">
                        <div className="home-utilities-box d-flex align-items-center h-100">
                            <p>
                                tiêu chuẩn tại
                            </p>

                            <img src="/assets/images/u_logo.svg" alt="utilities" />
                        </div>
                    </div>

                    <div className="col-md-4 col-lg-2">
                        <div className="home-utilities-box">
                            <p>
                                đặt phòng dễ dàng với <span className="utilities-color">tingtong</span>
                            </p>
                        </div>
                    </div>

                    <div className="col-md-4 col-lg-2">
                        <div className="home-utilities-box">
                            <p>
                                ảnh <span className="utilities-color">thật</span>
                            </p>

                            <p>
                                phòng <span className="utilities-color">thật</span>
                            </p>
                        </div>
                    </div>

                    <div className="col-md-4 col-lg-2">
                        <div className="home-utilities-box">
                            <p>
                                cộng đồng dân cư <span className="utilities-color">văn minh</span>
                            </p>
                        </div>
                    </div>

                    <div className="col-md-4 col-lg-2">
                        <div className="home-utilities-box">
                            <p>
                                Xử lí
                            </p>

                            <p>
                                sự cố <span className="utilities-color">48h</span>
                            </p>
                        </div>
                    </div>

                    <div className="col-md-4 col-lg-2">
                        <div className="home-utilities-box">
                            <p>
                                hỗ trợ làm tttv
                            </p>

                            <p>
                                <span className="utilities-color">miễn phí 100%</span>
                            </p>
                        </div>
                    </div>
                </div>

                <div className="home-utilities-slider">
                    <Swiper slidesPerView={2} loop={true} pagination={{ "clickable": true }} navigation={false} autoplay={{
                        "delay": 5000, "disableOnInteraction": false
                    }} breakpoints={{
                        "300": {
                            "slidesPerView": 2,
                        },
                        "768": {
                            "slidesPerView": 4,
                        },
                        "886": {
                            "slidesPerView": 4,
                        },
                        "1024": {
                            "slidesPerView": 4,
                        },
                        "1368": {
                            "slidesPerView": 4,
                        }
                    }} className="mySwiper">
                        <SwiperSlide>
                            <div className="home-utilities-box d-flex align-items-center h-100">
                                <p>
                                    tiêu chuẩn tại
                                </p>

                                <img src="/assets/images/u_logo.svg" alt="utilities" />
                            </div>
                        </SwiperSlide>

                        <SwiperSlide>
                            <div className="home-utilities-box">
                                <p>
                                    đặt phòng dễ dàng với <span className="utilities-color">tingtong</span>
                                </p>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide>
                            <div className="home-utilities-box">
                                <p>
                                    cộng đồng dân cư <span className="utilities-color">văn minh</span>
                                </p>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide>
                            <div className="home-utilities-box">
                                <p>
                                    Xử lí
                                </p>

                                <p>
                                    sự cố <span className="utilities-color">48h</span>
                                </p>
                            </div>
                        </SwiperSlide>

                        <SwiperSlide>
                            <div className="home-utilities-box">
                                <p>
                                    hỗ trợ làm tttv
                                </p>

                                <p>
                                    <span className="utilities-color">miễn phí 100%</span>
                                </p>
                            </div>
                        </SwiperSlide>
                    </Swiper>
                </div>
            </div>
        </section>
    )
}

export default Utilities;