import Link from "next/link";
import React from 'react';
import SwiperCore, {
    Pagination
} from 'swiper';
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from 'swiper/react';
import { homeSettings } from '@/lib/settings/home'

SwiperCore.use([Pagination]);

const Caring = () => {
    return (
        <section className="caring-area">
            <div className="container">
                <div className="homepages-title">
                    <h2 className="main-title">
                        {homeSettings.moduleGreatInterest.titleOne}
                    </h2>
                </div>

                <Swiper slidesPerView={3} spaceBetween={30} loop={true} pagination={{
                    "dynamicBullets": true
                }} navigation={false} autoplay={{
                    "delay": 5000
                }}
                    breakpoints={{
                        "300": {
                            "slidesPerView": 2,
                            "spaceBetween": 16
                        },
                        "768": {
                            "slidesPerView": 2,
                        },
                        "886": {
                            "slidesPerView": 3,
                        },
                        "1024": {
                            "slidesPerView": 3,
                        },
                        "1368": {
                            "slidesPerView": 4,
                        }
                    }}
                    className="mySwiper">
                    {
                        homeSettings.moduleGreatInterest.items.map((item, index) => (
                            <SwiperSlide key={index}>
                                <Link href={item.redirectUrl}>
                                    <a className="area-box">
                                        <div className="area-box__images">
                                            <img src={item.imageUrl} alt="" />
                                        </div>

                                        <div className="area-box__text">
                                            <h3>
                                                {item.title}
                                            </h3>
                                            <p>
                                                {item.houseCount} căn nhà
                                            </p>
                                        </div>
                                    </a>
                                </Link>
                            </SwiperSlide>
                        ))
                    }

                </Swiper>
            </div>
        </section>
    )
}

export default Caring;