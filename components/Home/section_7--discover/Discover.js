import ProductItem from "@/components/Product-item/product-box";
import React from "react";
import { Grid, Navigation, Pagination } from 'swiper';
import "swiper/css";
import "swiper/css/grid";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from 'swiper/react';

const Discover = ({ products }) => {
    return (
        <section className="discover">
            <div className="container">
                <div className="homepages-title">
                    <h2 className="main-title">
                        Khám phá thêm
                    </h2>

                    <p className="sub-title">
                        Chúng tôi đưa ra cho bạn những gợi ý khác có thể bạn sẽ quan tâm
                    </p>
                </div>

                <Swiper slidesPerView={'auto'} spaceBetween={30}
                    modules={[Pagination, Grid, Navigation]}
                    grid={{
                        "rows": 2,
                        fill: 'row',
                    }}
                    pagination={{
                        "clickable": true,
                        "renderBullet": function (index, className) {

                            return '<span class=\"' + className + '\">' + (index + 1) + '</span>';

                        }
                    }} navigation={false} autoplay={{
                        "delay": 5000
                    }}
                    breakpoints={{
                        "300": {
                            "slidesPerView": 'auto',
                            "spaceBetween": 16,
                            "grid": {
                                "rows": 1,
                                fill: 'column',
                            },
                            "pagination": {
                                "type": "fraction"
                            }
                        },
                        "768": {
                            "slidesPerView": 2,
                            "slidesPerGroup": 2
                        },
                        "886": {
                            "slidesPerView": 3,
                            "slidesPerGroup": 3
                        },
                        "1024": {
                            "slidesPerView": 3,
                            "slidesPerGroup": 3
                        },
                        "1368": {
                            "slidesPerView": 3,
                            "slidesPerGroup": 3
                        }
                    }}
                    className="mySwiper">
                    {products.map(product => (
                        <SwiperSlide key={product.id}>
                            <ProductItem product={product} name={"Discover"}></ProductItem>
                        </SwiperSlide>
                    ))}

                </Swiper>
            </div>
        </section >
    )
}

export default Discover;