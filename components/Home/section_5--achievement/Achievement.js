import React, { useEffect, useRef, useState } from 'react';
import { useCountUp } from 'react-countup';

const Achievement = () => {

    const {
        start: startFlow,
    } = useCountUp({
        ref: "counterFlow",
        start: 0,
        end: 10.000,
        duration: 2,
        decimal: ",",
        decimals: 3
    });

    const {
        start: startQualityHome,
    } = useCountUp({
        ref: "counterQuality",
        start: 0,
        end: 14.822,
        duration: 2,
        decimal: ",",
        decimals: 3
    });

    const {
        start: startTrusted,
    } = useCountUp({
        ref: "counterTrusted",
        start: 0,
        end: 500,
        duration: 2,
        decimal: ","
    });

    const {
        start: startCity,
    } = useCountUp({
        ref: "counterCity",
        start: 0,
        end: 50,
        duration: 2,
        decimal: ","
    });

    let isCounterStart = false
    const achievementEl = useRef(null);

    useEffect(() => {
        const a = (achievementEl.current.offsetTop - window.outerHeight) + achievementEl.current.offsetHeight + 80

        window.addEventListener("scroll", function () {
            if (a <= window.pageYOffset && !isCounterStart) {
                startFlow()
                startTrusted()
                startQualityHome()
                startCity()
                isCounterStart = true
            }
        }, false);

    }, [])

    return (
        <section className="achievement" id="achievement" ref={achievementEl}>
            <div className="container">
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-6">
                        <div className="homepages-title text-left">
                            <h2 className="main-title">
                                Thành tựu của chúng tôi
                            </h2>

                            <p className="sub-title">
                                Với đội ngũ chuyên nghiệp luôn làm việc chăm chỉ để cho khách hàng có trải nghiệm tốt nhất.
                            </p>
                        </div>
                    </div>

                    <div className="col-12 col-sm-12 col-md-12 col-lg-6">
                        <div className="row no-gutters">
                            <div className="col-6">
                                <div className="media">
                                    <img src="/assets/images/home-ic-1.svg" alt="" className="mr-2" />

                                    <div className="media-body">
                                        <h5 className="title dp_fl jc_fs ai_c">
                                            <div id="counterFlow" />
                                            <span>+</span>
                                        </h5>

                                        <p className="text">
                                            Lượt theo dõi mỗi ngày
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-6">
                                <div className="media">
                                    <img src="/assets/images/home-ic-2.svg" alt="" className="mr-2" />

                                    <div className="media-body">
                                        <h5 className="title dp_fl jc_fs ai_c">

                                            <div id="counterQuality" /><span>+</span>

                                        </h5>

                                        <p className="text">
                                            Căn nhà chất lượng
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-6">
                                <div className="media">
                                    <img src="/assets/images/home-ic-3.svg" alt="" className="mr-2" />

                                    <div className="media-body">
                                        <h5 className="title dp_fl jc_fs ai_c">
                                            <div id="counterTrusted" /><span>+</span>
                                        </h5>
                                        <p className="text">
                                            Khách hàng tín nhiệm
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-6">
                                <div className="media">
                                    <img src="/assets/images/home-ic-4.svg" alt="" className="mr-2" />

                                    <div className="media-body">
                                        <h5 className="title dp_fl jc_fs ai_c">
                                            <div id="counterCity" /><span>+</span>
                                        </h5>
                                        <p className="text">
                                            Tỉnh/Thành phố
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Achievement;