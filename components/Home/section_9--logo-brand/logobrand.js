import Link from "next/link";
import React from "react";
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from 'swiper/react';


const LogoBrand = () => {
    return (
        <section className="logo-brand">
            <div className="container">
                <div className="homepages-title">
                    <h2 className="main-title">
                        Đối tác tiêu biểu
                    </h2>
                </div>
                <div className="slider-logo">
                    <Swiper slidesPerView={3} spaceBetween={30} loop={true} pagination={false} navigation={false} autoplay={{
                        "delay": 5000
                    }}
                        breakpoints={{
                            "300": {
                                "slidesPerView": 2,
                            },
                            "768": {
                                "slidesPerView": 3,
                            },
                            "886": {
                                "slidesPerView": 4,
                            },
                            "1024": {
                                "slidesPerView": 5,
                            },
                            "1368": {
                                "slidesPerView": 6,
                            }
                        }}
                        className="mySwiper">
                        <SwiperSlide>
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/lgb1.svg" alt="" />
                                </a>
                            </Link>
                        </SwiperSlide>

                        <SwiperSlide>
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/lgb2.svg" alt="" />
                                </a>
                            </Link>
                        </SwiperSlide>

                        <SwiperSlide>
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/lgb3.svg" alt="" />
                                </a>
                            </Link>
                        </SwiperSlide>

                        <SwiperSlide>
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/lgb4.svg" alt="" />
                                </a>
                            </Link>
                        </SwiperSlide>

                        <SwiperSlide>
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/lgb5.svg" alt="" />
                                </a>
                            </Link>
                        </SwiperSlide>

                        <SwiperSlide>
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/lgb6.svg" alt="" />
                                </a>
                            </Link>
                        </SwiperSlide>

                        <SwiperSlide>
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/lgb4.svg" alt="" />
                                </a>
                            </Link>
                        </SwiperSlide>
                    </Swiper>
                </div>
            </div>
        </section>
    )
}

export default LogoBrand;