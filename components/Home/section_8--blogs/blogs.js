import Link from "next/link";
import React from "react";
import SwiperCore, {
    Pagination
} from 'swiper';
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from 'swiper/react';
import { formatDate } from "@/lib/helper";
import VerticalBlogItem from "../../Blog-item/vertical-blog-item";


SwiperCore.use([Pagination]);


const HomeBlogs = ({ articles }) => {
    return (
        <section className="home-blogs">
            <div className="container">
                <div className="homepages-title">
                    <h2 className="main-title">
                        Tất cả bài viết
                    </h2>
                </div>
                <Swiper slidesPerView={'auto'} spaceBetween={30} loop={true} pagination={{
                    "dynamicBullets": true
                }} navigation={false} autoplay={{
                    "delay": 5000
                }}
                    breakpoints={{
                        "300": {
                            "slidesPerView": 'auto',
                            "spaceBetween": 16
                        },
                        "768": {
                            "slidesPerView": 2,
                        },
                        "886": {
                            "slidesPerView": 3,
                        },
                        "1024": {
                            "slidesPerView": 3,
                        },
                        "1368": {
                            "slidesPerView": 3,
                        }
                    }}
                    className="mySwiper">

                    {articles.map(item => (
                        <SwiperSlide key={item.id}>
                            <VerticalBlogItem item={item}></VerticalBlogItem>
                        </SwiperSlide>
                    ))}
                </Swiper>
            </div>
        </section>
    )
}

export default HomeBlogs;