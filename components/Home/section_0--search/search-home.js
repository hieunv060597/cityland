import { removeAccent } from '@/lib/helper';
import LocationService from '@/lib/location/location.service';
import search from '@/styles/header/search.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import SelectSearch from 'react-select-search/dist/cjs';
import 'react-select-search/style.css';
import * as Yup from 'yup';
import { filterCheckboxValues, listRangePrices, maxPeopleOfRoom } from '../../Header/search.model';

const fuzzySearch = (options) => {
    return (value) => {
        if (!value.length) {
            return options;
        }
        const newValue = removeAccent(value).toLowerCase();
        return options.filter(s => {
            const newKey = removeAccent(s.name).toLowerCase();
            return newKey.includes(newValue);
        })
    };
}

const SearchHome = (data) => {
    const { houseTypes, cities } = data;

    const [districts, setDistrict] = useState([]);
    const [cityPicked, setCityPicked] = useState(null);
    const [districtPicked, setDistrictPicked] = useState(null);
    const [pricePicked, setPricePicked] = useState(null);
    const [houseTypePicked, setHouseTypePicked] = useState(null);
    const [peopleCountPicked, setPeopleCountPicked] = useState(null);
    const [flag, setFlag] = useState(false)
    const [checkboxValues, setCheckboxValues] = useState(filterCheckboxValues);
    const [checkboxValueCount, setCheckboxValueCount] = useState(checkboxValues.filter(c => c.checked).length);

    const [services, setService] = useState(data.services);

    const validationSchema = Yup.object().shape({
        q: Yup.string().nullable().notRequired().when('q', {
            is: (value) => value?.length,
            then: (rule) => rule.min(3, 'Từ khoá phải từ 3-200 ký tự'),
        })
    }, [
        ['q', 'q']
    ]);

    const formOptions = { resolver: yupResolver(validationSchema) };

    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    const selectDataCities = cities.map(c => ({
        name: c.name.includes('Tỉnh') ? c.name.replace('Tỉnh', '') : c.name.replace('Thành phố', ''),
        value: c.id
    }));

    const selectDataDistricts = districts.map(d => ({
        value: d.id,
        name: d.name
    }));

    const listDistricts = async (cityId) => {
        try {
            const res = await LocationService.litsDistrict(cityId);
            setDistrict(res.districts)
        } catch (error) {
            alert(error.message ?? 'Có lỗi xảy ra')
        }
    }

    const cityChanged = async (value) => {
        setDistrictPicked(null);
        setCityPicked(value);
        await listDistricts(value);
    }

    function onSubmit(data) {
        if (cityPicked) {
            const city = cities.find(c => c.id === cityPicked);
            if (!city) {
                alert('Không tìm thấy Tỉnh/Thành phố');
                return
            }
            data.city = city.codename;
        } else {
            data.district = ''
        }
        if (districtPicked) {
            const district = districts.find(d => d.id === districtPicked);
            if (!district) {
                alert('Không tìm thấy Quận/Huyện');
                return
            }
            data.district = district.codename
        } else {
            data.district = '';
        }

        if (pricePicked) {
            const prices = pricePicked.split('-');
            data.minPrice = `${parseInt(`${prices[0]}000000`, 10)}`;
            data.maxPrice = `${prices[1]}000000`;
        }

        data.type = houseTypes.find(t => t.value == houseTypePicked)?.name;
        data.roomPeopleCount = peopleCountPicked;

        const pickedServices = services.filter(v => v.isPicked);
        if (pickedServices.length > 0) {
            data.serviceIds = pickedServices.map(s => s.id);
        } else {
            data.serviceIds = ''
        }

        Object.keys(data).forEach((k) => {
            if (!data[k] || data[k] == '' || data[k] == 'undefined') {
                delete data[k]
            }
        });

        const queries = new URLSearchParams(data);
        window.open(`/tim-kiem?${queries}`, "_blank")
        return;
    }

    const resetSearchForm = () => {
        setDistrictPicked(null);
        setCityPicked(null);
        setPeopleCountPicked(null);
        setPricePicked(null);
        setHouseTypePicked(null)
        setCheckboxValues(checkboxValues.map(c => ({
            ...c,
            checked: false,
        })))
        setCheckboxValueCount(0)
        reset();
    }

    const inputChange = (idx, isPicked) => {
        services[idx].isPicked = isPicked;
        setService(services);
        const checkedCheckboxes = services.filter(v => v.isPicked);
        setCheckboxValueCount(checkedCheckboxes.length);
    }

    const openPanel = () => {
        setFlag(!flag)
    }

    return (
        <section className="homepages-search">
            <div id={search.header_search} >
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="header_search_home-pages">
                                <form onSubmit={handleSubmit(onSubmit)} className={search.header_search_form}>
                                    <div className="row">
                                        <div className="col-12 col-sm-12 col-md-7 col-lg-8">
                                            <div className="row">
                                                <div className="col-12 col-lg-12">
                                                    <div className={`${search.header_search_col} w-10`}>
                                                        <span className={search.search_icon}>
                                                            <i className="search-feature-icon"></i>
                                                        </span>
                                                        <input
                                                            name="q"
                                                            type="text" {...register('q')}
                                                            placeholder="Tìm kiếm theo tên toà nhà, địa điểm"
                                                            className={`form-control m-0 ${errors.q ? 'is-invalid' : ''} ${search.form_input}`}
                                                        />
                                                        <span className="error_message search_form_col_error_message">{errors.q?.message}</span>
                                                    </div>
                                                </div>

                                                <div className="col-6 col-lg-6">
                                                    <SelectSearch
                                                        value={cityPicked}
                                                        options={selectDataCities}
                                                        search={true}
                                                        filterOptions={fuzzySearch}
                                                        onChange={cityChanged}
                                                        placeholder="Tỉnh/Thành phố"
                                                    />
                                                </div>

                                                <div className="col-6 col-lg-6">
                                                    <SelectSearch
                                                        value={districtPicked}
                                                        options={selectDataDistricts}
                                                        search
                                                        filterOptions={fuzzySearch}
                                                        onChange={setDistrictPicked}
                                                        placeholder="Quận/Huyện"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12 col-sm-12 col-md-5 col-lg-4">
                                            <div className="row">
                                                <div className="col-12 col-lg-12">
                                                    <div className="submit-search">
                                                        <button type="submit" className={`btn btn-primary ${search.form_submit}`}>
                                                            Tìm kiếm
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className="col-12 col-lg-12">
                                                    <div className="d-flex align-items-center justify-content-center">
                                                        <div className="flex-item">
                                                            <button type="button" onClick={resetSearchForm} className={`${search.form_button_reset} dp_fl jc_c ai_c`}>
                                                                <span>Đặt lại</span>
                                                                <i className={`fa fa-repeat mg_l5 ${search.form_reset_icon}`}></i>
                                                            </button>
                                                        </div>
                                                        <div className="flex-item d-flex align-items-center">
                                                            <span className={search.form_filter_count}>
                                                                {checkboxValueCount}
                                                            </span>
                                                            <button type="button" onClick={openPanel}
                                                                id="show_search_advanced_button"
                                                                className={`${search.form_advanced_search_button} accordion`}>
                                                                Tìm kiếm nâng cao
                                                            </button>
                                                            <i className={flag == false ? `fa fa-angle-down mg_l5 ${search.form_search_icon}` : `fa fa-angle-up mg_l5 ${search.form_search_icon}`}></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id={search.header_search_advanced} >
                <div className={flag == false ? "slide-up" : "slide-down"}>
                    <div className="container">
                        <div className="row mg_bt20">
                            <div className="col-12">
                                <h5>
                                    Thông tin bổ sung
                                </h5>
                            </div>
                        </div>
                        <div className="row mg_bt20" >
                            <div className={`col-12 col-sm-12 col-md-4 col-lg-4 ${search.header_search_advanced_col}`}>
                                <SelectSearch
                                    value={pricePicked}
                                    options={listRangePrices}
                                    onChange={setPricePicked}
                                    placeholder="Khoảng giá"
                                />
                            </div>
                            <div className={`col-12 col-sm-12 col-md-4 col-lg- ${search.header_search_advanced_col}`}>
                                <SelectSearch
                                    value={houseTypePicked}
                                    options={houseTypes}
                                    onChange={setHouseTypePicked}
                                    placeholder="Chọn loại nhà"
                                />
                            </div>
                            <div className={`col-12 col-sm-12 col-md-4 col-lg- ${search.header_search_advanced_col}`}>
                                <SelectSearch
                                    value={peopleCountPicked}
                                    options={maxPeopleOfRoom}
                                    onChange={setPeopleCountPicked}
                                    placeholder="Chọn số người ở"
                                />
                            </div>
                        </div>
                        <div className="row mg_bt20">
                            <div className="col-12">
                                <h5>
                                    Tiện tích
                                </h5>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 dp_fl jc_fs ai_fs fw_w">
                                {
                                    services.map((item, index) => (
                                        <div className="form_check_box mg_bt15" key={item.id}>
                                            <label className="form_check_label pointer" htmlFor={`checkBoxUtility${item.id}`}>
                                                <input
                                                    className="form_check_input"
                                                    type="checkbox"
                                                    value={item.id}
                                                    id={`checkBoxUtility${item.id}`}
                                                    onChange={() => inputChange(index, !item.isPicked)}
                                                    checked={item.isPicked}
                                                />
                                                <span>{item.name}</span>
                                            </label>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SearchHome;