import Head from 'next/head';
import React, { useContext } from "react";
import { Alert } from '@/components/Alert';
import { CustomerContext } from '../customer_context';

const Layout = ({ children }) => {
    const customerData = useContext(CustomerContext);
    const pathName = customerData.pathName ?? '';

    return (
        <div className="main-layout">
            <Head>

                <title>City Land - Website đặt phòng TingTong</title>

                <meta name="description" content="TingTong.vn là công ty có nhiều năm kinh nghiệm trong lĩnh vực thuê và cho thuê nhà giá tốt nhất thị trường." data-rh="true" />
                <link rel="shortcut icon" href="/assets/favicon.ico" />
                <meta name="title" content="Ting tong" />
                <meta name="robots" content="noodp,index,follow" />
                <meta name="author" content="nong hai" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta name="MobileOptimized" content="device-width" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta httpEquiv="content-language" content="vi" />
                <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="google" content="nositelinkssearchbox" />
                <meta property="og:title" content="City Land - Website đặt phòng TingTong" data-rh="true" />
                <meta name="robots" content="all" data-rh="true" />
                <meta property="og:url" content="https://tingtong.dev.suplo.vn" data-rh="true" />
                <meta property="og:image" content="https://tingtong.dev.suplo.vn" data-rh="true" />
                <link rel="canonical" href={`https://tingtong.dev.suplo.vn${pathName}`} />
                <link href="/assets/icon/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
                <link href="/assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
                <link href="/assets/style/scss/style.css" rel="stylesheet" />
                <link href="/assets/style/css/settings.css" rel="stylesheet" />

            </Head>
            <Alert id="right-alert" />
            {children}
        </div>
    )
}

export default Layout;