import { signOut } from "next-auth/client";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from "react-hook-form";
import { menuSettings } from "../../lib/settings/main-menu";

const Navbar = (data) => {
    const [opennav, setOpennav] = useState(false);

    const router = useRouter();
    useEffect(() => {
        navDesktop();
        navMobile();
        scroll_header();
    }, [])

    function navMobile() {
        const drop_menu = document.querySelectorAll(".nav-right .main-menu li .drop-menu");

        if (drop_menu == null) {
            return 0;
        } else {
            for (var i = 0; i < drop_menu.length; i++) {
                drop_menu[i].parentNode.classList.add("has-child");
            }

            const nav_li = document.querySelectorAll(".nav-right .main-menu li.has-child");

            for (var i = 0; i < nav_li.length; i++) {
                var btn = document.createElement("i");
                btn.classList.add("toggle_menu");
                btn.classList.add("fa");
                btn.classList.add("fa-angle-right");
                nav_li[i].appendChild(btn);
            }

            const btn_nav = document.querySelectorAll(".nav-right .main-menu li.has-child .toggle_menu");

            for (var i = 0; i < btn_nav.length; i++) {
                btn_nav[i].addEventListener("click", function () {
                    var menu = this.previousElementSibling;
                    this.classList.toggle("active");
                    if (this.classList.contains("active")) {
                        menu.style.maxHeight = menu.scrollHeight + "px";
                    }
                    else {
                        menu.style.maxHeight = null;
                    }
                })
            }
        }

    }

    function navDesktop() {
        const drop_menu = document.querySelectorAll(".nav-left .main-menu li .drop-menu");
        if (drop_menu == null) {
            return 0;
        }
        else {
            for (var i = 0; i < drop_menu.length; i++) {
                drop_menu[i].parentNode.classList.add("has-child");
            }
        }
    }

    function scroll_header() {
        var header = document.getElementById("header");
        if (header == null) {
            return 0;
        } else {
            var check = true;
            window.addEventListener("scroll", function () {
                if (window.pageYOffset > 10) {
                    if (check == true) {
                        header.classList.add("sticky");
                        check = false
                    }
                } else {
                    if (check == false) {
                        header.classList.remove("sticky");
                        check = true
                    }
                }
            })
        }
    }

    const openNav = () => {
        setOpennav(!opennav);
    }

    const validationSchema = Yup.object().shape({
        q: Yup.string().nullable().notRequired().when('q', {
            is: (value) => value?.length,
            then: (rule) => rule.min(3, 'Từ khoá phải từ 3-200 ký tự'),
        }).when('q', {
            is: (value) => value?.length,
            then: (rule) => rule.max(200, 'Từ khoá phải từ 3-200 ký tự'),
        })
    }, [
        ['q', 'q']
    ]);
    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const onSubmit = async (data) => {
        window.location.href = `/tim-kiem?q=${data.q}`
    }

    return (
        <header id="header">
            <div className="header-main">
                <div className="container">
                    <div className="nav-left">
                        <div className="logo">
                            <Link href="/">
                                <a>
                                    <img src="/assets/images/logo.svg" alt="logo" />
                                </a>
                            </Link>
                        </div>

                        <ul className="main-menu">
                            {menuSettings.map((item, index) => (
                                <li className={router.asPath == item.value ? "active" : ""} key={index}>
                                    <Link href={item.value}>
                                        <a>
                                            {item.name}
                                        </a>

                                    </Link>

                                    {item?.subMenu ? (
                                        <ul className="drop-menu">
                                            <li>
                                                <Link href="/bai-viet">
                                                    <a>
                                                        Link 1
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link href="/bai-viet">
                                                    <a>
                                                        Link 2
                                                    </a>
                                                </Link>
                                            </li>
                                        </ul>
                                    ) : ""}
                                </li>
                            ))}
                        </ul>
                    </div>

                    <div className="nav-right">
                        <div className={opennav == false ? "overlay" : "overlay active"}
                            onClick={openNav}>
                        </div>

                        <div className={opennav == false ? "respo-recont" : "respo-recont active"}>
                            {
                                data.children[0]
                            }
                            <ul className="main-menu">
                                <li className="close-li">
                                    <button className="close-nav" onClick={openNav}>
                                        <img src="/assets/images/cancel.svg" alt="close-nav" />
                                    </button>
                                </li>

                                {menuSettings.map((item, index) => (
                                    <li className={router.asPath == item.value ? "active" : ""} key={index}>
                                        <Link href={item.value}>
                                            <a>
                                                {item.name}
                                            </a>
                                        </Link>

                                        {item?.subMenu ? (
                                            <ul className="drop-menu">
                                                <li>
                                                    <Link href={item.value}>
                                                        <a>
                                                            {item.name}
                                                        </a>
                                                    </Link>
                                                </li>
                                            </ul>
                                        ) : ""}
                                    </li>
                                ))}


                                <li>
                                    <button className="switch-link" onClick={() => signOut()}>
                                        <span>Đăng xuất</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div className="mobile-bars">
                            <button onClick={openNav}>
                                <i className="fa fa-bars"></i>
                            </button>
                        </div>
                    </div >
                </div >
            </div >

            {data.children[1]}
        </header>
    )
}
export default Navbar;