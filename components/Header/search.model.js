export const filterCheckboxValues = [
    {
        name: 'Có điều hòa',
        value: '1',
        checked: false
    },
    {
        name: 'Có bình nóng lạnh',
        value: '2',
        checked: false
    },
    {
        name: 'Có khu vực đỗ xe',
        value: '3',
        checked: false
    },
    {
        name: 'Có khu bếp riêng',
        value: '4',
        checked: false
    },
    {
        name: 'Nhà vệ sinh khép kín',
        value: '5',
        checked: false
    },
    {
        name: 'Có lò vi sóng',
        value: '6',
        checked: false
    },
    {
        name: 'Có ti vi',
        value: '7',
        checked: false
    },
    {
        name: 'Có bàn là',
        value: '8',
        checked: false
    },
    {
        name: 'Không gian yên tĩnh',
        value: '9',
        checked: false
    },
    {
        name: 'Không gian xanh',
        value: '10',
        checked: false
    },
    {
        name: 'Có Internet',
        value: '11',
        checked: false
    },
    {
        name: 'Có khóa cửa vân tay',
        value: '12',
        checked: false
    },
    {
        name: 'Có bảo vệ khu nhà',
        value: '13',
        checked: false
    },
    {
        name: 'Khu vực đông dân cư',
        value: '14',
        checked: false
    },
    {
        name: 'Có quạt',
        value: '15',
        checked: false
    },
];


export const maxPeopleOfRoom = [
    {
        value: '1',
        name: '1',
        checked: false,
        id: '1',
    },
    {
        value: '2',
        name: '2',
        checked: false,
        id: '2',
    },
    {
        value: '3',
        name: '3',
        checked: false,
        id: '3',
    },
    {
        value: '4',
        name: '4',
        checked: false,
        id: '4',
    },
];


export const listHouseTypes = [
    {
        value: 'Nhà cho thuê cao cấp',
        name: 'Nhà cho thuê cao cấp',
        checked: false
    },
    {
        value: 'Nhà cho thuê',
        name: 'Nhà cho thuê',
        checked: false
    },
    {
        value: 'Nhà cho thuê bình dân',
        name: 'Nhà cho thuê bình dân',
        checked: false
    },
];


export const listRangePrices = [
    {
        value: '0-1',
        name: '< 1 triệu',
        checked: false
    },
    {
        value: '1-5',
        name: '1 triệu - 5 triệu',
        checked: false
    },
    {
        value: '5-10',
        name: '5 triệu - 10 triệu',
        checked: false
    },
    {
        value: '10-20',
        name: '10 triệu - 20 triệu',
        checked: false
    },
    {
        value: '20-50',
        name: '20 triệu - 50 triệu',
        checked: false
    },
    {
        value: '50-100',
        name: '50 triệu - 100 triệu',
        checked: false
    },
];

export const listArea = [
    {
        value: "0-30",
        name: '<30m2',
        checked: false
    },

    {
        value: "30-50",
        name: '30m2 - 50m2',
        checked: false
    },

    {
        value: "50-80",
        name: '50m2 - 80m2',
        checked: false
    },

    {
        value: "80-120",
        name: '80m2 - 120m2',
        checked: false
    },

    {
        value: "120-200",
        name: '120m2 - 200m2',
        checked: false
    },
]
