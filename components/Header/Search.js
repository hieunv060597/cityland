import { sleep } from '@/lib/helper';
import LocationService from '@/lib/location/location.service';
import ProductService from '@/lib/products/product.service';
import classes from '@/styles/header/search.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import Select from 'react-select';
import SelectSearch from 'react-select-search/dist/cjs';
import 'react-select-search/style.css';
import * as Yup from 'yup';
import { listRangePrices, maxPeopleOfRoom } from './search.model';

const HeaderSearch = () => {
    const [pricePicked, setPricePicked] = useState(null);
    const router = useRouter();
    const [district, setDistrict] = useState(null);
    const [city, setCity] = useState(null);
    const [cities, setCities] = useState([]);
    const [cityPicked, setCityPicked] = useState(null);
    const [districtPicked, setDistrictPicked] = useState(null);
    const [houseTypePicked, setHouseTypePicked] = useState(null);
    const [peopleCountPicked, setPeopleCountPicked] = useState(null);
    const [flag, setFlag] = useState(false);
    const [selectDataCities, setSelectDataCities] = useState([]);
    const [selectDataDistricts, setSelectDataDistricts] = useState([]);
    const [isDistrictLoading, setIsDistrictLoading] = useState(false);
    const [checkboxValueCount, setCheckboxValueCount] = useState(0);
    const [services, setServices] = useState([]);
    const [flagSearchMobile, setFlagSearchMobile] = useState(false);
    const [houseTypes, setHouseTypes] = useState([]);

    const validationSchema = Yup.object().shape({
        q: Yup.string().nullable().notRequired().when('q', {
            is: (value) => value?.length,
            then: (rule) => rule.min(3, 'Từ khoá phải từ 3-200 ký tự'),
        }).when('q', {
            is: (value) => value?.length,
            then: (rule) => rule.max(200, 'Từ khoá phải từ 3-200 ký tự'),
        })
    }, [
        ['q', 'q']
    ]);



    const formOptions = { resolver: yupResolver(validationSchema) };

    const { register, handleSubmit, reset, formState: { errors } } = useForm(formOptions);

    const { register: mobileRegister, handleSubmit: mobileHandleSubmit, reset: mobileReset, formState: {
        errors: mobileErrors
    } } = useForm(formOptions);


    useEffect(() => {
        const getData = async () => {
            const serviceRes = await ProductService.listService();
            const newServices = serviceRes.services.filter(s => s.type === "Thiết bị").map(s => (
                {
                    ...s,
                    isPicked: false,
                }
            ))
            setServices(newServices);

            const typeRes = await ProductService.listType();
            const newTypes = typeRes.types.map(t => ({
                value: t,
                name: t,
                checked: false
            }));
            setHouseTypes(newTypes);
        }
        getData()
    }, []);

    useEffect(() => {
        if (cities.length === 0) {
            const getCities = localStorage.getItem('ting_tong_cities');
            if (!getCities) {
                LocationService.listCity().then(res => {
                    localStorage.setItem('ting_tong_cities', JSON.stringify(res.cities));
                    setCities(res.cities);
                });
            } else {
                setCities(JSON.parse(getCities));
            }
        }

        setSelectDataCities(cities.map(c => ({
            label: c.name.includes('Tỉnh') ? c.name.replace('Tỉnh', '') : c.name.replace('Thành phố', ''),
            value: c.codename,
            id: c.id,
        })));
    }, [cities]);

    const openPanel = () => {
        setFlag(!flag)
    }

    const resetSearchForm = () => {
        setDistrictPicked(null);
        setDistrict(null);
        setCityPicked(null);
        setPeopleCountPicked(null);
        setPricePicked(null);
        setHouseTypePicked(null)
        setServices(services.map(c => ({
            ...c,
            isPicked: false,
        })));
        setCheckboxValueCount(0);
        setCityPicked(null)
        setCity(null);
        reset();
        mobileReset()
    }

    const inputChange = (idx, newChecked) => {
        const values = services;
        values[idx].isPicked = newChecked;
        setServices(values);
        const checkedCheckboxes = values.filter(v => v.isPicked);
        setCheckboxValueCount(checkedCheckboxes.length);
    }

    const listDistricts = async (cityId) => {
        try {
            setIsDistrictLoading(true)
            await sleep(200)
            const res = await LocationService.litsDistrict(cityId);
            setSelectDataDistricts(res.districts.map(c => ({
                label: c.name,
                value: c.codename,
                id: c.id,
            })));
            await sleep(200)
            setIsDistrictLoading(false)
        } catch (error) {
            setIsDistrictLoading(false)
            alert(error.message ?? 'Có lỗi xảy ra')
        }
    }

    const cityChanged = async (option) => {
        setCityPicked(null);
        setDistrictPicked(null);
        setDistrict(null);
        setCity(option);

        await listDistricts(option.id);
    }

    const districtChanged = async (option) => {
        setDistrictPicked(null);
        setDistrict(option)
    }

    function onSubmit(dataForm) {
        const formData = dataForm;
        if (district) {
            formData.district = district.value;
        }
        if (city) {
            formData.city = city.value;
        }

        formData.type = houseTypes.find(t => t.value === houseTypePicked)?.name;
        if (pricePicked) {
            const prices = pricePicked.split('-');
            formData.minPrice = `${parseInt(`${prices[0]}000000`, 10)}`;
            formData.maxPrice = `${prices[1]}000000`;
        }
        formData.roomPeopleCount = peopleCountPicked;

        formData.serviceIds = services.filter(s => s.isPicked).map(s => s.id)

        if (router.pathname !== '/tim-kiem') {
            Object.keys(formData).forEach((k) => {
                if (!formData[k] || formData[k] == '' || formData[k] == 'undefined') {
                    delete formData[k]
                }
            });
            const queries = new URLSearchParams(formData);
            window.open(`/tim-kiem?${queries}`, '_ blank');
            return;
        }
    }

    const toggleSearchMobile = () => {
        setFlagSearchMobile(!flagSearchMobile);
    }

    return (
        <>
            <section className="header-search-pages">
                <div id={classes.header_search}>
                    <div className="container">
                        <div className="header_search_main">
                            <form onSubmit={handleSubmit(onSubmit)} className={classes.header_search_form}>
                                <div className="form-row fw_nw mg_t0 pd_y15">
                                    <div className={`${classes.header_search_col} header-search-pages__select-box`}>
                                        <span className={classes.search_icon}>
                                            <i className="search-feature-icon ml-1"></i>
                                        </span>
                                        <input
                                            name="q"
                                            type="text" {...register('q')}
                                            placeholder="Tìm kiếm theo tên toà nhà, địa điểm"
                                            className={`form-control ${errors.q ? 'is-invalid' : ''} ${classes.form_input}`}
                                        />
                                        <span className="error_message search_form_col_error_message">{errors.q?.message}</span>
                                    </div>
                                    <div className="header-search-pages__select-box">
                                        <Select
                                            value={cityPicked}
                                            options={selectDataCities}
                                            onChange={cityChanged}
                                            placeholder="Tỉnh/Thành phố"
                                            noOptionsMessage={(data) => `Không tìm thấy Tỉnh/Thành với từ khoá "${data.inputValue}"`}
                                        />
                                        <span className={classes.city_name}>
                                            {
                                                city?.label ? city.label.includes('Tỉnh') ? city.label.replace('Tỉnh', '') : city.label.replace('Thành phố', '') : 'Toàn quốc'
                                            }
                                        </span>
                                    </div>
                                    <div className="header-search-pages__select-box">
                                        <Select
                                            value={districtPicked}
                                            options={selectDataDistricts}
                                            onChange={districtChanged}
                                            placeholder="Quận/Huyện"
                                            noOptionsMessage={(data) => `Không tìm thấy Quận/Huyện với từ khoá "${data.inputValue}"`}
                                            isLoading={isDistrictLoading}
                                        />
                                        <span className={classes.city_name}>{district?.label ?? 'Tất cả'}</span>
                                    </div>
                                    <div className="header-search-pages__select-box">
                                        <button type="button" onClick={resetSearchForm} className={`${classes.form_button_reset} dp_fl jc_c ai_c`}>
                                            <span>Đặt lại</span>
                                            <i className={`fa fa-repeat mg_l5 ${classes.form_reset_icon}`}></i>
                                        </button>
                                    </div>
                                    <div className="header-search-pages__select-box d-flex align-items-center">
                                        <span className={classes.form_filter_count}>
                                            {checkboxValueCount}
                                        </span>
                                        <button type="button" onClick={openPanel}
                                            id="show_search_advanced_button"
                                            className={`${classes.form_advanced_search_button} accordion`}>
                                            Tìm kiếm nâng cao
                                        </button>
                                        <i className={flag == false ? `fa fa-angle-down mg_l5 ${classes.form_search_icon}` : `fa fa-angle-up mg_l5 ${classes.form_search_icon}`}></i>
                                    </div>
                                    <div className="header-search-pages__select-box">
                                        <button type="submit" className={`btn btn-primary ${classes.form_submit}`}>
                                            Tìm kiếm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id={classes.header_search_advanced} >
                    <div className={flag == false ? "slide-up" : "slide-down"}>
                        <div className="container">
                            <div className="row mg_bt20">
                                <div className="col-12">
                                    <h5>
                                        Thông tin bổ sung
                                    </h5>
                                </div>
                            </div>
                            <div className="row mg_bt20" >
                                <div className={`col col-lg-4 col-md-4 col-sm-6 ${classes.header_search_advanced_col}`}>
                                    <SelectSearch
                                        value={pricePicked}
                                        options={listRangePrices}
                                        onChange={setPricePicked}
                                        placeholder="Khoảng giá"
                                    />
                                </div>
                                <div className={`col col-lg-4 col-md-4 col-sm-6 ${classes.header_search_advanced_col}`}>
                                    <SelectSearch
                                        value={houseTypePicked}
                                        options={houseTypes}
                                        onChange={setHouseTypePicked}
                                        placeholder="Chọn loại nhà"
                                    />
                                </div>
                                <div className={`col col-lg-4 col-md-4 col-sm-6 ${classes.header_search_advanced_col}`}>
                                    <SelectSearch
                                        value={peopleCountPicked}
                                        options={maxPeopleOfRoom}
                                        onChange={setPeopleCountPicked}
                                        placeholder="Chọn số người ở"
                                    />
                                </div>
                            </div>
                            <div className="row mg_bt20">
                                <div className="col-12">
                                    <h5>
                                        Tiện tích
                                    </h5>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 dp_fl jc_fs ai_fs fw_w">
                                    {
                                        services.map((item, index) => (
                                            <div className="form_check_box mg_bt15" key={item.id}>
                                                <label className="form_check_label pointer" htmlFor={`checkBoxUtility${item.id}`}>
                                                    <input
                                                        className="form_check_input"
                                                        type="checkbox"
                                                        value={item.id}
                                                        id={`checkBoxUtility${item.id}`}
                                                        onChange={() => inputChange(index, !item.isPicked)}
                                                        checked={item.isPicked}
                                                        key={item.id}
                                                    />
                                                    <span>{item.name}</span>
                                                </label>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="header-search-mobile">
                <div className="container">
                    <button className="btn-open-search-mb" onClick={toggleSearchMobile}>
                        <i></i>
                        <span>Tìm kiếm theo tên toà nhà, địa điểm</span>
                    </button>
                </div>

                <div className={flagSearchMobile == true ? "search-mb-pop-up" : "d-none"}>
                    <div className="container">
                        <div className="title">
                            <span>
                                Tìm kiếm
                            </span>

                            <button onClick={toggleSearchMobile}>
                                &times;
                            </button>
                        </div>

                        <form className="search-mb-form" onSubmit={mobileHandleSubmit(onSubmit)}>
                            <div className="input-box">
                                <input
                                    name="q"
                                    type="text" {...mobileRegister('q')}
                                    placeholder="Tìm kiếm địa điểm, khu vực"
                                    className={`form-control ${mobileErrors.q ? 'is-invalid' : ''} ${classes.form_input}`}
                                />
                            </div>

                            <div className="input-box">
                                <Select
                                    value={cityPicked}
                                    options={selectDataCities}
                                    onChange={cityChanged}
                                    placeholder=""
                                    noOptionsMessage={(data) => `Không tìm thấy Tỉnh/Thành với từ khoá "${data.inputValue}"`}
                                />
                                <span className="select-field">
                                    {
                                        city?.label ? city.label.includes('Tỉnh') ? city.label.replace('Tỉnh', '') : city.label.replace('Thành phố', '') : 'Toàn quốc'
                                    }
                                </span>
                            </div>

                            <div className="input-box">
                                <Select
                                    value={districtPicked}
                                    options={selectDataDistricts}
                                    onChange={districtChanged}
                                    placeholder=""
                                    noOptionsMessage={(data) => `Không tìm thấy Quận/Huyện với từ khoá "${data.inputValue}"`}
                                    isLoading={isDistrictLoading}
                                />
                                <span className="select-field">{district?.label ?? 'Tất cả'}</span>
                            </div>

                            <div className="input-box">
                                <div className="advance-search d-flex align-items-center">
                                    <span className={classes.form_filter_count}>
                                        {checkboxValueCount}
                                    </span>
                                    <button type="button" onClick={openPanel}
                                        id="show_search_advanced_button"
                                        className={`${classes.form_advanced_search_button} accordion`}>
                                        Tìm kiếm nâng cao
                                    </button>
                                    <i className={flag == false ? `fa fa-angle-down mg_l5 ${classes.form_search_icon}` : `fa fa-angle-up mg_l5 ${classes.form_search_icon}`}></i>
                                </div>
                            </div>

                            <div className={flag == false ? "input-box slide-up" : "input-box slide-down"}>
                                <div className="advance-select">
                                    <SelectSearch
                                        value={peopleCountPicked}
                                        options={maxPeopleOfRoom}
                                        onChange={setPeopleCountPicked}
                                        placeholder="Chọn số người ở"
                                    />
                                </div>

                                <div className="advance-select">
                                    <SelectSearch
                                        value={pricePicked}
                                        options={listRangePrices}
                                        onChange={setPricePicked}
                                        placeholder="Khoảng giá"
                                    />
                                </div>

                                <div className="advance-select">
                                    <SelectSearch
                                        value={houseTypePicked}
                                        options={houseTypes}
                                        onChange={setHouseTypePicked}
                                        placeholder="Chọn loại nhà"
                                    />
                                </div>

                                <div className="advance-select has-bg">
                                    <p className="filter-title">
                                        Tiện ích
                                    </p>
                                    <div className="filter-utilities">
                                        {
                                            services.map((item, index) => (
                                                <div className="form_check_box mg_bt15" key={item.id}>
                                                    <label className="form_check_label pointer" htmlFor={`checkBoxUtility${item.id}`}>
                                                        <input
                                                            className="form_check_input"
                                                            type="checkbox"
                                                            value={item.id}
                                                            id={`checkBoxUtility${item.id}`}
                                                            onChange={() => inputChange(index, !item.checked)}
                                                            checked={item.isPicked}
                                                            key={item.id}
                                                        />
                                                        <span>{item.name}</span>
                                                    </label>
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>

                            <div className="input-box has-stick">
                                <div className="actions">
                                    <button type="button" onClick={resetSearchForm} className="btn-reset-form">
                                        <span>Đặt lại</span>
                                        <i className={`fa fa-repeat mg_l5 ${classes.form_reset_icon}`}></i>
                                    </button>

                                    <button type="submit" className="btn-submit">
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </>
    )
}

export default HeaderSearch;