import { CustomerContext } from '@/components/customer_context';
import { alertService } from '@/lib/alert';
import { alertUnauthorize, convertCityName, convertDistrictName, formatDate, formatMoney, handleWishlist, showGlobalPopup, sleep } from '@/lib/helper';
import Image from 'next/image';
import Link from 'next/link';
import { useContext, useState } from 'react';

const ProductItem = ({ product }) => {
    const [isWishlistLoading, setWishlistLoading] = useState(false);
    const customerContext = useContext(CustomerContext);
    const totalDisable = customerContext.totalDisable;

    const isLiked = customerContext.wishlistIds?.includes(product.id)

    const pHandleWishlist = async (id, action) => {
        setWishlistLoading(true);
        customerContext.setTotalDisable(true);
        await sleep(500);

        const token = customerContext?.session?.user?.accessToken;
        if (!token) {
            return alertUnauthorize();
        }

        try {
            const resData = await handleWishlist(id, action, token, customerContext.wishlistCount, customerContext.wishlistIds)

            customerContext.setWishlistCount(resData.wishlistCount);
            customerContext.setWishlistId(resData.wishlistIds);

            if (resData.wishlistIds.length > 0) {
                localStorage.setItem('ting_tong_wishlist_id', JSON.stringify(resData.wishlistIds))
            } else {
                localStorage.removeItem('ting_tong_wishlist_id')
            }

            alertService.success(`${action ? 'Y' : 'Bỏ y'}êu thích nhà ${product.name} thành công.`, { id: 'right-alert' })
        } catch (error) {
            showGlobalPopup(error?.response?.data?.message ?? error.message ?? 'Có lỗi xảy ra')
        }

        await sleep(500);
        setWishlistLoading(false);
        customerContext.setTotalDisable(false);
    }

    return (<>
        <div className="product-box">
            <div className="product-box__images">
                <Link href={{
                    pathname: '/toa-nha/[id]',
                    query: { id: product.slug },
                }} passHref>
                    <a className="product-box__images--link" >
                        {
                            Array.isArray(product.images) && product.images.length > 0 ? (
                                <Image src={product.images[0].source} layout="fill" alt={product.slug} />
                            ) : (
                                <div className="images-bg" style={
                                    {
                                        backgroundImage: `url(/assets/images/pr1.jpg)`
                                    }
                                }>
                                </div>
                            )
                        }
                    </a>
                </Link>

                <div className="product-box__images--info">
                    <div className="console">
                        <i className="fa fa-eye"></i>
                        <span>{product.viewCount}</span>
                    </div>

                    <div className="console">
                        <i className="fa fa-picture-o"></i>
                        <span>{product.images?.length ?? 0}</span>
                    </div>

                    <div className="console">
                        <i className="fa fa-star"></i>
                        <span>{product.rating ?? 0}</span>
                    </div>

                    {product.roomCount == null || product.roomCount == 0 ?

                        <div className="status">

                            Đang được thuê

                        </div>

                        :
                        <div className="status available">

                            Có thể thuê

                        </div>

                    }

                </div>
            </div>

            <div className="product-box__text">
                <div className="product-box__text--header">
                    <Link href={{
                        pathname: '/toa-nha/[id]',
                        query: { id: product.slug },
                    }}>
                        <a className="title">
                            {product.name}
                        </a>
                    </Link>

                    <p className="type">
                        <span>Loại nhà:</span>
                        <span>{product.type}</span>
                    </p>
                </div>

                <div className="product-box__text--body">
                    <div className="location-info">
                        <a target="_blank" href="#" className="location">
                            {
                                `${product.address.address1}, ${convertDistrictName(product.address.district.name)}, ${convertCityName(product.address.city.name)}`
                            }
                        </a>

                        <p className="date">
                            <span>Ngày đăng: </span>
                            <span>{formatDate(product.createdAt)}</span>
                        </p>
                    </div>
                    <div className="count-room">
                        <div className="count-box">
                            <h4>Số Phòng</h4>
                            <p>{product.roomCount}</p>
                        </div>
                        {
                            product.totalArea > 0 ? (
                                <div className="count-box">
                                    <h4>Diện tích</h4>
                                    <p>{product.minRoomArea}-{product.maxRoomArea}m2</p>
                                </div>
                            ) : ''
                        }

                    </div>
                </div>

                <div className="product-box__text--footer">
                    <div className="price">
                        <label>Giá thuê</label>
                        <div className="price-box">
                            {
                                product.compareAtMaxPrice >= product.minPrice ? (
                                    <p className="price-box__sale">
                                        {formatMoney(product.compareAtMaxPrice)}
                                    </p>
                                ) : ''
                            }
                            <p className="price-box__main">
                                {formatMoney(product.minPrice)} /Phòng/Tháng
                            </p>
                        </div>
                    </div>

                    <div className="actions">

                        <button
                            value={product.id}
                            onClick={() => pHandleWishlist(product.id, !isLiked)}
                            className={isLiked ? "active" : ""}
                            disabled={isWishlistLoading || totalDisable}
                        >
                            {
                                isWishlistLoading ? (
                                    <div className="spinner-border text-danger" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                ) : (
                                    <i></i>
                                )
                            }
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </>)
}


export default ProductItem;