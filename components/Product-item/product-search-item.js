import Link from 'next/link';
import React, { useContext, useState } from 'react';
import { convertCityName, convertDistrictName, formatDate, formatMoney, handleWishlist, showGlobalPopup, sleep } from '@/lib/helper';
import { CustomerContext } from '../customer_context';
import { alertService } from '../../lib/alert';
import Image from 'next/image';
const ProductSearchItem = ({ product }) => {

    const [isWishlistLoading, setWishlistLoading] = useState(false);
    const customerContext = useContext(CustomerContext);
    const totalDisable = customerContext.totalDisable;

    const isLiked = customerContext.wishlistIds?.includes(product.id)

    const pHandleWishlist = async (id, action) => {
        setWishlistLoading(true);
        customerContext.setTotalDisable(true);
        await sleep(500);

        const token = customerContext?.session?.user?.accessToken;
        if (!token) {
            return alertUnauthorize();
        }

        try {
            const resData = await handleWishlist(id, action, token, customerContext.wishlistCount, customerContext.wishlistIds)

            customerContext.setWishlistCount(resData.wishlistCount);
            customerContext.setWishlistId(resData.wishlistIds);

            if (resData.wishlistIds.length > 0) {
                localStorage.setItem('ting_tong_wishlist_id', JSON.stringify(resData.wishlistIds))
            } else {
                localStorage.removeItem('ting_tong_wishlist_id')
            }

            alertService.success(`${action ? 'Y' : 'Bỏ y'}êu thích nhà ${product.name} thành công.`, { id: 'right-alert' })
        } catch (error) {
            showGlobalPopup(error?.response?.data?.message ?? error.message ?? 'Có lỗi xảy ra')
        }

        await sleep(500);
        setWishlistLoading(false);
        customerContext.setTotalDisable(false);
    }

    return (<>
        <div className={`col-12 col-md-12 col-lg-12`}>
            <div className="wishlist-box">
                <div className="wishlist-box__images">
                    <Link href={`/toa-nha/${product.slug}`} className="wishlist-box__images--link">
                        <a>
                            {
                                Array.isArray(product.images) && product.images.length > 0
                                    ? (<Image src={product.images[0].source} layout="fill" alt={product.slug} />)
                                    : (<img src={"/assets/images/no-img.svg"} alt={product.slug} />)
                            }
                        </a>
                    </Link>

                    <div className="wishlist-box__images--info">
                        <div className="console">
                            <i className="fa fa-eye"></i>
                            <span>{product.viewCount}</span>
                        </div>

                        <div className="console">
                            <i className="fa fa-picture-o"></i>
                            <span>{product.images?.length ?? 0}</span>
                        </div>

                        <div className="console">
                            <i className="fa fa-star"></i>
                            <span>4,0</span>
                        </div>
                    </div>

                    {product.roomCount == null || product.roomCount == 0 ?

                        <div className="wishlist-box__images--status">Đang được thuê</div>

                        :
                        <div className="wishlist-box__images--status available">Có thể thuê</div>

                    }


                </div>

                <div className="wishlist-box__info">
                    <div className="left-view">
                        <Link href={`/toa-nha/${product.slug}`} >
                            <a className="left-view__title" >
                                {product.name}
                            </a>
                        </Link>
                        <h4 className="left-view__location">
                            <i className="fa fa-map-marker"></i>
                            <span>{
                                `${product.address.address1}, ${convertDistrictName(product.address.district.name)}, ${convertCityName(product.address.city.name)}`
                            }</span>
                        </h4>

                        <p className="left-view__date">
                            <b>Ngày đăng: </b><span>{formatDate(product.createdAt)}</span>
                        </p>

                        <div className="left-view__price">
                            <label>Giá thuê</label>
                            <div className="left-view__price--box">
                                {
                                    product.compareAtMaxPrice >= product.minPrice ? (
                                        <del>{formatMoney(product.maxPrice)}</del>
                                    ) : ''
                                }
                                <ins>{formatMoney(product.minPrice)}<span>/Phòng/Tháng</span></ins>
                            </div>
                        </div>
                    </div>

                    <div className="right-view">
                        <div className="right-view__furniture">
                            <div className="right-view__furniture--info">
                                <label>Diện tích</label>
                                {
                                    product.totalArea > 0 ? (
                                        <p>
                                            <img src="/assets/images/mrs1.svg" alt="" />
                                            <span>{product.minRoomArea}-{product.maxRoomArea}m2</span>
                                        </p>

                                    ) : ''
                                }

                            </div>

                            <div className="right-view__furniture--info">
                                <label>Số phòng</label>
                                <p>
                                    <img src="/assets/images/mrs2.svg" alt="" />
                                    <span>{product.roomCount}</span>
                                </p>
                            </div>
                        </div>

                        <div className="right-view__action">
                            <button
                                value={product.id}
                                onClick={() => pHandleWishlist(product.id, !isLiked)}
                                className={isLiked ? "active" : ""}
                                disabled={isWishlistLoading || totalDisable}
                            >
                                {
                                    isWishlistLoading ? (
                                        <div className="spinner-border text-danger" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                    ) : (
                                        <i></i>
                                    )
                                }
                                <span className="liked">Yêu thích</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>)
}



export default ProductSearchItem;
