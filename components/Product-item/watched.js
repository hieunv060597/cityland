import Link from 'next/link';
import { formatDate, formatMoney } from '@/lib/helper';
import Image from 'next/image';
const WatchedItem = ({ item }) => {
    return (
        <div className="wishlist-box">
            <div className="wishlist-box__images">
                <Link href={{
                    pathname: '/toa-nha/[id]',
                    query: { id: item.slug },
                }}>
                    <a className="wishlist-box__images--link">
                        {
                            Array.isArray(item.images) && item.images.length > 0 ? (
                                <Image src={item.images[0].source} layout="fill" alt={item.slug} />
                            ) : (
                                <img src="/assets/images/no-img.jpg" alt="img" />
                            )
                        }
                    </a>
                </Link>

                <div className="wishlist-box__images--info">
                    <div className="console">
                        <i className="fa fa-eye"></i>
                        <span>{item.viewCount}</span>
                    </div>

                    <div className="console">
                        <i className="fa fa-picture-o"></i>
                        <span>{item.images?.length ?? 0}</span>
                    </div>

                    <div className="console">
                        <i className="fa fa-star"></i>
                        <span>{item.rating ?? 0}</span>
                    </div>
                </div>

                {item.roomCount == null || item.roomCount == 0 ?

                    <div className="wishlist-box__images--status">
                        Đang được thuê
                    </div>
                    :
                    <div className="wishlist-box__images--status"
                        style={
                            {
                                backgroundColor: '#0FBF60'
                            }
                        }
                    >
                        Có thể thuê
                    </div>
                }

            </div>


            <div className="wishlist-box__info">
                <div className="left-view">
                    <Link href={{
                        pathname: '/toa-nha/[id]',
                        query: { id: item.slug },
                    }}>
                        <a className="left-view__title">
                            {item.name}
                        </a>
                    </Link>

                    {
                        item.address ?
                            (<h4 className="left-view__location">
                                <i className="fa fa-map-marker"></i>
                                <span>
                                    {item.address?.address1}
                                </span>
                            </h4>) : ""

                    }


                    <p className="left-view__date">
                        <b>Ngày đăng: </b><span>{formatDate(item.createdAt)}</span>
                    </p>

                    <div className="left-view__price">
                        <label>Giá thuê</label>
                        <div className="left-view__price--box">
                            {
                                item.compareAtMaxPrice >= item.minPrice ? (
                                    <del>{formatMoney(item.compareAtMaxPrice)}</del>
                                ) : ''
                            }
                            <ins>{formatMoney(item.minPrice)}</ins>
                            <span>/Phòng/Tháng</span>
                        </div>
                    </div>
                </div>

                <div className="right-view">
                    <div className="right-view__furniture">
                        <div className="right-view__furniture--info">
                            <label>Diện tích</label>
                            {
                                item.totalArea > 0 ? (
                                    <p>
                                        <img src="/assets/images/mrs1.svg" alt="" />
                                        <span>{item.minRoomArea}-{item.maxRoomArea}m2</span>
                                    </p>
                                ) : ''
                            }

                        </div>

                        <div className="right-view__furniture--info">
                            <label>Số phòng</label>
                            <p>
                                <img src="/assets/images/mrs2.svg" alt="" />
                                <span>{item.roomCount}</span>
                            </p>
                        </div>
                    </div>

                    <div className="right-view__action">
                        <Link href={{
                            pathname: '/toa-nha/[id]',
                            query: { id: item.slug },
                        }} passHref>
                            <button className="bg-2">
                                <span>Chi tiết</span>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WatchedItem;