import { formatDate, formatMoney } from '@/lib/helper';
import { async } from '@firebase/util';
import Link from 'next/link';
import { useState } from 'react';
import Image from 'next/image';
const WishlistItem = ({ wishlist, onClick, totalDisable }) => {
    const [isLoading, setLoading] = useState(false)

    const deleteWishlist = async (id) => {
        setLoading(true);
        await onClick(id);
        setLoading(false)
    }

    return (
        <div className="wishlist-box">
            <div className="wishlist-box__images">
                <Link href={{
                    pathname: '/toa-nha/[id]',
                    query: { id: wishlist.slug },
                }}>
                    <a className="wishlist-box__images--link">
                        {
                            Array.isArray(wishlist.images) && wishlist.images.length > 0 ? (
                                <Image src={wishlist.images[0].source} layout="fill" alt="img" />
                            ) : (
                                <img src="/assets/images/pr1.jpg" alt="img" />
                            )
                        }
                    </a>
                </Link>

                <div className="wishlist-box__images--info">
                    <div className="console">
                        <i className="fa fa-eye"></i>
                        <span>{wishlist.viewCount}</span>
                    </div>

                    <div className="console">
                        <i className="fa fa-picture-o"></i>
                        <span>{wishlist.images?.length ?? 0}</span>
                    </div>

                    <div className="console">
                        <i className="fa fa-star"></i>
                        <span>{wishlist.rating ?? 0}</span>
                    </div>
                </div>

                {wishlist.roomCount == null || wishlist.roomCount == 0 ?

                    <div className="wishlist-box__images--status">
                        Đang được thuê
                    </div>

                    :
                    <div className="wishlist-box__images--status"
                        style={
                            {
                                backgroundColor: '#0FBF60'
                            }
                        }
                    >
                        Có thể thuê
                    </div>
                }

            </div>


            <div className="wishlist-box__info">
                <div className="left-view">
                    <Link href={{
                        pathname: '/products/[id]',
                        query: { id: wishlist.slug },
                    }}>
                        <a className="left-view__title">
                            {wishlist.name}
                        </a>
                    </Link>

                    {
                        wishlist.address ?
                            (<h4 className="left-view__location">
                                <i className="fa fa-map-marker"></i>
                                <span>
                                    {wishlist.address?.address1}
                                </span>
                            </h4>) : ""

                    }


                    <p className="left-view__date">
                        <b>Ngày đăng: </b><span>{formatDate(wishlist.createdAt)}</span>
                    </p>

                    <div className="left-view__price">
                        <label>Giá thuê</label>
                        <div className="left-view__price--box">
                            {
                                wishlist.compareAtMaxPrice >= wishlist.minPrice ? (
                                    <del>{formatMoney(wishlist.compareAtMaxPrice)}</del>
                                ) : ''
                            }
                            <ins>{formatMoney(wishlist.minPrice)}</ins>
                            <span>/Phòng/Tháng</span>
                        </div>
                    </div>
                </div>

                <div className="right-view">
                    <div className="right-view__furniture">
                        <div className="right-view__furniture--info">
                            <label>Diện tích</label>
                            {
                                wishlist.totalArea > 0 ? (
                                    <p>
                                        <img src="/assets/images/mrs1.svg" alt="" />
                                        <span>{wishlist.minRoomArea}-{wishlist.maxRoomArea}m2</span>
                                    </p>
                                ) : ''
                            }

                        </div>

                        <div className="right-view__furniture--info">
                            <label>Số phòng</label>
                            <p>
                                <img src="/assets/images/mrs2.svg" alt="" />
                                <span>{wishlist.roomCount}</span>
                            </p>
                        </div>
                    </div>

                    <div className="right-view__action">
                        <button onClick={(event) => deleteWishlist(wishlist.id)} disabled={totalDisable || isLoading}>
                            {
                                isLoading ? (
                                    <div className="spinner-border text-danger" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                ) : (
                                    <>
                                        <i className="unlike"></i>
                                        <span className="liked">Bỏ thích</span>
                                    </>
                                )
                            }
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WishlistItem;