import { formatMoney } from '@/lib/helper';
import Image from 'next/image';
const ProductDetailSampleRoom = ({ sampleRoom, openPopup }) => {
    return (
        <div className="list-type-box">
            <div className="images">
                {
                    Array.isArray(sampleRoom.images) && sampleRoom.images.length ?
                        (<Image src={sampleRoom.images[0].source} layout="fill" alt={sampleRoom.name} />) :
                        (<img src="/assets/images/no-img.jpg" alt={sampleRoom.name} />)
                }
            </div>

            <div className="content">
                <div className="content-header">
                    <div className="title">
                        <h4>{sampleRoom.type}</h4>
                    </div>

                    <div className="info">
                        <ul>
                            <li>
                                Diện tích: {sampleRoom.floorArea}m2
                            </li>

                            <li>
                                Số người ở: {sampleRoom.peopleCount}
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="content-body">
                    <ul className="row">
                        {
                            Array.isArray(sampleRoom.services) && sampleRoom.services.length > 0 ? (
                                sampleRoom.services.filter(s => s.type === "Thiết bị").map(s => (
                                    <li key={s.id} className="col-4 col-md-3">
                                        {
                                            Array.isArray(s.images) && s.images.length > 0 ? (
                                                <img src={s.images[0].source} alt={s.name} />
                                            ) : (<img src="/assets/images/no-img.jpg" alt={s.name} />)
                                        }
                                        <span>{s.name}</span>
                                    </li>
                                ))
                            ) : ''
                        }
                    </ul>
                </div>

                <div className="content-footer">
                    <div className="price">
                        {
                            sampleRoom.compareAtMaxPrice > sampleRoom.minPrice ? (
                                <div className="price-sale">
                                    <span> {formatMoney(sampleRoom.compareAtMaxPrice)}</span>
                                </div>
                            ) : ''
                        }
                        <div className="price-main">
                            <span> {formatMoney(sampleRoom.minPrice)}/tháng</span>
                        </div>
                    </div>
                    <div className="pick-up">
                        <button onClick={() => openPopup(sampleRoom.id)}>
                            Chọn phòng
                        </button>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default ProductDetailSampleRoom;