


const ProductDetailRenderImages = ({ images }) => {
    const i = images?.length ?? 0;
    if (i === 0) {
        return (
            <div className="grid-container">
                <div className="grid-item full">
                    <div className="product-detail__galerry--item height-50">
                        <img src="/assets/images/no-img.svg" alt="" />
                    </div>
                </div>
            </div>
        )
    }

    if (i === 1) {
        return (
            <div className="grid-container">
                <div className="grid-item full">

                    <div className="product-detail__galerry--item height-50 full">
                        <a href={images[0].source} style={
                            {
                                backgroundImage: `url(${images[0].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>
            </div>
        )
    }

    else if (i === 2) {
        return (
            <div className="grid-container">
                <div className="grid-item">

                    <div className="product-detail__galerry--item">
                        <a href={images[0].source} style={
                            {
                                backgroundImage: `url(${images[0].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>
                <div className="grid-item">
                    <div className="product-detail__galerry--item">
                        <a href={images[1].source} style={
                            {
                                backgroundImage: `url(${images[1].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>
            </div>
        )
    }

    else if (i === 3) {
        return (
            <div className="grid-container">
                <div className="grid-item">
                    <div className="product-detail__galerry--item">
                        <a href={images[0].source} style={
                            {
                                backgroundImage: `url(${images[0].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>
                </div>

                <div className="grid-item">

                    <div className="product-detail__galerry--item height-50">
                        <a href={images[1].source} style={
                            {
                                backgroundImage: `url(${images[1].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>

                <div className="grid-item three-grid">

                    <div className="product-detail__galerry--item height-50">
                        <a href={images[2].source} style={
                            {
                                backgroundImage: `url(${images[2].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>
            </div>
        )
    }

    else if (i === 4) {
        return (
            <div className="grid-container">
                <div className="grid-item">

                    <div className="product-detail__galerry--item">
                        <a href={images[0].source} style={
                            {
                                backgroundImage: `url(${images[0].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>

                <div className="grid-item">

                    <div className="product-detail__galerry--item height-50">
                        <a href={images[1].source} style={
                            {
                                backgroundImage: `url(${images[1].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>

                <div className="grid-item">

                    <div className="product-detail__galerry--item">
                        <a href={images[2].source} style={
                            {
                                backgroundImage: `url(${images[2].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>

                <div className="grid-item">

                    <div className="product-detail__galerry--item">
                        <a href={images[3].source} style={
                            {
                                backgroundImage: `url(${images[3].source})`
                            }
                        } className="btn-fancy" data-fancybox="gallery"></a>
                    </div>

                </div>
            </div>
        )
    } else {
        return (
            <div className="grid-container">
                {
                    images.map((item) => (
                        <div className="grid-item" key={item.position}>

                            <div className="product-detail__galerry--item loop-grid">
                                <a href={item.source} style={
                                    {
                                        backgroundImage: `url(${item.source})`
                                    }
                                } className="btn-fancy" data-fancybox="gallery"></a>
                            </div>

                        </div>
                    ))
                }
            </div>
        )
    }
}

export default ProductDetailRenderImages;