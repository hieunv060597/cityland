import { formatMoney } from '@/lib/helper';
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/thumbs";
import { Swiper, SwiperSlide } from "swiper/react";
import Image from 'next/image'
const ProductDetailPopup = ({
    thumbsSwiper,
    setThumbsSwiper,
    roomClicked,
    checkOut,
    roomType,
    roomPicked,
    floors,
    popup,
    closePopup,
    sameTypeOfRooms,
    sampleRoom
}) => {
    const services = roomPicked.services?.filter(s => s.type === "Thiết bị");

    return (
        <section className={popup == false ? "d-none" : "pop-up-detail"}>
            <div className="container">
                <div className="pop-up-title">
                    <p>Loại phòng: {roomType}</p>
                    <button onClick={() => closePopup()}>
                        <img src="/assets/images/ic_close.svg" alt="" />
                    </button>
                </div>
                <div className="pop-up-content">
                    <div className="slider-content">
                        {
                            Array.isArray(sampleRoom.images) && sampleRoom.images.length > 0 ?
                                <>
                                    <Swiper loop={true} spaceBetween={10} navigation={false} thumbs={{ swiper: thumbsSwiper }} className="mySwiper2">
                                        {sampleRoom.images.map((item) => (
                                            <SwiperSlide key={item.position}>
                                                <div className="images-props">
                                                    <Image src={item.source} layout="fill" alt="img" />
                                                </div>
                                            </SwiperSlide>
                                        ))}
                                    </Swiper>
                                    <Swiper
                                        onSwiper={setThumbsSwiper}
                                        loop={true}
                                        spaceBetween={10}
                                        navigation={true}
                                        slidesPerView={5}
                                        className="mySwiper"
                                    >
                                        {sampleRoom.images.map((item) => (
                                            <SwiperSlide key={item.position}>
                                                <div className="images-props-thumb">
                                                    <Image src={item.source} layout="fill" alt="img" />
                                                </div>
                                            </SwiperSlide>
                                        ))}
                                    </Swiper>
                                </>
                                :
                                <div className="slider-content__no-img">
                                    <img src="/assets/images/no-img.svg" alt="" className="w-100" />
                                </div>
                        }
                        <div className="slider-sub">
                            <ul>
                                <li>
                                    Diện tích: {roomPicked.floorArea}m2
                                </li>
                                <li>
                                    số người ở: {roomPicked.peopleCount}
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="info-room">
                        <div className="price">
                            {
                                roomPicked.compareAtMaxPrice > roomPicked.minPrice ? (
                                    <p className="price__sale">
                                        {formatMoney(roomPicked.compareAtMaxPrice)}
                                    </p>
                                ) : ''
                            }
                            <p className="price__main">
                                {formatMoney(roomPicked.minPrice)}
                            </p>
                        </div>

                        <div className="room-switch">
                            <div className="room-switch__title">
                                <p>
                                    <span>
                                        Chọn phòng
                                    </span>
                                    <span>
                                        {sameTypeOfRooms.length}
                                    </span>
                                </p>
                            </div>
                            <div className="room-switch__info">
                                <div className="room-switch__info--content">
                                    <div className="list">
                                        {
                                            floors.map(f => (
                                                <div key={f} className="list-room-box">
                                                    <h4 className="title">
                                                        Tầng {f}
                                                    </h4>
                                                    <ul>
                                                        {
                                                            sameTypeOfRooms.filter(s => s.floor === f).map(r =>
                                                            (
                                                                <li key={r.id}>
                                                                    {
                                                                        Array.isArray(r.variants) && r.variants.length > 0 ?
                                                                            (
                                                                                <button onClick={() => roomClicked(r.id, r.variants[0].id, !r.picked)} className={r.id === roomPicked.id ? "active" : ''}>
                                                                                    {r.name}
                                                                                </button>
                                                                            ) :
                                                                            (
                                                                                <button disabled>
                                                                                    {r.name}
                                                                                </button>
                                                                            )
                                                                    }
                                                                </li>
                                                            ))
                                                        }
                                                    </ul>
                                                </div>
                                            ))
                                        }
                                    </div>

                                    <div className="submit-room">
                                        <button onClick={checkOut}>
                                            Đặt lịch hẹn
                                        </button>
                                    </div>
                                </div>

                                <div className="room-switch__info--fill-data">

                                    <div className="title">
                                        <p>
                                            <span>Tiện ích</span>
                                            <span>{services.length}</span>
                                        </p>
                                    </div>
                                    <ul className="row">
                                        {
                                            services.map(s => (
                                                <li key={s.id} className="col-6 col-md-4 col-lg-3">
                                                    {
                                                        Array.isArray(s.images) && s.images.length > 0 ? (
                                                            <img src={s.images[0].source} alt={s.name} />
                                                        ) : (<img src="/assets/images/no-img.jpg" alt={s.name} />)
                                                    }
                                                    <span>{s.name}</span>
                                                </li>
                                            ))
                                        }
                                    </ul>

                                    {
                                        roomPicked?.description?.trim() && roomPicked?.description?.trim()?.length > 0 ? (
                                            <div className="description">
                                                <button>
                                                    Mô tả
                                                </button>

                                                <div dangerouslySetInnerHTML={{ __html: roomPicked.description }}>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="overlay" onClick={() => closePopup()}></div>
        </section>
    )
}

export default ProductDetailPopup;