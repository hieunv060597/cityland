import { CustomerContext } from '@/components/customer_context';
import { formatMoney } from '@/lib/helper';
import { signOut } from "next-auth/client";
import Link from "next/link";
import React, { useContext } from 'react';

const Authorize = () => {
    const customerData = useContext(CustomerContext);
    const customer = customerData.customer;
    const wishlistNumber = customerData.wishlistCount;
    const cart = customerData.cart;
    const itemCount = cart?.items?.length ?? 0;
    const items = cart?.items;

    return (
        <>
            {
                customer ?
                    (
                        <ul className="user-authorize-header">

                            <li>
                                <Link href="/dat-phong">
                                    <a>
                                        <img src="/assets/images/nav1.svg" alt="cart" />
                                        {
                                            itemCount > 0 ? (<span className="noti">
                                                {itemCount}
                                            </span>) : ""
                                        }

                                    </a>
                                </Link>

                                {
                                    Array.isArray(items) && items.length > 0 ?
                                        (
                                            <div className="drop-cart">
                                                <h4 className="drop-cart__title">Đơn đặt lịch</h4>

                                                <div className="drop-cart__content">
                                                    {
                                                        items.map(i => (
                                                            <div key={i.id} className="drop-cart__content--box">
                                                                <div className="title">
                                                                    <h5>{i.variant?.product?.building?.name}</h5>
                                                                    <ul>
                                                                        <li>
                                                                            Loại phòng: {i.variant?.product?.type}
                                                                        </li>

                                                                        <li>
                                                                            Tầng {i.variant?.product?.floor}
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div className="cart-box">
                                                                    <div className="row no-gutters">
                                                                        <div className="col-5">
                                                                            <div className="cart-box__images">
                                                                                {
                                                                                    Array.isArray(i.variant?.product?.images) && i.variant?.product.images.length > 0 ? (
                                                                                        <img src={i.variant?.product.images[0].source} alt={i.variant?.product?.name ?? 'no-img'} />
                                                                                    ) : (
                                                                                        <img src="/assets/images/no-img.jpg" alt="no-img" />
                                                                                    )
                                                                                }
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-7">
                                                                            <div className="cart-box__info">
                                                                                <h4 className="title">Phòng {i.variant?.name}</h4>
                                                                                <p className="price">{formatMoney(i.variant?.product?.minPrice ?? 0)}/tháng</p>
                                                                                <p className="area">Diện tích: {i.variant?.product?.floorArea}m2</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        ))
                                                    }
                                                </div>

                                                <div className="drop-cart__action jc_fe">
                                                    <Link href="/dat-phong" passHref>
                                                        <a className="btn-checkout">
                                                            Tiếp tục đặt lịch
                                                        </a>
                                                    </Link>
                                                </div>
                                            </div>
                                        ) : (
                                            <div className="drop-cart">
                                                <h4 className="drop-cart__title">Đơn đặt lịch</h4>

                                                <div className="drop-cart__content">
                                                    <div className="drop-cart__content--box">
                                                        <div className="cart-box">
                                                            <div className="row no-gutters">
                                                                <div className="col-5">
                                                                    <div className="cart-box__images">
                                                                        <img src="/assets/images/cart_none.svg" alt="cart none" />
                                                                    </div>
                                                                </div>

                                                                <div className="col-7">
                                                                    <div className="cart-box__info">
                                                                        <p className="cart-none">Chưa có đơn nào</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="drop-cart__action jc_fe">
                                                    <button className="btn-checkout disable">
                                                        Tiếp tục đặt lịch 1
                                                    </button>
                                                </div>
                                            </div>
                                        )
                                }
                            </li>

                            <li>
                                <Link href="/tai-khoan/yeu-thich">
                                    <a>
                                        <img src="/assets/images/nav2.svg" alt="yeu thich" />
                                        <span className={wishlistNumber > 0 ? "noti" : "d-none"}>
                                            +{wishlistNumber}
                                        </span>
                                    </a>
                                </Link>
                            </li>

                            <li>
                                <Link href="/tai-khoan/thong-tin">
                                    <a>
                                        <img src={customer.photoUrl == null ? "/assets/images/profile_check.svg" : customer.photoUrl} className="rounded-circle" alt="account" />
                                    </a>
                                </Link>
                                <div className="drop-down">
                                    <div className="media">
                                        <img src={customer.photoUrl == null ? "/assets/images/profile_check.svg" : customer.photoUrl}
                                            alt="account"
                                            className="mr-3"
                                        />
                                        <div className="media-body">
                                            <h4 className="title">
                                                {customer.name}
                                            </h4>

                                            <p className="email">
                                                {customer.email}
                                            </p>
                                        </div>
                                    </div>

                                    <div className="list-action">
                                        <ul>
                                            <li>
                                                <Link href="/tai-khoan/thong-tin">
                                                    <a className="switch-link">
                                                        <img src="/assets/images/icu1.svg" alt="account info" />
                                                        <span>Thông tin cá nhân</span>
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link href="/tai-khoan/dat-lich">
                                                    <a className="switch-link">
                                                        <img src="/assets/images/icu2.svg" alt="account rooms" />
                                                        <span>Danh sách đặt lịch</span>
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link href="/tai-khoan/yeu-thich">
                                                    <a className="switch-link">
                                                        <img src="/assets/images/icu3.svg" alt="account wishlist" />
                                                        <span>Danh sách yêu thích</span>
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link href="/tai-khoan/da-xem">
                                                    <a className="switch-link">
                                                        <img src="/assets/images/icu4.svg" alt="account watached" />
                                                        <span>Danh sách nhà đã xem</span>
                                                    </a>
                                                </Link>
                                            </li>

                                            <li>
                                                <button className="switch-link" onClick={() => signOut()}>
                                                    <img src="/assets/images/icu5.svg" alt="logout" />
                                                    <span>Đăng xuất</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    ) : (
                        <ul className="user-unauthorize-header">
                            <li>
                                <Link href="/dang-ky">
                                    <a>Đăng ký</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/dang-nhap">
                                    <a>Đăng Nhập</a>
                                </Link>
                            </li>

                            <li>
                                <div className="media">
                                    <img src="/assets/images/user_profile_unauth.png" alt="hello" className="mr-3" />
                                    <div className="media-body">
                                        <h4>Xin chào Guest</h4>
                                        <Link href="/dang-nhap">
                                            <a>Đăng nhập ngay</a>
                                        </Link>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    )
            }

        </>
    )
}

export default Authorize;