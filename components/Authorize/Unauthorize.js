import Link from "next/link";
import React from 'react';

const Unauthorize = () => {
    return (
        <ul className="user-unauthorize-header">
            <li>
                <Link href="/dang-ky">
                    <a>Đăng ký</a>
                </Link>
            </li>
            <li>
                <Link href="/dang-nhap">
                    <a>Đăng Nhập</a>
                </Link>
            </li>
            <li>
                <div className="media">
                    <img src="/assets/images/user_profile_unauth.png" alt="" className="mr-3" />
                    <div className="media-body">
                        <h4>Xin chào Guest</h4>
                        <Link href="/dang-nhap">
                            <a>Đăng Nhập</a>
                        </Link>
                    </div>
                </div>
            </li>
        </ul>
    )
}

export default Unauthorize;