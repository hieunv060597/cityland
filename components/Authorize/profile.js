import { signOut } from "next-auth/client";
import Link from "next/link";
import { useRouter } from 'next/router';
import React, { useContext } from 'react';
import { formatDate } from '@/lib/helper';
import HeaderSearch from '../Header/Search';
import Layout from '../Layout/Layout';
import Navbar from '../Navbar/Navbar';
import Authorize from './Authorize';
import { CustomerContext } from "../customer_context";

const Profile = ({ children }) => {
    const customerData = useContext(CustomerContext);
    const customer = customerData.customer;
    const wishlistTotalItem = customerData.wishlistCount;
    const router = useRouter();

    return (
        <Layout>
            <Navbar>
                <Authorize />
                <HeaderSearch></HeaderSearch>
            </Navbar>

            <main className="user-pages pd_bt50">
                <section className="main-breadcumb">
                    <div className="container">
                        <nav>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link href="/">
                                        <a>Trang chủ</a>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Tài khoản
                                </li>
                            </ol>
                        </nav>
                    </div>
                </section>

                <section className="user-profile">
                    <div className="container">
                        <div className="row no-gutters">
                            <div className="col-12 col-sm-12 col-lg-3 col-user-l-3">
                                <div className="user-profile__sesstion">
                                    <div className="user-profile__sesstion--image">
                                        <img src={customer.photoUrl == null ? "/assets/images/user_profile_unauth.png" : customer.photoUrl} alt="customer" />
                                    </div>
                                    <div className="user-profile__sesstion--name">
                                        <h4>
                                            {customer.name}
                                        </h4>
                                        <p>
                                            Ngày tạo tài khoản: {formatDate(customer.createdAt)}
                                        </p>
                                    </div>
                                </div>

                                <div className="user-profile__product-count">
                                    <ul>
                                        <li>
                                            Số nhà đã theo dõi: {wishlistTotalItem ?? 0}
                                        </li>

                                        <li>
                                            Số nhà đã yêu thích: {wishlistTotalItem ?? 0}
                                        </li>
                                    </ul>
                                </div>

                                <div className="user-profile__switch">
                                    <ul>
                                        <li className={router.asPath == "/tai-khoan/thong-tin" ? "active" : ""}>
                                            <Link href="/tai-khoan/thong-tin">
                                                <a
                                                    type="button"
                                                    className="switch-link">
                                                    <img src="/assets/images/icu1.svg" alt="" />
                                                    <span>Thông tin cá nhân</span>
                                                </a>
                                            </Link>
                                        </li>

                                        <li className={router.asPath == "/tai-khoan/dat-lich" ? "active" : ""}>
                                            <Link href="/tai-khoan/dat-lich">
                                                <a
                                                    type="button"
                                                    className="switch-link">
                                                    <img src="/assets/images/icu2.svg" alt="" />
                                                    <span>Danh sách đặt lịch</span>
                                                </a>
                                            </Link>
                                        </li>

                                        <li className={router.asPath == "/tai-khoan/yeu-thich" ? "active" : ""}>
                                            <Link href="/tai-khoan/yeu-thich">
                                                <a
                                                    type="button"
                                                    className="switch-link">
                                                    <img src="/assets/images/icu3.svg" alt="" />
                                                    <span>Danh sách yêu thích</span>
                                                </a>
                                            </Link>
                                        </li>

                                        <li className={router.asPath == "/tai-khoan/da-xem" ? "active" : ""}>
                                            <Link href="/tai-khoan/da-xem">
                                                <a
                                                    type="button"
                                                    className="switch-link">
                                                    <img src="/assets/images/icu4.svg" alt="" />
                                                    <span>Danh sách nhà đã xem</span>
                                                </a>
                                            </Link>
                                        </li>

                                        <li>
                                            <button className="switch-link" onClick={() => signOut()}>
                                                <img src="/assets/images/icu5.svg" alt="" />
                                                <span>Đăng xuất</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-12 col-sm-12 col-lg-9">
                                {children}
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
    )
}
export default Profile;

