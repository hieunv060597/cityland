import { useEffect, useState } from "react";
import eventEmitter from "@/lib/eventEmitter";

const Popup = () => {
    const [isShowPopup, setIsShowPopup] = useState(false);
    const [message, setMessage] = useState('');

    const listenerEvent = (eventData) => {
        setIsShowPopup(true);
        setMessage(eventData.message)
    }
    const listenerClonePopupEvent = () => {
        setIsShowPopup(false);
    }

    useEffect(() => {
        eventEmitter.addListener('show_global_popup', listenerEvent);
        eventEmitter.addListener('clone_global_popup', listenerClonePopupEvent);
    }, [])

    return (
        <section className={isShowPopup ? 'global-popup' : 'd-none'}>
            <div className="container">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Thông báo</h5>
                        <button type="button" className="close" onClick={() => setIsShowPopup(!isShowPopup)}>
                            ×
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>{message}</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={() => setIsShowPopup(!isShowPopup)}>Đóng</button>
                    </div>
                </div>
            </div>
            <div className="overlay" onClick={() => setIsShowPopup(!isShowPopup)}></div>
        </section>
    )
}

export default Popup;