import Link from 'next/link';
import { formatDate } from '@/lib/helper';


const VerticalBlogItem = ({ item }) => {

    return (
        <div className="blogs-box">
            <Link href={{
                pathname: '/bai-viet/[id]',
                query: { id: item.id },
            }}>
                <a className="blogs-box__images">
                    {item.image ? (
                        <img src={item.image} alt="adwa" />
                    ) : (
                        <img src="/assets/images/no-img.jpg" alt="adwa" />
                    )}
                </a>
            </Link>

            <div className="blogs-box__text">
                <Link href={{
                    pathname: '/bai-viet/[id]',
                    query: { id: item.id },
                }}>
                    <a className="blogs-box__text--title">
                        {item.title}
                    </a>
                </Link>

                <p className="blogs-box__text--description">
                    {
                        item.content?.replace(/(<([^>]+)>)/gi, "")?.length > 170 ?
                            `${item.content?.replace(/(<([^>]+)>)/gi, "")?.substring(0, 170)}...`
                            :
                            item.content?.replace(/(<([^>]+)>)/gi, "")
                    }
                </p>

                <div className="blogs-box__text--flex-date">
                    <p className="date">
                        {
                            formatDate(item.createdAt)
                        }
                    </p>

                    <Link href={{
                        pathname: '/bai-viet/[id]',
                        query: { id: item.id },
                    }}>
                        <a className="link">
                            Xem thêm
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default VerticalBlogItem;