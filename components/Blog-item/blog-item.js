import Link from 'next/link';
import Image from 'next/image'

const BlogItem = ({ blog }) => {

    return (
        <div className="blogs-item">
            <Link href={{
                pathname: '/bai-viet/[id]',
                query: { id: blog.id },
            }} >
                <a className="blogs-item__images">
                    <Image src={blog.image} layout="fill" alt="Picture of the author" />
                </a>
            </Link>

            <div className="blogs-item__text">
                <Link href={{
                    pathname: '/bai-viet/[id]',
                    query: { id: blog.id },
                }} >
                    <a className="blogs-item__text--title">
                        {blog.title}
                    </a>
                </Link>

                <p className="blogs-item__text--descript">
                    {
                        blog.content?.replace(/(<([^>]+)>)/gi, "")?.length > 150 ?
                            `${blog.content?.replace(/(<([^>]+)>)/gi, "")?.substring(0, 150)}...`
                            :
                            blog.content?.replace(/(<([^>]+)>)/gi, "")
                    }
                </p>

                <div className="blogs-item__text--date">
                    <p>19/02/2021</p>

                    <Link href={{
                        pathname: '/bai-viet/[id]',
                        query: { id: blog.id },
                    }} >
                        <a>
                            Xem thêm
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default BlogItem;