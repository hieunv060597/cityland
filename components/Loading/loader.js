import React, { useState } from 'react';
import Head from 'next/head';


const Loader = () => {
    return (
        <div className="loading-box">
            <Head>
                <link href="/assets/icon/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
                <link href="/assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
                <link href="/assets/style/scss/style.css" rel="stylesheet" />
            </Head>
            <div className="loading">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    )
}

export default Loader;