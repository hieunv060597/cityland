import Link from "next/link";
import React from "react";

const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer-body">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div className="footer-body__logo">
                                <Link href="/">
                                    <img src="/assets/images/logo.svg" alt="" />
                                </Link>
                            </div>

                            <div className="footer-body__sub-logo">
                                <p>
                                    TingTong.vn là công ty có nhiều năm kinh nghiệm trong lĩnh vực thuê và cho thuê nhà giá tốt nhất thị trường.
                                </p>
                            </div>

                            <div className="footer-body__social-media">
                                <ul>
                                    <li>
                                        <Link href="https://www.facebook.com/Tingtongvietnam">
                                            <a target="_blank" rel="noopener">
                                                <i className="fa fa-facebook"></i>
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="https://www.youtube.com/channel/UCqXBz2mLllZwBEBgSkFsDOQ">
                                            <a target="_blank" rel="noopener">
                                                <i className="fa fa-youtube-play"></i>
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div className="footer-body__link">
                                <h4 className="footer-body__link--tile">
                                    Danh sách trang
                                </h4>

                                <ul>
                                    <li>
                                        <Link href="/ve-chung-toi">
                                            <a>
                                                Về chúng tôi
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="/tim-kiem">
                                            <a>
                                                Toà nhà
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="/bai-viet">
                                            <a>
                                                Tin tức
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="#">
                                            <a>
                                                Dịch vụ
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="/lien-he">
                                            <a>
                                                Liên hệ
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div className="footer-body__link">
                                <h4 className="footer-body__link--tile">
                                    Dịch vụ của chúng tôi
                                </h4>

                                <ul>
                                    <li>
                                        <Link href="#">
                                            <a>
                                                Tuyển dụng
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="#">
                                            <a>
                                                Liên hệ
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-3">
                            <div className="footer-body__link">
                                <h4 className="footer-body__link--tile">
                                    Kết nối với chúng tôi
                                </h4>

                                <ul className="contact">
                                    <li>
                                        <Link href="mailto: tapdoantingtong@gmail.com">
                                            <a >
                                                <img src="/assets/images/icf1.svg" alt="email" />
                                                <span>tapdoantingtong@gmail.com</span>
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="tel: 0246286222">
                                            <a>
                                                <img src="/assets/images/icf2.svg" alt="phone number" />
                                                <span>024 628 6222</span>
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link href="https://www.google.com/maps/place/Platinum+Residences/@21.0279943,105.8174516,18.25z/data=!4m5!3m4!1s0x3135ab6e050f540f:0x3c07e4327cee132!8m2!3d21.027233!4d105.8169367?hl=vi-VN">
                                            <a target="_blank" rel="noopener">
                                                <img src="/assets/images/icf3.svg" alt="address" />
                                                <span>Cầu Giấy, Hà Nội</span>
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="footer-end">
                <p className="text-end">
                    Copyrights © 2021 by Cityland.
                </p>
            </div>
        </footer>
    )
}

export default Footer;